package Model.Strategy;

import Application.Events.EventListenerAsync;
import Application.Literals;
import Application.Preferences.PreferencesApp;
import Model.Marketplace.Base.Connector;
import Model.Marketplace.Base.Currency.Currency;
import Model.Marketplace.Base.Currency.*;
import Model.Marketplace.Base.Market.Market;
import Model.Marketplace.Base.Market.Offer;
import Model.Marketplace.Base.Marketplace;
import Model.Marketplace.MarketplacesManager;
import Model.Stats.SessionsManager;
//import Telegram.BotLaunch;
import Utils.ScaleOp;
import Utils.TableMono;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.*;

public class Strategy extends EventListenerAsync {

    protected static Logger log = LogManager.getLogger(Strategy.class.getName());

    //////////////////////////////////////////////////
    // Request actions.

    private interface ActionFollowUp {
        void onResult(Connector.EventResult eventResult);
    }

    private class ActionFollowUpArbitrage implements ActionFollowUp {

        private int arbitrageCaseID;

        ActionFollowUpArbitrage(int arbitrageCaseID) {
            this.arbitrageCaseID = arbitrageCaseID;
        }

        public void onResult(Connector.EventResult eventResult) {
            onArbitrageCaseResult(arbitrageCaseID, eventResult);
        }
    }

    //////////////////////////////////////////////////
    // Members.

    private PreferencesApp preferencesApp;
    private MarketplacesManager marketplacesManager;
    private SessionsManager sessionsManager;
//    private BotLaunch telegramBotLaunch;

    private Map<Integer, ActionFollowUp> actionFollowUps;

    //////////////////////////////////////////////////
    // Constructor.

    public Strategy(PreferencesApp preferencesApp, MarketplacesManager marketplacesManager, SessionsManager sessionsManager){//, BotLaunch telegramBotLaunch) {
        super();
        this.preferencesApp = preferencesApp;
        this.marketplacesManager = marketplacesManager;
        this.sessionsManager = sessionsManager;
//        this.telegramBotLaunch = telegramBotLaunch;

        // Subscribe to all marketplaces.
        marketplacesManager.addListener(this);

        actionFollowUps = new HashMap<>();

        arbitrageCaseID = 0;
        arbitrageCases = new HashMap<>();
        arbitrageBlocks = new HashSet<>();
        arbitrageTimeBalanceSend = 0;

        setTestMode(TestMode.DEFAULT);
    }

    //////////////////////////////////////////////////
    // Event listener.

    @Override
    protected void listenAsync(Event event) {
        if (event instanceof Connector.EventMarketChanged) {
            onEventMarketChanged((Connector.EventMarketChanged) event);
        }
        if (event instanceof Connector.EventResult) {
            onConnectorResult((Connector.EventResult) event);
        }
    }

    private void onEventMarketChanged(Connector.EventMarketChanged eventMarketChanged) {
        if (testMode == TestMode.DEFAULT) {
            try {
                decideArbitrage(eventMarketChanged.marketplaceType, eventMarketChanged.direction);
            } catch (Exception e) {
                log.error("Arbitrage exception.", e);
            }
        }
        if (testMode == TestMode.TESTDECIDE1) {
            testDecide1(eventMarketChanged.marketplaceType, eventMarketChanged.direction);
        }
        if (testMode == TestMode.TESTDECIDE2) {
            testDecide2(eventMarketChanged.marketplaceType, eventMarketChanged.direction);
        }
    }

    private void onConnectorResult(Connector.EventResult eventResult) {
        ActionFollowUp actionFollowUp = actionFollowUps.remove(eventResult.commID);
        if (actionFollowUp != null) {
            actionFollowUp.onResult(eventResult);
        }
    }

    //////////////////////////////////////////////////
    // Arbitrage.

    private static class OfferInfo {
        public Marketplace marketplace;
        public BigDecimal  amount;
        public BigDecimal  rate;
        public BigDecimal  rateWithFee;

        public OfferInfo(Marketplace marketplace, BigDecimal amount, BigDecimal rate, BigDecimal rateWithFee) {
            this.marketplace = marketplace;
            this.amount      = amount;
            this.rate        = rate;
            this.rateWithFee = rateWithFee;
        }
    }

    private static class KeyActionTake {
        public Marketplace marketplace;
        public Direction direction;

        public KeyActionTake() {
        }

        public KeyActionTake(Marketplace marketplace, Direction direction) {
            this.marketplace = marketplace;
            this.direction = direction;
        }

        public KeyActionTake(KeyActionTake other) {
            marketplace = other.marketplace;
            direction = other.direction;
        }

        @Override
        public boolean equals(Object other) {
            return (other instanceof KeyActionTake) && matches((KeyActionTake) other);
        }

        public boolean matches(KeyActionTake other) {
            return marketplace == other.marketplace && direction.matches(other.direction);
        }

        @Override
        public int hashCode() {
            return Objects.hash(marketplace, direction);
        }
    }

    private static class ValueActionTake {
        public BigDecimal amountBase;
        public BigDecimal amountQuote;
        public BigDecimal rate;

        public ValueActionTake() {
            amountBase  = BigDecimal.ZERO;
            amountQuote = BigDecimal.ZERO;
            rate        = BigDecimal.ZERO;
        }

        public ValueActionTake(BigDecimal amountBase, BigDecimal amountQuote, BigDecimal rate) {
            this.amountBase  = amountBase;
            this.amountQuote = amountQuote;
            this.rate        = rate;
        }
    }

    private class ArbitrageCase {
        private boolean observe;
        private DirectionPair directionPair;
        private Map<Integer, Map.Entry<KeyActionTake, ValueActionTake>> actionsTake;
        private Map<Integer, Marketplace.ResultData> resultsData;

        public ArbitrageCase(boolean observe, DirectionPair directionPair, Map<Integer, Map.Entry<KeyActionTake, ValueActionTake>> actionsTake) {
            this.observe = observe;
            this.directionPair = directionPair;
            this.actionsTake = actionsTake;
            resultsData = new HashMap<>();
        }
    }

    private int arbitrageCaseID;
    private Map<Integer, ArbitrageCase> arbitrageCases;
    private Set<DirectionPair> arbitrageBlocks;
    private long arbitrageTimeBalanceSend;

    //////////////////////////////////////////////////
    // Decision making.

    public void decideArbitrage(Marketplace.Type marketplaceTypeSource, Direction directionSource) {

        // If this direction pair is blocked,
        if (arbitrageBlocks.contains(directionSource.directionPair)) {
            // Ignore arbitrage request.
            return;
        }

        // This function is called when one of the markets has been updated. The updated market is called source
        // market. It needs to be compared with opposite markets on all other marketplaces. These markets are
        // called target markets.

        // Carry out all the tests to rule out empty source market.
        Marketplace marketplaceSource = marketplacesManager.marketplaces.get(marketplaceTypeSource);
        if (marketplaceSource == null) {
            return;
        }

        Market marketSource = marketplaceSource.marketsManager.markets.get(directionSource);
        if (marketSource == null || marketSource.offers == null || marketSource.offers.size() == 0) {
            return;
        }

        BigDecimal feeTransfer = preferencesApp.getTransferFee();

        DirectionPair directionPair = directionSource.directionPair;

        // Total arbitrage fee includes a user-set transfer fee (which is an average of how much a transfer from one
        // exchange to another costs) and a fee to complete a trade which is stored in direction pair's local data.

        // Create a list consisting of a single source marketplace.
        Collection<Marketplace> marketplacesSource = Collections.singletonList(marketplaceSource);

        // Target direction is opposite to the source direction.
        Direction directionTarget = marketplacesManager.getOppositeDirection(directionSource);

        Collection<Marketplace> marketplacesTarget = new ArrayList<>(marketplacesManager.marketplaces.values());
        marketplacesTarget.remove(marketplaceSource);

        // Trace basic data about markets.
        if (preferencesApp.getLogging())
        {
            StringBuilder traceMarketsGap = new StringBuilder();
            traceMarketsGap.append(directionPair.getName()).append(" ");
            if (directionSource.ask) {
                traceMarketsGap.append(Literals.nameDirectionAsk2).append(": ");
                addTraceMarketsGap(traceMarketsGap, marketplaceSource, directionSource);
                traceMarketsGap.append(Literals.nameDirectionBid2).append(": ");
                for (Marketplace marketplaceTarget : marketplacesTarget) {
                    addTraceMarketsGap(traceMarketsGap, marketplaceTarget, directionTarget);
                }
            } else {
                traceMarketsGap.append(Literals.nameDirectionAsk2).append(": ");
                for (Marketplace marketplaceTarget : marketplacesTarget) {
                    addTraceMarketsGap(traceMarketsGap, marketplaceTarget, directionTarget);
                }
                traceMarketsGap.append(Literals.nameDirectionBid2).append(": ");
                addTraceMarketsGap(traceMarketsGap, marketplaceSource, directionSource);
            }

            log.trace(traceMarketsGap);
        }

        // Create a list of all offers from target marketplaces that have better rates than the most competitive
        // rate on source market.
        List<OfferInfo> offersTarget = createJoinedShortList(marketplacesTarget, directionTarget, feeTransfer.add(preferencesApp.getMargin(directionPair.base)), marketSource.offers.get(0).getRate());

        // Check that there's at least a single offer in the comprehensive target list.
        int countTarget = offersTarget.size();
        if (countTarget == 0) {
            return;
        }

        // Sort offers from most competitive to least competitive.
        offersTarget.sort((offerInfo1, offerInfo2) -> -offerInfo1.marketplace.compareRates(directionTarget, offerInfo1.rateWithFee,
                                                       offerInfo2.marketplace,             directionTarget, offerInfo2.rateWithFee));

        // Create a list of all offers from source marketplace that have better rates than the most competitive
        // rate on target markets.
        List<OfferInfo> offersSource = createJoinedShortList(marketplacesSource, directionSource, feeTransfer, offersTarget.get(0).rateWithFee);

        int countSource = offersSource.size();
        if (countSource == 0) {
            return;
        }

        // Change over to ask/bid terms.
        int countAsk;
        int countBid;
        List<OfferInfo> offersAsk;
        List<OfferInfo> offersBid;
        Direction directionAsk;
        Direction directionBid;

        if (directionSource.ask) {
            countAsk = countSource;
            countBid = countTarget;
            offersAsk = offersSource;
            offersBid = offersTarget;
            directionAsk = directionSource;
            directionBid = directionTarget;
        } else {
            countAsk = countTarget;
            countBid = countSource;
            offersAsk = offersTarget;
            offersBid = offersSource;
            directionAsk = directionTarget;
            directionBid = directionSource;
        }

        // Create list of future actions.
        Map<KeyActionTake, ValueActionTake> actionsCreate = new HashMap<>();
        // Create key object to be used in the cycle.
        KeyActionTake keyActionTake = new KeyActionTake();

        // Start with the most competitive offers on both directions.
        int indexAsk = 0;
        int indexBid = 0;

        // Declare variables to hold cached data.
        OfferInfo offerAsk = null;
        BigDecimal amountLeftAsk = null;
        boolean updateDataAsk = true;

        OfferInfo offerBid = null;
        BigDecimal amountLeftBid = null;
        boolean updateDataBid = true;

        // Find smallest common base scale.
        ScaleOp scaleOpBaseCommon = marketplaceSource.getLocal(directionPair).scaleOpBase;
        for (OfferInfo offerTarget : offersTarget) {
            scaleOpBaseCommon = scaleOpBaseCommon.min(offerTarget.marketplace.getLocal(directionPair).scaleOpBase);
        }

        while (indexAsk < countAsk && indexBid < countBid) {
            // Read parameters of the offer on source market.
            if (updateDataAsk) {
                offerAsk = offersAsk.get(indexAsk);
                amountLeftAsk = offerAsk.amount;
                updateDataAsk = false;
            }

            // Read parameters of the offer on one of the target markets.
            if (updateDataBid) {
                offerBid = offersBid.get(indexBid);
                amountLeftBid = offerBid.amount;
                updateDataBid = false;
            }

            // Stop if target rate is more competitive than source rate.
            if (offerAsk.marketplace.compareRates(directionAsk, offerAsk.rateWithFee,
                offerBid.marketplace,             directionBid, offerBid.rateWithFee) <= 0) {
                break;
            }

            // Spent amount is equal to the minimal of the available amounts.
            BigDecimal amountSpent = amountLeftAsk.min(amountLeftBid);

            amountLeftAsk = amountLeftAsk.subtract(amountSpent);
            amountLeftBid = amountLeftBid.subtract(amountSpent);

            if (amountLeftAsk.signum() == 0) {
                keyActionTake.marketplace = offerAsk.marketplace;
                keyActionTake.direction = directionAsk;
                addAction(actionsCreate, keyActionTake, offerAsk.amount, offerAsk.rate);
                ++indexAsk;
                updateDataAsk = true;
            }

            if (amountLeftBid.signum() == 0) {
                keyActionTake.marketplace = offerBid.marketplace;
                keyActionTake.direction = directionBid;
                addAction(actionsCreate, keyActionTake, offerBid.amount, offerBid.rate);
                ++indexBid;
                updateDataBid = true;
            }
        }

        if (amountLeftAsk.signum() > 0) {
            BigDecimal used = scaleOpBaseCommon.set(offerAsk.amount.subtract(amountLeftAsk), RoundingMode.FLOOR);
            if (used.signum() > 0 && used.compareTo(offerAsk.marketplace.getLocal(directionPair).amountBaseMin) >= 0) {
                keyActionTake.marketplace = offerAsk.marketplace;
                keyActionTake.direction   = directionAsk;
                addAction(actionsCreate, keyActionTake, used, offerAsk.rate);
            }
        }

        if (amountLeftBid.signum() > 0) {
            BigDecimal used = scaleOpBaseCommon.set(offerBid.amount.subtract(amountLeftBid), RoundingMode.FLOOR);
            if (used.signum() > 0 && used.compareTo(offerBid.marketplace.getLocal(directionPair).amountBaseMin) >= 0) {
                keyActionTake.marketplace = offerBid.marketplace;
                keyActionTake.direction   = directionBid;
                addAction(actionsCreate, keyActionTake, used, offerBid.rate);
            }
        }

        createArbitrageCase(directionPair, new ArrayList<>(actionsCreate.entrySet()));
    }

    private void addTraceMarketsGap(StringBuilder stringBuilder, Marketplace marketplace, Direction direction) {
        BigDecimal rate = null;
        Market market = marketplace.marketsManager.markets.get(direction);
        if (market != null) {
            rate = market.getRateTop();
        }

        stringBuilder.append(String.format("%s %s %s ", marketplace.type.name, (rate == null) ? "-" : rate.toString(), marketplace.purses.get0(direction.getOut())));
    }

    private List<OfferInfo> createJoinedShortList(Collection<Marketplace> marketplaces, Direction direction, BigDecimal margin, BigDecimal rateLimit) {

        DirectionPair directionPair = direction.directionPair;

        // Fill out targets list with all offers that have better rate than the limiting rate including fee.
        List<OfferInfo> offersInfo = new ArrayList<>();
        // Iterate through all marketplaces excluding source marketplace.
        for (Marketplace marketplace : marketplaces) {
            // Carry out all the tests to rule out null pointers.
            Market market = marketplace.marketsManager.markets.get(direction);
            if (market == null) {
                continue;
            }

            synchronized (market) {
                List<Offer> offers = market.offers;
                if (offers == null) {
                    continue;
                }

                // Get total amount of currency that will be used to buy offers on this particular exchange.
                BigDecimal amountPurseLeftOut = marketplace.purses.get0(direction.getOut());
                // Get maximal purchase amount for limited mode.
                BigDecimal amountLimitLeftQuote = preferencesApp.getArbitrageLimit(direction.directionPair.quote);

                // Calculate trade fee for target market.
                BigDecimal feeTrade = BigDecimal.ONE.add(marketplace.getLocal(direction.directionPair).feeTradeTake);
                // Calculate total fee for target market.
                BigDecimal feeTotal = feeTrade.add(margin);

                // Get minimal trade amounts in both base and quote currencies.
                BigDecimal amountTradeMinBase  = marketplace.getLocal(directionPair).amountBaseMin;
                BigDecimal amountTradeMinQuote = marketplace.getLocal(directionPair).amountQuoteMin;

                // Iterate through all orders on target market.
                for (Offer offer : offers) {
                    // Find rate of the current offer on target market.
                    BigDecimal rate = offer.getRate();
                    // Include total fee.
                    BigDecimal rateWithFeeTotal = marketplace.decRate(direction, rate, feeTotal, RatePolicy.ScaleMode.UNLIMITED, 0, ScaleOp.roundingModeDefault);

                    // Stop iterating if rate including fee is worse than the limiting rate.
                    if (marketplace.compareRates(direction, rateWithFeeTotal, rateLimit) <= 0) {
                        break;
                    }

                    // Convert minimal trade amount from quote to base currency.
                    BigDecimal amountTradeMinBaseFromQuote = convertFromQuoteWithFee(marketplace, directionPair, amountTradeMinQuote, rate, feeTrade);
                    BigDecimal amountTradeMinBaseCombined = amountTradeMinBase.min(amountTradeMinBaseFromQuote);
                    // Convert purse amount to base currency if needed.
                    BigDecimal amountPurseLeftBase = (direction.getOut() == direction.getBase()) ? amountPurseLeftOut : convertFromQuoteWithFee(marketplace, directionPair, amountPurseLeftOut, rate, feeTrade);
                    // Convert purchase limit to base currency.
                    BigDecimal amountLimitLeftBase = convertFromQuoteWithFee(marketplace, directionPair, amountLimitLeftQuote, rate, feeTrade);
                    // Use the smaller limitation.
                    BigDecimal amountLeftBase = amountPurseLeftBase.min(amountLimitLeftBase);

                    // Stop iterating if the available amount to spend is smaller than the minimal allowed order size.
                    if (amountLeftBase.compareTo(amountTradeMinBaseCombined) < 0) {
                        break;
                    }

                    // Offer amount is always stored in base currency.
                    BigDecimal amountOfferBase = offer.getAmountBase();

                    // If the amount left in the purse is not enough to buy the offer completely,
                    if (amountOfferBase.compareTo(amountLeftBase) > 0) {
                        // If the limitation is greater than 0,
                        if (amountLeftBase.compareTo(BigDecimal.ZERO) > 0) {
                            // Consider buying as much of the offer as possible.
                            offersInfo.add(new OfferInfo(marketplace, marketplace.getLocal(directionPair).scaleOpBase.set(amountLeftBase, RoundingMode.FLOOR), rate, rateWithFeeTotal));
                        }
                        // No more offers will be considered.
                        break;
                    } else {
                        // Consider buying the offer completely.
                        offersInfo.add(new OfferInfo(marketplace, amountOfferBase, rate, rateWithFeeTotal));
                        // Convert the offer amount to the quote currency.
                        BigDecimal amountOfferQuote = convertWithFee(marketplace, directionPair, amountOfferBase, rate, feeTrade);
                        // Subtract amount purchased from purse stringAmount.
                        amountPurseLeftOut = amountPurseLeftOut.subtract((direction.getOut() == direction.getBase()) ? amountOfferBase : amountOfferQuote);
                        // Subtract amount purchased from purchase limit.
                        amountLimitLeftQuote = amountLimitLeftQuote.subtract(amountOfferQuote);
                    }
                }
            }
        }

        return offersInfo;
    }

    private static BigDecimal convertWithFee(Marketplace marketplace, DirectionPair directionPair, BigDecimal amountBase, BigDecimal rate, BigDecimal fee) {
        // Reserve two variables.
        BigDecimal amountQuote1;
        BigDecimal amountQuote2;

        // Convert purse amount to base currency first.
        amountQuote1 = marketplace.convert(directionPair, amountBase, rate, RoundingMode.CEILING);
        // Include trade fee second.
        amountQuote1 = marketplace.getLocal(directionPair).scaleOpQuote.mul(amountQuote1, fee, RoundingMode.CEILING);

        // Include trade fee first.
        amountQuote2 = marketplace.getLocal(directionPair).scaleOpQuote.mul(amountBase, fee, RoundingMode.CEILING);
        // Convert purse amount to base currency second.
        amountQuote2 = marketplace.convert(directionPair, amountQuote2, rate, RoundingMode.CEILING);

        // See which method provides the lowest amount and use it to be safe.
        return amountQuote1.max(amountQuote2);
    }

    private static BigDecimal convertFromQuoteWithFee(Marketplace marketplace, DirectionPair directionPair, BigDecimal amountQuote, BigDecimal rate, BigDecimal fee) {
        // Reserve two variables.
        BigDecimal amountBase1;
        BigDecimal amountBase2;

        // Convert purse amount to base currency first.
        amountBase1 = marketplace.convertFromQuote(directionPair, amountQuote, rate, RoundingMode.FLOOR);
        // Include trade fee second.
        amountBase1 = marketplace.getLocal(directionPair).scaleOpBase.div(amountBase1, fee, RoundingMode.FLOOR);

        // Include trade fee first.
        amountBase2 = marketplace.getLocal(directionPair).scaleOpBase.div(amountQuote, fee, RoundingMode.FLOOR);
        // Convert purse amount to base currency second.
        amountBase2 = marketplace.convertFromQuote(directionPair, amountBase2, rate, RoundingMode.FLOOR);

        // See which method provides the lowest amount and use it to be safe.
        return amountBase1.min(amountBase2);
    }

    private static void addAction(Map<KeyActionTake, ValueActionTake> groupActionCreate, KeyActionTake keyActionTake,
                                  BigDecimal amountBase, BigDecimal rate) {
        log.trace("Marking for purchase " + keyActionTake.marketplace.type.name + " " + keyActionTake.direction.getName() + " " + amountBase + " " + rate + ".");
        ValueActionTake valueActionTakeTarget = groupActionCreate.get(keyActionTake);
        if (valueActionTakeTarget == null) {
            valueActionTakeTarget = new ValueActionTake();
            groupActionCreate.put(new KeyActionTake(keyActionTake), valueActionTakeTarget);
        }

        BigDecimal amountQuote = keyActionTake.marketplace.ratePolicy.convert(keyActionTake.marketplace.getLocal(keyActionTake.direction.directionPair), amountBase, rate);

        valueActionTakeTarget.amountBase  = valueActionTakeTarget.amountBase .add(amountBase );
        valueActionTakeTarget.amountQuote = valueActionTakeTarget.amountQuote.add(amountQuote);
        valueActionTakeTarget.rate        = rate;
    }

    //////////////////////////////////////////////////
    // Decision execution and follow-up.

    // This function can potentially create a problem since a connector can start running immediately after a request
    // is made and return a result before a commID is put on a list. But realistically that should never happen (two
    // operators delayed long enough for an http request to be completed.
    private void createArbitrageCase(DirectionPair directionPair, List<Map.Entry<KeyActionTake, ValueActionTake>> actionsTake) {
        if (preferencesApp.getArbitrageMode() == PreferencesApp.ArbitrageMode.OBSERVE) {
            Map<Integer, Map.Entry<KeyActionTake, ValueActionTake>> actionsTakeMap = new HashMap<>();
            int commID = 0;

            for (Map.Entry<KeyActionTake, ValueActionTake> entryActionTake : actionsTake) {
                actionsTakeMap.put(commID++, entryActionTake);
            }

            ArbitrageCase arbitrageCase = new ArbitrageCase(true, directionPair, actionsTakeMap);
            onArbitrageCaseCompleted(arbitrageCase);
        } else {
            arbitrageBlocks.add(directionPair);

            int arbitrageCaseID = this.arbitrageCaseID++;
            Map<Integer, Map.Entry<KeyActionTake, ValueActionTake>> actionsTakeMap = new HashMap<>();

            for (Map.Entry<KeyActionTake, ValueActionTake> entryActionTake : actionsTake) {
                KeyActionTake keyActionTake = entryActionTake.getKey();
                ValueActionTake valueActionTake = entryActionTake.getValue();
                Direction direction = marketplacesManager.getOppositeDirection(keyActionTake.direction);
                int commID = keyActionTake.marketplace.request(this, new Marketplace.RequestDataTake(direction, valueActionTake.amountBase, valueActionTake.rate));
                // Convert request parameters to expected results.
                if (keyActionTake.direction.ask) {
                    valueActionTake.amountQuote = valueActionTake.amountQuote.negate();
                } else {
                    valueActionTake.amountBase  = valueActionTake.amountBase .negate();
                }
                sessionsManager.recordPlanned(keyActionTake.marketplace.type, directionPair, valueActionTake.amountBase, valueActionTake.amountQuote);
                actionsTakeMap.put(commID, entryActionTake);
                ActionFollowUp actionFollowUp = new ActionFollowUpArbitrage(arbitrageCaseID);
                actionFollowUps.put(commID, actionFollowUp);
            }

            ArbitrageCase arbitrageCase = new ArbitrageCase(false, directionPair, actionsTakeMap);
            arbitrageCases.put(arbitrageCaseID, arbitrageCase);
        }
    }

    private void onArbitrageCaseResult(int arbitrageCaseID, Connector.EventResult eventResult) {
        // Find
        ArbitrageCase arbitrageCase = arbitrageCases.get(arbitrageCaseID);
        if (arbitrageCase != null) {
            arbitrageCase.resultsData.put(eventResult.commID, eventResult.resultData);
            // If all results are in,
            if (arbitrageCase.resultsData.size() == arbitrageCase.actionsTake.size()) {
                onArbitrageCaseCompleted(arbitrageCase);
                arbitrageCases.remove(arbitrageCaseID);
            }
        }
    }

    private static final TableMono.CellStyle cellStyleColTitle    = new TableMono.CellStyle(TableMono.CellStyle.Type.STRING, null, TableMono.CellStyle.Align.LEFT);
    private static final TableMono.CellStyle cellStyleMarketplace = new TableMono.CellStyle(TableMono.CellStyle.Type.STRING, null, TableMono.CellStyle.Align.LEFT);
    private static final TableMono.CellStyle cellStyleCompleted   = new TableMono.CellStyle(TableMono.CellStyle.Type.PERCENTAGE, null, TableMono.CellStyle.Align.RIGHT);
    private static final TableMono.CellStyle cellStyleAmount      = new TableMono.CellStyle(TableMono.CellStyle.Type.BIGDECIMAL, new TableMono.CellStyle.TypeParamsBigDecimal(true), TableMono.CellStyle.Align.LEFT);

    private void onArbitrageCaseCompleted(ArbitrageCase arbitrageCase) {
        DirectionPair directionPair = arbitrageCase.directionPair;

        if (arbitrageCase.observe) {
            long timeNow = System.currentTimeMillis();
            if ((arbitrageTimeBalanceSend == 0) || (timeNow - arbitrageTimeBalanceSend > 60000)) {
//                telegramBotLaunch.sendMessage(String.format(Literals.infoTelegramArbitrageTitleObserve, arbitrageCase.directionPair), null);
                arbitrageTimeBalanceSend = timeNow;
            }
        } else {
            boolean succeed = true;

            BigDecimal amountReportedBaseTotal  = BigDecimal.ZERO;
            BigDecimal amountReportedQuoteTotal = BigDecimal.ZERO;

            BigDecimal amountPlannedBaseAbsTotal  = BigDecimal.ZERO;
            BigDecimal amountPlannedQuoteAbsTotal = BigDecimal.ZERO;

            ScaleOp scaleOpBaseMax  = null;
            ScaleOp scaleOpQuoteMax = null;
            ScaleOp scaleOpRateMax  = null;

            StringBuilder messageFunds = new StringBuilder();

            final int tableRowCount = arbitrageCase.actionsTake.size() + 2;
            int tableRowIndex;

            TableMono table = new TableMono(tableRowCount, 4);
            table.setCell(0, 0, cellStyleColTitle, Literals.infoTelegramArbitrageColumnTitleMarketplace);
            table.setCell(0, 1, cellStyleColTitle, Literals.infoTelegramArbitrageColumnTitleSuccessRatio);
            table.setCell(0, 2, cellStyleColTitle, String.format(Literals.infoTelegramArbitrageColumnTitleCurrencyDelta, directionPair.base .getName()));
            table.setCell(0, 3, cellStyleColTitle, String.format(Literals.infoTelegramArbitrageColumnTitleCurrencyDelta, directionPair.quote.getName()));

            tableRowIndex = 1;
            for (Integer commID : arbitrageCase.actionsTake.keySet()) {
                // Create shortcuts to objects holding all data.
                KeyActionTake   keyActionTake   = arbitrageCase.actionsTake.get(commID).getKey  ();
                ValueActionTake valueActionTake = arbitrageCase.actionsTake.get(commID).getValue();
                Marketplace.ResultDataTake resultData = (Marketplace.ResultDataTake) arbitrageCase.resultsData.get(commID);

                Marketplace marketplace = keyActionTake.marketplace;
                DirectionPairLocal directionPairLocal = marketplace.getLocal(directionPair);

                BigDecimal amountPlannedBase  = valueActionTake.amountBase;
                BigDecimal amountPlannedQuote = valueActionTake.amountQuote;

                BigDecimal amountReportedBase  = resultData.amountBase;
                BigDecimal amountReportedQuote = resultData.amountQuote;

                scaleOpBaseMax  = (scaleOpBaseMax  == null) ? directionPairLocal.scaleOpBase  : scaleOpBaseMax .max(directionPairLocal.scaleOpBase );
                scaleOpQuoteMax = (scaleOpQuoteMax == null) ? directionPairLocal.scaleOpQuote : scaleOpQuoteMax.max(directionPairLocal.scaleOpQuote);
                scaleOpRateMax  = (scaleOpRateMax  == null) ? directionPairLocal.scaleOpRate  : scaleOpRateMax .max(directionPairLocal.scaleOpRate );

                // If the request hasn't been completed in full,
                if (amountPlannedBase.compareTo(amountReportedBase) != 0) {
                    // Clear success flag.
                    succeed = false;
                }

                // Fill table for failure response.
                table.setCell(tableRowIndex, 0, cellStyleMarketplace, marketplace.type.nameShort);
                table.setCell(tableRowIndex, 1, cellStyleCompleted, amountReportedBase.divide(amountPlannedBase, 2, RoundingMode.FLOOR).movePointRight(2).intValue());
                table.setCell(tableRowIndex, 2, cellStyleAmount, amountPlannedBase);
                table.setCell(tableRowIndex, 3, cellStyleAmount, amountPlannedQuote);
                ++tableRowIndex;

                amountPlannedBaseAbsTotal  = amountPlannedBaseAbsTotal .add(amountPlannedBase .abs());
                amountPlannedQuoteAbsTotal = amountPlannedQuoteAbsTotal.add(amountPlannedQuote.abs());

                amountReportedBaseTotal  = amountReportedBaseTotal .add(amountReportedBase );
                amountReportedQuoteTotal = amountReportedQuoteTotal.add(amountReportedQuote);

                BigDecimal walletBase  = marketplace.purses.get0(directionPair.base );
                BigDecimal walletQuote = marketplace.purses.get0(directionPair.quote);

                // Report turnover.
                sessionsManager.cancelPending(marketplace.type, directionPair, amountPlannedBase, amountPlannedQuote);
                sessionsManager.recordReported(marketplace.type, directionPair, amountReportedBase, amountReportedQuote);
                sessionsManager.recordPurse(marketplace.type, directionPair, walletBase, walletQuote);

                // Generate lines about low/superfluous funds.
                appendFundsMessage(messageFunds, marketplace, directionPair.base,  amountReportedBase,  walletBase );
                appendFundsMessage(messageFunds, marketplace, directionPair.quote, amountReportedQuote, walletQuote);
            }

            table.setCell(tableRowIndex, 2, cellStyleAmount, amountReportedBaseTotal );
            table.setCell(tableRowIndex, 3, cellStyleAmount, amountReportedQuoteTotal);

//            if (succeed) {
//                telegramBotLaunch.sendMessage(String.format(Literals.infoTelegramArbitrageTitleSuccess, Format.toSignedString(amountReportedQuoteTotal.add(scaleOpQuoteMax.mul(amountReportedBaseTotal, scaleOpRateMax.div(amountPlannedQuoteAbsTotal, amountPlannedBaseAbsTotal)))), arbitrageCase.directionPair.quote), null);
//            } else {
//                telegramBotLaunch.sendMessage(Literals.infoTelegramArbitrageTitleFailure + "<code>" + table.toString() + "</code>" + messageFunds.toString(), "HTML");
//            }
        }

        arbitrageBlocks.remove(directionPair);
    }

    private void appendFundsMessage(StringBuilder messageFunds, Marketplace marketplace, Currency currency, BigDecimal amountReported, BigDecimal wallet) {
        if (amountReported.signum() > 0) {
            BigDecimal walletLimit = preferencesApp.getSuperfluousFunds(currency);
            if (walletLimit.signum() > 0 && wallet.compareTo(walletLimit) > 0) {
                messageFunds.append(String.format(Literals.infoTelegramSuperfluousFunds, marketplace.type.nameShort, wallet, currency.getName()));
            }
        }
        if (amountReported.signum() < 0) {
            BigDecimal walletLimit = preferencesApp.getLowFunds        (currency);
            if (walletLimit.signum() > 0 && wallet.compareTo(walletLimit) < 0) {
                messageFunds.append(String.format(Literals.infoTelegramLowFunds,         marketplace.type.nameShort, wallet, currency.getName()));
            }
        }
    }

    //////////////////////////////////////////////////
    // Test mode.

    public enum TestMode {
        DEFAULT,
        TESTDECIDE1,
        TESTDECIDE2,
        TESTARBITRAGECOMPLETED,
        DISABLED
    };

    private TestMode testMode;
    private boolean testOneTime;

    public void setTestMode(TestMode testMode) {
        this.testMode = testMode;

        if (this.testMode == TestMode.TESTDECIDE1) {
            testOneTime = true;
        }
        if (this.testMode == TestMode.TESTDECIDE2) {
            testOneTime = true;
        }

        if (testMode == TestMode.TESTARBITRAGECOMPLETED) {
            testArbitrageCompleted();
        }
    }

    private void testDecide1(Marketplace.Type marketplaceTypeSource, Direction directionSource) {
        if (testOneTime) {
            Marketplace marketplaceSource = marketplacesManager.marketplaces.get(marketplaceTypeSource);
            if (marketplaceSource == null) {
                return;
            }

            Market marketSource = marketplaceSource.marketsManager.markets.get(directionSource);
            if (marketSource == null) {
                return;
            }

            Offer offerTop = marketSource.getOfferTop();
            if (offerTop == null) {
                return;
            }

            Currency currencyBase = directionSource.directionPair.base;
            BigDecimal amountBase = offerTop.getAmountBase();

            BigDecimal amountBaseMax = BigDecimal.ZERO;
            if (currencyBase.matches("BTC")) {
                amountBaseMax = new BigDecimal("0.01");
            }
            if (currencyBase.matches("USDT")) {
                amountBaseMax = new BigDecimal("50");
            }
            if (currencyBase.matches("SNM")) {
                amountBaseMax = new BigDecimal("500");
            }

            if (amountBase.compareTo(amountBaseMax) < 0) {
                marketplaceSource.request(this, new Marketplace.RequestDataTake(marketplacesManager.getOppositeDirection(directionSource), amountBase.multiply(new BigDecimal("2")), offerTop.getRate()));
                testOneTime = false;
            }
        }
    }

    private void testDecide2(Marketplace.Type marketplaceTypeSource, Direction directionSource) {
        if (testOneTime) {
            Marketplace marketplaceSource = marketplacesManager.marketplaces.get(marketplaceTypeSource);
            if (marketplaceSource == null) {
                return;
            }

            Market marketSource = marketplaceSource.marketsManager.markets.get(directionSource);
            if (marketSource == null) {
                return;
            }

            Offer offerTop = marketSource.getOfferTop();
            if (offerTop == null) {
                return;
            }

            Currency currencyBase = directionSource.directionPair.base;
            BigDecimal amountBase = BigDecimal.ZERO;
            if (currencyBase.matches("BTC")) {
                return;
            }
            if (currencyBase.matches("USDT")) {
                amountBase = new BigDecimal("50");
            }
            if (currencyBase.matches("SNM")) {
                amountBase = new BigDecimal("500");
            }

            marketplaceSource.request(this, new Marketplace.RequestDataTake(marketplacesManager.getOppositeDirection(directionSource), amountBase, marketplaceSource.incRate(directionSource, offerTop.getRate(), new BigDecimal("1.03"), RatePolicy.ScaleMode.DEFAULT, 0, ScaleOp.roundingModeDefault)));
            testOneTime = false;
        }
    }

    private void testArbitrageCompleted() {
        Currency currencyTSTA = marketplacesManager.getCurrency("TSTA");
        Currency currencyTSTB = marketplacesManager.getCurrency("TSTB");
        DirectionPair directionPair = marketplacesManager.getDirectionPair(currencyTSTA, currencyTSTB);
        Marketplace marketplaceBinance = marketplacesManager.marketplaces.get(Marketplace.Type.BINANCE);
//        Marketplace marketplaceBittrex = marketplacesManager.marketplaces.get(Marketplace.Type.BITTREX);

        marketplaceBinance.addLocal(currencyTSTA, new CurrencyLocal(8));
        marketplaceBinance.addLocal(currencyTSTB, new CurrencyLocal(8));
//        marketplaceBittrex.addLocal(currencyTSTA, new CurrencyLocal(8));
//        marketplaceBittrex.addLocal(currencyTSTB, new CurrencyLocal(8));
        marketplaceBinance.addLocal(directionPair, new DirectionPairLocal(new BigDecimal("0"), new BigDecimal("0"), 6, 8, 8, BigDecimal.ZERO, BigDecimal.ZERO, BigDecimal.ZERO, BigDecimal.ZERO));
//        marketplaceBittrex.addLocal(directionPair, new DirectionPairLocal(new BigDecimal("0"), new BigDecimal("0"), 8, 8, 8, BigDecimal.ZERO, BigDecimal.ZERO, BigDecimal.ZERO, BigDecimal.ZERO));

        Map<Integer, Map.Entry<KeyActionTake, ValueActionTake>> actionsTake = new HashMap<>();
        actionsTake.put(0, new AbstractMap.SimpleEntry<>(new KeyActionTake(marketplaceBinance, directionPair.ask), new ValueActionTake(new BigDecimal("-100.000000"  ), new BigDecimal( "200.00000000"), new BigDecimal("2.00000000"))));
//        actionsTake.put(1, new AbstractMap.SimpleEntry<>(new KeyActionTake(marketplaceBittrex, directionPair.bid), new ValueActionTake(new BigDecimal( "100.00000000"), new BigDecimal("-180.00000000"), new BigDecimal("1.80000000"))));
        ArbitrageCase arbitrageCase = new ArbitrageCase(false, directionPair, actionsTake);
        Marketplace.ResultDataTake resultDataTake0 = new Marketplace.ResultDataTake(Marketplace.ResultDataTake.Result.SUCCESS, new BigDecimal("-100.000000"  ), new BigDecimal( "200.00000000"));
        Marketplace.ResultDataTake resultDataTake1 = new Marketplace.ResultDataTake(Marketplace.ResultDataTake.Result.SUCCESS, new BigDecimal(  "50.00000000"), new BigDecimal( "-90.00000000"));
//        Marketplace.ResultDataTake resultDataTake1 = new Marketplace.ResultDataTake(Marketplace.ResultDataTake.Result.SUCCESS, new BigDecimal( "100.00000000"), new BigDecimal("-180.00000000"));
        arbitrageCase.resultsData.put(0, resultDataTake0);
        arbitrageCase.resultsData.put(1, resultDataTake1);
        onArbitrageCaseCompleted(arbitrageCase);
    }

}
