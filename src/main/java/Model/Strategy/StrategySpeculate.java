//package Model.Strategy;
//
//import Application.Preferences.PreferencesApp;
//
//import Model.Marketplace.Base.Currency.Direction;
//import Model.Marketplace.Base.Currency.DirectionPair;
//import Model.Marketplace.Base.Market.Offer;
//import Model.Marketplace.Base.Market.Market;
//import Model.Marketplace.Base.Marketplace;
//import Model.Marketplace.MarketplacesManager;
//
//import java.math.BigDecimal;
//import java.util.List;
//
//public class StrategySpeculate {
//
//    public static void decide(List<Strategy.Action> actions, PreferencesApp preferencesApp, MarketplacesManager marketplacesManager, Marketplace.Type marketplaceType, Direction direction1) {
//
//        Marketplace marketplace = marketplacesManager.marketplaces.get(marketplaceType);
//        if (marketplace == null) {
//            return;
//        }
//
//        Direction direction2 = marketplace.libraryDirection.getOpposite(direction1);
//        if (direction2 == null) {
//            return;
//        }
//
//        DirectionPair directionPair = direction1.directionPair;
//
//        Market market1 = marketplace.marketsManager.markets.get(direction1);
//        Market market2 = marketplace.marketsManager.markets.get(direction2);
//        if (market1 == null || market2 == null) {
//            return;
//        }
//
//        Double rateObject = marketplace.rateSource.rates.get(directionPair);
//
//        if (market1.offers.size() == 0
//         || market2.offers.size() == 0
//         || rateObject == null) {
//            actions.add(new Strategy.ActionCancelDirection(marketplace, direction1));
//            actions.add(new Strategy.ActionCancelDirection(marketplace, direction2));
//            return;
//        }
//
//        double rate = rateObject;
//
//        // Buy decision.
//        boolean buyAsk = false;
//        boolean buyBid = false;
//        if ((action = createActionBuy(marketDirectionAsk, marketDirectionBid, true, false)) != null) {
//            actions.add(action);
//            buyAsk = true;
//        }
//        if ((action = createActionBuy(marketDirectionBid, marketDirectionAsk, true, false)) != null) {
//            actions.add(action);
//            buyBid = true;
//        }
//
//        OfferMarket offerUserAskBulk  = user.findFirstUserOfferByType(marketDirectionAsk, OfferMarket.Type.BULK);
//        OfferMarket offerUserAskSlice = user.findFirstUserOfferByType(marketDirectionAsk, OfferMarket.Type.SLICE);
//        OfferMarket offerUserBidBulk  = user.findFirstUserOfferByType(marketDirectionBid, OfferMarket.Type.BULK);
//        OfferMarket offerUserBidSlice = user.findFirstUserOfferByType(marketDirectionBid, OfferMarket.Type.SLICE);
//        OfferBuilder offerRateCreate;
//        OfferBuilder offerRateModify;
//
//        // Bulk decision.
//        if (offerUserAskBulk != null) {
//            offerRateModify = createTestUserOffer(offerUserAskBulk);
//            if (offerRateModify != null && offerRateModify.getRateTarget().compareTo(offerUserAskBulk.getRate()) != 0) {
//                actions.add(new ActionModify(offerUserAskBulk, offerRateModify.getAmountOut(), offerRateModify.getRate(), offerRateModify.getRateTarget()));
//            }
//        }
//
//        if (offerUserBidBulk != null) {
//            offerRateModify = createTestUserOffer(offerUserBidBulk);
//            if (offerRateModify != null && offerRateModify.getRateTarget().compareTo(offerUserBidBulk.getRate()) != 0) {
//                actions.add(new ActionModify(offerUserBidBulk, offerRateModify.getAmountOut(), offerRateModify.getRate(), offerRateModify.getRateTarget()));
//            }
//        }
//
//        // Slice decision.
//        if (offerUserAskSlice != null) {
//            if (!buyAsk) {
//                offerRateModify = createTestUserOffer(offerUserAskSlice);
//                if (offerRateModify != null) {
//                    offerRateCreate = createTestUserOffer(marketDirectionAsk);
//                    BigDecimal amountInCreate = (offerRateCreate == null) ? BigDecimal.ZERO : offerRateCreate.getAmountIn();
//                    if (offerUserAskBulk != null && offerUserAskSlice.getAmountIn().compareTo(amountInCreate.multiply(sliceMergeAndSplitRatio)) < 0) {
//                        actions.add(new ActionMerge(offerUserAskBulk, offerUserAskSlice));
//                        if (offerRateCreate != null) {
//                            actions.add(new ActionSplit(offerUserAskBulk, offerRateCreate.getAmountIn(), offerRateCreate.getAmountOut()));
//                        }
//                    } else if (offerRateModify.getRateTarget().compareTo(offerUserAskSlice.getRate()) != 0) {
//                        actions.add(new ActionModify(offerUserAskSlice, offerRateModify.getAmountOut(), offerRateModify.getRate(), offerRateModify.getRateTarget()));
//                    }
//                } else {
//                    actions.add(new ActionMerge(offerUserAskBulk, offerUserAskSlice));
//                }
//            }
//        } else {
//            if (offerUserAskBulk != null) {
//                offerRateCreate = createTestUserOffer(marketDirectionAsk);
//                if (offerRateCreate != null) {
//                    actions.add(new ActionSplit(offerUserAskBulk, offerRateCreate.getAmountIn(), offerRateCreate.getAmountOut()));
//                }
//            }
//        }
//
//        if (offerUserBidSlice != null) {
//            if (!buyBid) {
//                offerRateModify = createTestUserOffer(offerUserBidSlice);
//                if (offerRateModify != null) {
//                    offerRateCreate = createTestUserOffer(marketDirectionBid);
//                    BigDecimal amountInCreate = (offerRateCreate == null) ? BigDecimal.ZERO : offerRateCreate.getAmountIn();
//                    if (offerUserBidBulk != null && offerUserBidSlice.getAmountIn().compareTo(amountInCreate.multiply(sliceMergeAndSplitRatio)) < 0) {
//                        actions.add(new ActionMerge(offerUserBidBulk, offerUserBidSlice));
//                        if (offerRateCreate != null) {
//                            actions.add(new ActionSplit(offerUserBidBulk, offerRateCreate.getAmountIn(), offerRateCreate.getAmountOut()));
//                        }
//                    } else if (offerRateModify.getRateTarget().compareTo(offerUserBidSlice.getRate()) != 0) {
//                        actions.add(new ActionModify(offerUserBidSlice, offerRateModify.getAmountOut(), offerRateModify.getRate(), offerRateModify.getRateTarget()));
//                    }
//                } else {
//                    actions.add(new ActionMerge(offerUserBidBulk, offerUserBidSlice));
//                }
//            }
//        } else {
//            if (offerUserBidBulk != null) {
//                offerRateCreate = createTestUserOffer(marketDirectionBid);
//                if (offerRateCreate != null) {
//                    actions.add(new ActionSplit(offerUserBidBulk, offerRateCreate.getAmountIn(), offerRateCreate.getAmountOut()));
//                }
//            }
//        }
//
//        if (actions.size() > 0) {
//            log.trace(Literals.logTraceStrategyHasActions);
//            for (Action actionTrace : actions) {
//                log.trace(actionTrace.getDescription());
//            }
//        } else {
//            log.trace(Literals.logTraceStrategyNoActions);
//        }
//    }
//
//    private Strategy.Action createActionBuy(Marketplace marketplace, Direction direction, Market market, boolean repeat, boolean force) {
//
//        // Todo: correctly calculate threshold rate for buying dumb offers.
//        BigDecimal rateDumb = null;
//
//        BigDecimal quantity = BigDecimal.ZERO;
//        BigDecimal rate     = BigDecimal.ZERO;
//
//        for (int indexBuy = 0; repeat || indexBuy == 0; ++indexBuy) {
//            // Следующая лучшая заявка противоположного рынка.
//            Offer offer;
//            try {
//                offer = market.offers.get(indexBuy);
//            } catch (IndexOutOfBoundsException e) {
//                break;
//            }
//            if (offer == null || marketplace.offersPersonal.isPersonal(offer)) {
//                break;
//            }
//
//            // If either force flag is set or tests were passed, create an action.
//            if (force || marketplace.ratePolicy.compareRates(direction, offer.getRate(), rateDumb) >= 0) {
//                quantity = amount.add(offer.getQuantity());
//                rate     =              offer.getRate()     ;
//            } else {
//                break;
//            }
//        }
//
//        if (quantity.compareTo(BigDecimal.ZERO) == 0) {
//            return null;
//        }
//
//        return new Strategy.ActionCreate(marketplace, direction, quantity, rate);
//    }
//}
