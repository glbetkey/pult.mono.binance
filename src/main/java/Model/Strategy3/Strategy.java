package Model.Strategy3;

import Application.Events.EventListenerAsync;
import Application.Preferences.PreferencesApp;
import Model.Marketplace.Base.Connector;
import Model.Marketplace.Base.Currency.Direction;
import Model.Marketplace.Base.Currency.DirectionPair;
import Model.Marketplace.Base.Currency.RatePolicy;
import Model.Marketplace.Base.Market.*;
import Model.Marketplace.Base.Marketplace;
import Model.Marketplace.MarketplacesManager;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.*;


/**
 * Created by FlyingPirate on 18.11.2017.
 */
public class Strategy extends EventListenerAsync {

    protected static Logger log = LogManager.getLogger(Strategy.class.getName());


    private Market marketAsk;
    private Market marketBid;
    private ArrayList<Order> openOrdersAsk = new ArrayList<>();
    private ArrayList<Order> openOrdersBid = new ArrayList<>();
    private Connector connector;
    private MarketplacesManager marketplacesManager;
    private Marketplace marketplace;
    PreferencesApp preferencesApp;

    private BigDecimal minQuantRate;

    private int scaleRate;

    private DirectionPair directionPair;

    private Set<Direction> blockedDirections;
    private HashMap<Integer,Direction> blockingRequestIds;
    private HashMap<Integer,Marketplace.RequestData> pendingRequests;
    private Set<Thread> orderMonitors;

    RateSourceAtDepth rateSourceAtDepth;


    private static final BigDecimal rateSliceDeviation = new BigDecimal("0.0025");
    private static final int rateSliceDeviationScale = 10;

    private static final BigDecimal rateRatioDampSliceMin = BigDecimal.ONE.subtract(rateSliceDeviation);
    private static final BigDecimal rateRatioDampSliceMax = BigDecimal.ONE.add     (rateSliceDeviation);

    private static final BigDecimal sliceMergeAndSplitRatio = new BigDecimal("0.5");
    private boolean toBeInterrupted = false;


    private class OrderMonitor implements Runnable{
        List orderList;
        Order order;
        Direction direction;

        @Override
        public void run() {
            synchronized (order) {
                while (!order.isFinished()) {
                    try {
                        order.wait();
                    } catch (InterruptedException e) {
                        if (Strategy.this.toBeInterrupted)
                            return;
                    }
                    if(order.status== Order.Status.PARTIALLY_FILLED)
                        decidePartiallyFilledOrderCancel(order,direction);
                }
                orderList.remove(order);
                log.info("order finished");
            }
        }

        public OrderMonitor(List orderList, Order order , Direction direction) {
            this.orderList = orderList;
            this.order = order;
            this.direction = direction;
        }
    }
    public Strategy(MarketplacesManager marketplacesManager, Marketplace.Type marketplaceType, DirectionPair directionPair, PreferencesApp preferencesApp) {
        this.marketplacesManager = marketplacesManager;
        this.marketplace = marketplacesManager.marketplaces.get(marketplaceType);
        this.directionPair = directionPair;
        this.preferencesApp = preferencesApp;

        this.connector = this.marketplace.connector;

        marketAsk = marketplace.marketsManager.markets.get(directionPair.ask);
        marketBid = marketplace.marketsManager.markets.get(directionPair.bid);

        scaleRate = marketplace.getLocal(directionPair).scaleOpRate.scale;
        minQuantRate = new BigDecimal("0.1").pow(marketplace.getLocal(directionPair).scaleOpRate.scale);

        rateSourceAtDepth = new RateSourceAtDepth(marketplace);

        openOrdersAsk = new ArrayList<>();
        openOrdersBid = new ArrayList<>();
        synchronized (marketAsk.orders){
            for (Order order : marketAsk.orders.values()) {
                if(order.isActive()){
                    openOrdersAsk.add(order);
                    new Thread(new OrderMonitor(openOrdersAsk,order,marketAsk.direction)).start();
                }
            }
        }
        synchronized (marketBid.orders){
            for (Order order : marketBid.orders.values()) {
                if(order.isActive()){
                    openOrdersBid.add(order);
                    new Thread(new OrderMonitor(openOrdersBid,order,marketBid.direction)).start();
                }
            }
        }

        blockedDirections = new HashSet<>();
        blockingRequestIds = new HashMap<>();
        pendingRequests = new HashMap<>();
        marketplacesManager.addListener(this);
        orderMonitors = new HashSet<>();
    }

    private abstract static class Action {

        abstract Marketplace.ResultData execute(Connector connector);

        abstract String getDescription();

    }

    @Override
    protected void listenAsync(Event event) {
        if (event instanceof Connector.EventMarketChanged) {
            onEventMarketChanged((Connector.EventMarketChanged) event);
        }
        if (event instanceof Connector.EventResult) {
            onConnectorResult((Connector.EventResult) event);
        }
    }

    private void request(Marketplace.RequestData requestData,Direction direction){
        int commID = marketplace.request(this, requestData);
        blockedDirections.add(direction);
        blockingRequestIds.put(commID, direction);
        pendingRequests.put(commID, requestData);
    }
    private  void decidePartiallyFilledOrderCancel(Order order, Direction direction) {
        BigDecimal sizePreference = preferencesApp.getSize(direction);
            if (order.amount.subtract(order.filled).compareTo(sizePreference.multiply(new BigDecimal(0.4))) < 0) {
                request(new Marketplace.RequestDataCancel(direction,order),direction);
                log.trace("Cancelling order: amount < prefAmount");
            }
    }

    private void onEventMarketChanged(Connector.EventMarketChanged eventMarketChanged) {
        Market market = eventMarketChanged.direction.ask? marketAsk : marketBid;
        decide(market,eventMarketChanged.direction.ask? openOrdersAsk:openOrdersBid);
    }

    private void decide(Market market, ArrayList<Order> openOrders){
        if (blockedDirections.contains(market.direction))
            return;

        String msg = market.direction.ask ?"ask market: ":"bid market: ";

        BigDecimal amountPref = preferencesApp.getSize(market.direction);
        double depth = preferencesApp.getPlast(market.direction);
        boolean around = preferencesApp.getAround(market.direction);

        boolean orderExists = false;
        Order order = null;
//        synchronized (openOrders){
            if(openOrders.size()>1){
                //todo go safe?
                log.error(msg+ "decide go safe");
            }
            orderExists  = openOrders.size() > 0;
            if(orderExists){
                order = openOrders.get(0);
            }
//        }
        msg+= "orderExists = "+orderExists+"; ";
        Marketplace.RequestData requestData = null;
        if(orderExists){//todo чекнуть на реверс орбиту
//            synchronized (order){
                BigDecimal bestRate = calcBestRateAroundDepth(market,depth,around);
                if(bestRate==null)
                    return;
                int sign = bestRate.compareTo(order.rate);
                if (sign!=0){
                    BigDecimal rateRatio = bestRate.divide(order.rate, scaleRate,
                            (sign > 0) ? RoundingMode.CEILING : RoundingMode.FLOOR);
                    if (rateRatio.compareTo(rateRatioDampSliceMin) < 0
                            || rateRatio.compareTo(rateRatioDampSliceMax) > 0) {
                        requestData = new Marketplace.RequestDataCancel(market.direction, order);
                        msg += "decide cancel";
                    } else {
                        msg += "decide NOT cancel";
                    }
                }
//            }
        }else{
            BigDecimal orbit = market.direction.ask? preferencesApp.getOrbitMin(directionPair.base) : preferencesApp.getOrbitMax(directionPair.base);
            BigDecimal reverseOrbit = market.direction.ask? preferencesApp.getOrbitMax(directionPair.base) : preferencesApp.getOrbitMin(directionPair.base);
            BigDecimal currAsset = marketplace.purses.get0(directionPair.base);
            boolean beyondOrbit =  market.direction.ask? currAsset.compareTo(orbit)<0: currAsset.compareTo(orbit)>0;
            if(beyondOrbit){
                if ( market.direction.ask?currAsset.compareTo(orbit.multiply(new BigDecimal(0.7)))<0:currAsset.compareTo(orbit.divide(new BigDecimal(0.7),RoundingMode.FLOOR))>0){
                    log.info(msg+" decide do nothing - beyond orbit");
                    return;// мы не торгуем, если вышли совсем уж за орбиту
                }
                amountPref = amountPref.multiply(new BigDecimal(0.25)); //todo множим на правильный коеффициент
                amountPref = amountPref.setScale(marketplace.getLocal(directionPair).scaleOpBase.scale,RoundingMode.CEILING);//todo floor?
            }
            BigDecimal bestRate = calcBestRateAroundDepth(market,depth,around);

            if(bestRate==null || bestRate.multiply(amountPref).compareTo(marketplace.getLocal(directionPair).amountQuoteMin)<0){
                log.info(msg+" decide do nothing - less than min total");
                return;//на случай, если амаунт стал слишком мал
            }
            BigDecimal capRate = rateSourceAtDepth.rateAtPlast(directionPair,preferencesApp.getRateSourcePlast(directionPair));
            capRate = capRate.multiply(preferencesApp.getMargin(market.direction));

            if(market.direction.ask? bestRate.compareTo(capRate)<0:bestRate.compareTo(capRate)<0){
                if (market.direction.ask?currAsset.compareTo(reverseOrbit)<0:currAsset.compareTo(reverseOrbit)>0){
                    log.info(msg+" decide do nothing - stopped at capRate");
                    return;
                }
                //мы ниже синего блока, но нам надо продавать/покупать, потому продолжаем.
            }

            requestData = new Marketplace.RequestDataMake(market.direction,amountPref,bestRate);
            msg+="decide create";
        }
        if (requestData != null) {
            request(requestData,market.direction);
        }
        log.info("----------->"+msg);
    }

    private void onConnectorResult(Connector.EventResult eventResult) {
        Marketplace.ResultData resultData = eventResult.resultData;
        if (resultData instanceof Marketplace.ResultDataCancel){
            onResultCancel(eventResult.commID, (Marketplace.ResultDataCancel) resultData);
        }
        if (resultData instanceof Marketplace.ResultDataMake){
            onResultMake(eventResult.commID, (Marketplace.ResultDataMake) resultData);
        }
    }

    private void onResultCancel(int commID, Marketplace.ResultDataCancel resultData) {
        Direction direction = blockingRequestIds.remove(commID);
        Marketplace.RequestDataCancel cancelRequest = (Marketplace.RequestDataCancel) pendingRequests.remove(commID);
        if(resultData== Marketplace.ResultDataCancel.FAILURE){
            log.error("FAILURE CANCEL ORDER "+ cancelRequest.order.status+ " trying to fix it");
//            Market market = cancelRequest.direction.ask?marketAsk:marketBid;
//            ArrayList<Order> list =  cancelRequest.direction.ask? openOrdersAsk:openOrdersBid;
//            synchronized (list){
//                list.clear();
//                synchronized (market.orders) {
//                    for (Order order : market.orders.values()) {
//                        if (order.isActive()){
//                            list.add(order);
//                        new Thread(new OrderMonitor(list,order,direction)).start();}
//                    }
//                }
//            }
        }
        blockedDirections.remove(direction);
    }

    private void onResultMake(int commID, Marketplace.ResultDataMake resultData) {
        Direction direction = blockingRequestIds.remove(commID);

        if (resultData!= Marketplace.ResultDataMake.FAILURE) {
            Order newOrder = resultData.order;
            ArrayList<Order> openOrdersList = direction.ask ? openOrdersAsk : openOrdersBid;
//            synchronized (openOrdersList) {
                openOrdersList.add(newOrder);
                new Thread(new OrderMonitor(openOrdersList,newOrder,direction)).start();
                if (openOrdersList.size() > 1) {
                    //todo go safe
                }
//            }
        }
        pendingRequests.remove(commID);
        blockedDirections.remove(direction);
    }

    @Override
    public  void stop(){
        toBeInterrupted = true;
        for (Thread orderMonitor : orderMonitors) {
            if(orderMonitor.isAlive())
                orderMonitor.interrupt();
        }
        super.stop();
    }
    private BigDecimal calcBestRateAroundDepth(Market market, double depth, boolean around) {
        // Get market section at given depth.
        Section section = new Section();
        section.atDepth(market, depth, marketplace.ratePolicy);
//        Market.Section section = market.getSectionAtDepth(depth);
        // Get an offer at that section.
        Offer offerDepth = section.offer;
        BigDecimal precise = offerDepth.getRate();
//        OfferMarket offerDepth = section.offer;
        // If it exists,
        if (offerDepth != null) {
            // Proceed with the main calculations.
            final boolean ask = market.direction.ask;
            // Get a rate on top of that offer.
            BigDecimal rateDepth = calcRateAbove(offerDepth.getRate(), ask);
            // If looking around isn't requested,
            if (!around) {
                // Return simple rate at given depth.
                return rateDepth;
            }
            // Find the boundaries of the rate range.
            BigDecimal rateDepthTop = marketplace.incRate(market.direction, rateDepth, rateSliceDeviation.add(BigDecimal.ONE), RatePolicy.ScaleMode.DEFAULT,scaleRate, RoundingMode.FLOOR);
            BigDecimal rateDepthBot = marketplace.decRate(market.direction, rateDepth, rateSliceDeviation.add(BigDecimal.ONE), RatePolicy.ScaleMode.DEFAULT,scaleRate, RoundingMode.FLOOR);
//            BigDecimal rateDepthMin = marketplace.decRate(rateDepth, rateSliceDeviation.add(BigDecimal.ONE));
//            BigDecimal rateDepthMax = Offer.multiplyScaleForRate(rateDepth, rateSliceDeviation.add(BigDecimal.ONE));
//            BigDecimal rateDepthTop = ask ? rateDepthMin : rateDepthMax;
//            BigDecimal rateDepthBot = ask ? rateDepthMax : rateDepthMin;

            // Get market section at the top of the rate range.
            section = market.getSectionAtRate(rateDepthTop,marketplace.ratePolicy);
            // The offer at that section is guaranteed to exist.
            Offer offerNext = section.offer;
            // The best offer is currently the first offer.
            Offer offerBest = offerNext;
            BigDecimal sumBase = BigDecimal.ZERO;

            BigDecimal ratioRateDepth = rateSliceDeviation.divide(new BigDecimal(depth), rateSliceDeviationScale, RoundingMode.UP);

            // Repeat until there are offers left.
            synchronized (market.offers) {
                while (section.iteratorOffer.hasNext()) {
                    // All personal offers should be skipped.
                        // Add last offer's base amount to the total base amount change.
                    sumBase = sumBase.add(extractPersonal(offerNext,ask));
                    // Go to the next offer.
                    offerNext = section.iteratorOffer.next();
                    if(sumBase.compareTo(BigDecimal.ZERO)==0)
                        continue;
                    if (extractPersonal(offerNext,ask).compareTo(BigDecimal.ZERO)!=0) {
                        // True if need to stop after this iteration.
                        boolean stop = false;
                        // Next offer's rate.
                        BigDecimal rateOfferNext = offerNext.getRate();
                        // If the offer falls below rate range,
                        if (marketplace.compareRates(market.direction, rateOfferNext, rateDepthBot) < 0) {
                            // Stop after this iteration.
                            stop = true;
                            // Substitute next offer's rate with limiting rate.
                            rateOfferNext = rateDepthBot;
                        }
                        // Calculate total rate change as the difference between current rate and best rate.
                        BigDecimal sumRate = rateOfferNext.subtract(offerBest.getRate()).abs().divide(rateOfferNext, rateSliceDeviationScale, RoundingMode.UP);//todo спросить?
                        // If the total rate change per total base amount change is better than the threshold,
                        if (sumRate.divide(sumBase, rateSliceDeviationScale, RoundingMode.UP).compareTo(ratioRateDepth) > 0) {
                            // Assume new best offer.
                            offerBest = offerNext;
                            sumBase = BigDecimal.ZERO;
                        }
                        // If the offer falls below rate range,
                        if (stop) {
                            // Stop.
                            break;
                        }
                }
            }
            log.info((ask?"ASK":"BID")+" around depth = "+precise+" --->"+ offerBest.getRate());
                // Assume the rate on top of the best offer.
                return calcRateAbove(offerBest.getRate(), ask);
            }
        }

        // If there's at least one offer,
        offerDepth = market.getOfferBot();
        if (offerDepth != null) {
            // Assume the rate of the bottom offer.
            return offerDepth.getRate();
        }

        // If there are no offers on the market, a new offer cannot be created.
        return null;
    }


    // Get the closest rate above provided rate. The provided rate is assumed to be of the regular scale. If it's not,
    // it's reduced to the regular scale first.
    private BigDecimal calcRateAbove(BigDecimal rate, boolean ask) {
        BigDecimal rateRound = rate.setScale(scaleRate,RoundingMode.FLOOR);
        return ask ? rateRound.subtract(minQuantRate) : rateRound.add(minQuantRate);
    }

    /**
     * Возвращаем оффер амаунтБейсесли не содержит нашего ордера, а если содержит - возвращаем амаунтБейс оффера БЕЗ нашего оредра в нем.
     * @param offer
     * @param ask
     * @return
     */
    private BigDecimal extractPersonal(Offer offer, boolean ask){
        ArrayList<Order> openOrdersList = ask? openOrdersAsk: openOrdersBid;
//        synchronized (openOrdersList){
            if (openOrdersList.size()>0){
                Order order = openOrdersList.get(0);
                synchronized (order){
                    if(order.rate.compareTo(offer.getRate())==0){
                        //если рейты совпадают - наш ордер находится в этом оффере, и мы возвращаем амаунт оффера БЕЗ нашего ордера.
                        return offer.getAmountBase().subtract(order.amount.subtract(order.filled));
                    }
                }
            }
            return offer.getAmountBase();
//        }
    }
}
