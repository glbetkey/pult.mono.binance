package Model.Stats;

import Application.Literals;
import Model.Marketplace.Base.Currency.Currency;
import Model.Marketplace.Base.Marketplace;
import Model.Marketplace.MarketplacesManager;
import Utils.Format;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.KeyDeserializer;

import java.io.IOException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

public class Session {

    public enum Type {
        HOURLY  (true,  true ),
        DAILY   (true,  true ),
        MONTHLY (true,  true ),
        LIFETIME(true,  false),
        ONTIME  (false, false);

        boolean saved;
        boolean periodical;

        Type(boolean saved, boolean periodical) {
            this.saved = saved;
            this.periodical = periodical;
        }
    }

    public static class TurnoverKey {
        public Marketplace.Type marketplaceType;
        public Currency currency;

        public static class _KeyDeserializer extends KeyDeserializer {
            private MarketplacesManager marketplacesManager;

            _KeyDeserializer(MarketplacesManager marketplacesManager) {
                this.marketplacesManager = marketplacesManager;
            }

            @Override
            public Object deserializeKey(final String key, final DeserializationContext context) throws IOException {
                String[] split = key.split("\\W+");
                if (split.length != 2) {
                    throw new IOException();
                }

                try {
                    return new TurnoverKey(Marketplace.Type.valueOf(split[0]), marketplacesManager.getCurrency(split[1]));
                } catch (IllegalArgumentException e) {
                    throw new IOException();
                }
            }
        }

        public TurnoverKey() {
        }

        public TurnoverKey(Marketplace.Type marketplaceType, Currency currency) {
            this.marketplaceType = marketplaceType;
            this.currency = currency;
        }

        public TurnoverKey(TurnoverKey other) {
            marketplaceType = other.marketplaceType;
            currency        = other.currency;
        }

        @Override
        public boolean equals(Object other) {
            return (other instanceof TurnoverKey) && matches((TurnoverKey) other);
        }

        @Override
        public int hashCode() {
            return Objects.hash(marketplaceType, currency);
        }

        public boolean matches(TurnoverKey other) {
            return marketplaceType == other.marketplaceType && currency.matches(other.currency);
        }

        @Override
        public String toString() { return getName(); }

        public String getName() {
            return String.format(Literals.nameTurnoverKey, marketplaceType, currency.getName());
        }
    }

    public static class TurnoverValue {
        public BigDecimal plannedIn;
        public BigDecimal plannedOut;

        @JsonIgnore
        public BigDecimal pendingIn;
        @JsonIgnore
        public BigDecimal pendingOut;

        public BigDecimal reportedIn;
        public BigDecimal reportedOut;

        public BigDecimal purseDeltaIn;
        public BigDecimal purseDeltaOut;

        public TurnoverValue() {
            plannedIn = BigDecimal.ZERO;
            plannedOut = BigDecimal.ZERO;

            pendingIn = BigDecimal.ZERO;
            pendingOut = BigDecimal.ZERO;

            reportedIn = BigDecimal.ZERO;
            reportedOut = BigDecimal.ZERO;

            purseDeltaIn = BigDecimal.ZERO;
            purseDeltaOut = BigDecimal.ZERO;
        }
    }

    public Type type;

    public long timeStart;
    public long timeStop;

    public Map<TurnoverKey, TurnoverValue> turnovers;

    public BigDecimal capitalizationInitial;
    public BigDecimal capitalizationCurrent;

    Session() {
    }

    Session(Type type) {
        this.type = type;

        turnovers = new HashMap<>();

        capitalizationCurrent = BigDecimal.ZERO;
        capitalizationInitial = BigDecimal.ZERO;
    }

    public void start() { timeStart = System.currentTimeMillis(); }

    public void stop() { timeStop = System.currentTimeMillis(); }

    public void reset() {
        timeStart = System.currentTimeMillis();
        timeStop  = 0;

        turnovers.clear();

        capitalizationInitial = BigDecimal.ZERO;
        capitalizationCurrent = BigDecimal.ZERO;
    }

    @JsonIgnore
    public long getLength() {
        return ((timeStop == 0) ? System.currentTimeMillis() : timeStop) - timeStart;
    }

    void recordPlanned(TurnoverKey turnoverKey, BigDecimal amount) {
        TurnoverValue turnoverValue = getTurnoverValueSafe(turnoverKey);
        if (amount.signum() < 0) {
            turnoverValue.plannedIn  = turnoverValue.plannedIn .add(amount);
            turnoverValue.pendingIn  = turnoverValue.pendingIn .add(amount);
        } else {
            turnoverValue.plannedOut = turnoverValue.plannedOut.add(amount);
            turnoverValue.pendingOut = turnoverValue.pendingOut.add(amount);
        }
    }

    void cancelPending(TurnoverKey turnoverKey, BigDecimal amount) {
        TurnoverValue turnoverValue = getTurnoverValueSafe(turnoverKey);
        if (amount.signum() < 0) {
            turnoverValue.pendingIn  = turnoverValue.pendingIn .subtract(amount);
        } else {
            turnoverValue.pendingOut = turnoverValue.pendingOut.subtract(amount);
        }
    }

    void recordReported(TurnoverKey turnoverKey, BigDecimal amount) {
        TurnoverValue turnoverValue = getTurnoverValueSafe(turnoverKey);
        if (amount.signum() < 0) {
            turnoverValue.reportedIn  = turnoverValue.reportedIn .add(amount);
        } else {
            turnoverValue.reportedOut = turnoverValue.reportedOut.add(amount);
        }
    }

    void recordPurseDelta(TurnoverKey turnoverKey, BigDecimal purseDelta) {
        TurnoverValue turnoverValue = getTurnoverValueSafe(turnoverKey);
        if (purseDelta.signum() < 0) {
            turnoverValue.purseDeltaIn  = turnoverValue.purseDeltaIn .add(purseDelta);
        } else {
            turnoverValue.purseDeltaOut = turnoverValue.purseDeltaOut.add(purseDelta);
        }
    }

    private TurnoverValue getTurnoverValueSafe(TurnoverKey turnoverKey) {
        TurnoverValue turnoverValue = turnovers.get(turnoverKey);
        if (turnoverValue == null) {
            turnoverValue = new TurnoverValue();
            turnovers.put(new TurnoverKey(turnoverKey), turnoverValue);
        }
        return turnoverValue;
    }

    public void recordCapitalization(BigDecimal capitalization) {
        if (capitalizationInitial.compareTo(BigDecimal.ZERO) == 0) {
            capitalizationInitial = capitalization;
        }

        capitalizationCurrent = capitalization;
    }

    @JsonIgnore
    public BigDecimal getCapitalizationChange() {
        return capitalizationCurrent.subtract(capitalizationInitial);
    }

    public String generateInfo() {
        StringBuilder messageInfo = new StringBuilder();
        boolean hasCurrencies = false;

        // Reorganize by currency.
        Map<Currency, Map<Marketplace.Type, TurnoverValue>> turnoversCurrencies = new HashMap<>();
        for (Map.Entry<TurnoverKey, TurnoverValue> turnoverEntry : turnovers.entrySet()) {
            Currency currency = turnoverEntry.getKey().currency;
            Map<Marketplace.Type, TurnoverValue> turnoversCurrency = turnoversCurrencies.computeIfAbsent(currency, (k) -> new HashMap<Marketplace.Type, TurnoverValue>());
            turnoversCurrency.put(turnoverEntry.getKey().marketplaceType, turnoverEntry.getValue());
        }

        // Append title.
        messageInfo.append(String.format(Literals.sessionInfoTitle, type.toString()));

        // Cycle over all currencies.
        for (Map.Entry<Currency, Map<Marketplace.Type, TurnoverValue>> turnoverCurrenciesEntry : turnoversCurrencies.entrySet()) {
            // Append currency name.
            messageInfo.append(String.format(Literals.sessionInfoCurrency, turnoverCurrenciesEntry.getKey().getName()));

            // Calculate maximal scale for every value type.
            int scaleReported = 0;
            int scalePurseDelta = 0;

            for (Map.Entry<Marketplace.Type, TurnoverValue> turnoverCurrencyEntry : turnoverCurrenciesEntry.getValue().entrySet()) {
                TurnoverValue turnoverValue = turnoverCurrencyEntry.getValue();
                scaleReported   = Math.max(scaleReported,   Math.max(turnoverValue.reportedIn  .scale(), turnoverValue.reportedOut  .scale()));
                scalePurseDelta = Math.max(scalePurseDelta, Math.max(turnoverValue.purseDeltaIn.scale(), turnoverValue.purseDeltaOut.scale()));
            }

            BigDecimal plannedInTotal     = BigDecimal.ZERO;
            BigDecimal plannedOutTotal    = BigDecimal.ZERO;
            BigDecimal reportedInTotal    = BigDecimal.ZERO;
            BigDecimal reportedOutTotal   = BigDecimal.ZERO;
            BigDecimal purseDeltaInTotal  = BigDecimal.ZERO;
            BigDecimal purseDeltaOutTotal = BigDecimal.ZERO;

            // Append entry for every marketplace where the current currency was traded.
            for (Map.Entry<Marketplace.Type, TurnoverValue> turnoverCurrencyEntry : turnoverCurrenciesEntry.getValue().entrySet()) {
                TurnoverValue turnoverValue = turnoverCurrencyEntry.getValue();
                appendInfoCurrencyTurnover(messageInfo, turnoverCurrencyEntry.getKey(), scaleReported, scaleReported,
                        turnoverValue.reportedIn, turnoverValue.reportedOut, turnoverValue.plannedIn, turnoverValue.plannedOut, turnoverValue.purseDeltaIn, turnoverValue.purseDeltaOut);

                plannedInTotal     = plannedInTotal    .add(turnoverValue.plannedIn    );
                plannedOutTotal    = plannedOutTotal   .add(turnoverValue.plannedOut   );
                reportedInTotal    = reportedInTotal   .add(turnoverValue.reportedIn   );
                reportedOutTotal   = reportedOutTotal  .add(turnoverValue.reportedOut  );
                purseDeltaInTotal  = purseDeltaInTotal .add(turnoverValue.purseDeltaIn );
                purseDeltaOutTotal = purseDeltaOutTotal.add(turnoverValue.purseDeltaOut);

                hasCurrencies = true;
            }

            // Append entry for the total amounts.
            appendInfoCurrencyTurnover(messageInfo, null, scaleReported, scaleReported,
                    reportedInTotal, reportedOutTotal, plannedInTotal, plannedOutTotal, purseDeltaInTotal, purseDeltaOutTotal);
        }

        if (!hasCurrencies) {
            messageInfo.append(Literals.sessionInfoCurrencyTurnoverNone);
        }

        return messageInfo.toString();
    }

    private void appendInfoCurrencyTurnover(StringBuilder messageInfo, Marketplace.Type marketplaceType, int scaleReported, int scalePurseDelta,
                                            BigDecimal reportedIn, BigDecimal reportedOut,
                                            BigDecimal plannedIn, BigDecimal plannedOut,
                                            BigDecimal purseDeltaIn, BigDecimal purseDeltaOut) {
        BigDecimal reportedInAbs  = reportedIn .abs();
        BigDecimal reportedOutAbs = reportedOut.abs();
        if (marketplaceType == null) {
            messageInfo.append(String.format(Literals.sessionInfoCurrencyTurnoverAll,
                    reportedInAbs .setScale(scaleReported).toPlainString(), Format.toPercentage(reportedIn,  plannedIn ),
                    reportedOutAbs.setScale(scaleReported).toPlainString(), Format.toPercentage(reportedOut, plannedOut),
                    Format.toSignedString(purseDeltaOut.add(purseDeltaIn).setScale(scalePurseDelta, RoundingMode.HALF_EVEN)),
                    Format.toPercentage(reportedOutAbs.add(reportedInAbs), purseDeltaOut.abs().add(purseDeltaIn.abs()))));
        } else {
            messageInfo.append(String.format(Literals.sessionInfoCurrencyTurnover, marketplaceType.nameShort,
                    reportedInAbs .setScale(scaleReported).toPlainString(), Format.toPercentage(reportedIn,  plannedIn ),
                    reportedOutAbs.setScale(scaleReported).toPlainString(), Format.toPercentage(reportedOut, plannedOut),
                    Format.toSignedString(purseDeltaOut.add(purseDeltaIn).setScale(scalePurseDelta, RoundingMode.HALF_EVEN)),
                    Format.toPercentage(reportedOutAbs.add(reportedInAbs), purseDeltaOut.abs().add(purseDeltaIn.abs()))));
        }
    }
}
