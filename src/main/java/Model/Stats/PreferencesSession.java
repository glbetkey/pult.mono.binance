package Model.Stats;

import Application.Preferences.Preferences;
import com.fasterxml.jackson.core.JsonFactory;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.JsonNodeFactory;
import com.fasterxml.jackson.databind.node.ObjectNode;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class PreferencesSession extends Preferences {

    protected static Logger log = LogManager.getLogger(PreferencesSession.class.getName());

    private static final String nameTimeStart = "timestart";
    private static final String nameTimeStop  = "timestop";

    private static final String nameTurnovers = "turnovers";
    private static final String nameTurnoversBase      = "base";
    private static final String nameTurnoversQuote     = "quote";
    private static final String nameTurnoversAsk       = "ask";
    private static final String nameTurnoversActive    = "active";
    private static final String nameTurnoversAmountIn  = "amountin";
    private static final String nameTurnoversAmountOut = "amountout";

    private static final String nameDeltas = "deltas";
    private static final String nameDeltasCurrency = "currency";
    private static final String nameDeltasValueN   = "valueN";
    private static final String nameDeltasValueP   = "valueP";

    private static final String nameCapitalizationTotalInitial = "capitalizationtotalinitial";
    private static final String nameCapitalizationTotalCurrent = "capitalizationtotalcurrent";

    private String sessionName;

    public PreferencesSession(JsonFactory jsonFactory, ObjectMapper objectMapper, JsonNodeFactory jsonNodeFactory) {
        super(jsonFactory, objectMapper, jsonNodeFactory);
    }

    //////////////////////////////////////////////////
    //

    private JsonNode getNodeSessionType(Session.Type sessionType) {
        return path(getNodeMain(), sessionType.toString());
    }

    public void setSession(Session.Type sessionType, Session session) {
        ((ObjectNode) getNodeMain()).putPOJO(sessionType.toString(), session);
        saveAuto();
    }

    public Session getSession(Session.Type sessionType) {
        try {
            return objectMapper.treeToValue(getNodeMain().path(sessionType.toString()), Session.class);
        } catch (JsonProcessingException e) {
            return null;
        }
    }

}
