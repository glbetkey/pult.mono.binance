package Model.Stats;

import Application.Events.EventDispatcher;
import Application.Events.EventListener;
import Application.Events.EventListenerAsync;
import Application.Timer;
import Model.Marketplace.Base.Connector;
import Model.Marketplace.Base.Currency.Currency;
import Model.Marketplace.Base.Currency.DirectionPair;
import Model.Marketplace.Base.Marketplace;
import Model.Marketplace.Base.Purses;
import Model.Marketplace.MarketplacesManager;
import View.MainWindowController;
import com.fasterxml.jackson.core.JsonFactory;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.module.SimpleModule;
import com.fasterxml.jackson.databind.node.JsonNodeFactory;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.math.BigDecimal;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.temporal.ChronoUnit;
import java.util.*;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

public class SessionsManager extends EventListenerAsync {

    protected static Logger log = LogManager.getLogger(Connector.class.getName());

    public static final long NEVER = Long.MAX_VALUE;

    public static class EventSessionFinished extends EventListener.Event {
        public Session session;

        EventSessionFinished(Session session) {
            this.session = session;
        }
    }

    private MarketplacesManager marketplacesManager;

    public MainWindowController mainWindowController;

    private EventDispatcher eventDispatcher;

    private PreferencesSession preferencesSession;
    public Map<Session.Type, Session> sessions;

    private Map<Session.TurnoverKey, BigDecimal> purses;

    private ScheduledExecutorService scheduledExecutorService;
    private ScheduledFuture scheduledFuturePeriodCheck;

    //////////////////////////////////////////////////
    // Constructor.

    public SessionsManager(JsonFactory jsonFactory, ObjectMapper objectMapper, JsonNodeFactory jsonNodeFactory, MarketplacesManager marketplacesManager) {
        super();

        this.marketplacesManager = marketplacesManager;

        eventDispatcher = new EventDispatcher();

        objectMapper.registerModule(new SimpleModule() {{
            addKeyDeserializer(Currency.class, new Currency.           _KeyDeserializer(marketplacesManager));
            addKeyDeserializer(Session.TurnoverKey.class, new Session.TurnoverKey._KeyDeserializer(marketplacesManager));
        }});

        preferencesSession = new PreferencesSession(jsonFactory, objectMapper, jsonNodeFactory);
        preferencesSession.setFileName("sessions.json");
        preferencesSession.setSaveAuto(false);
        preferencesSession.load();

        sessions = new EnumMap<>(Session.Type.class);

        Session.Type[] sessionTypesInit = new Session.Type[] { Session.Type.HOURLY, Session.Type.DAILY, Session.Type.MONTHLY, Session.Type.LIFETIME, Session.Type.ONTIME };
        for (Session.Type sessionType : sessionTypesInit) {
            // Load saved session.
            Session session = preferencesSession.getSession(sessionType);
            // If session doesn't exist or is too old,
            if (session == null || getPeriod(sessionType) != getPeriod(sessionType, session.timeStart)) {
                // Discard it and create a new one.
                session = new Session(sessionType);
                // Record starting time.
                session.start();
            }
            sessions.put(sessionType, session);
        }

        purses = new HashMap<>();

        scheduledExecutorService = Executors.newSingleThreadScheduledExecutor();
        start();

        scheduleNextPeriodCheck();
    }

    public void done() {
        stop();
        scheduledFuturePeriodCheck.cancel(false);
        scheduledExecutorService.shutdown();
    }

    //////////////////////////////////////////////////
    // Event dispatcher.

    public void addListener(EventListener eventListener) { eventDispatcher.addListener(eventListener); }

    public void remListener(EventListener eventListener) { eventDispatcher.remListener(eventListener); }

    public void broadcast(EventListener.Event event) { eventDispatcher.broadcast(event); }

    //////////////////////////////////////////////////
    // Event cycle.

    @Override
    protected void listenAsync(Event event) {
        if (event instanceof Timer.EventTimer) {
            scheduleNextPeriodCheck();

            for (Map.Entry<Session.Type, Session> sessionEntry : sessions.entrySet()) {
                // Create shortcuts.
                Session.Type sessionType = sessionEntry.getKey();
                Session session = sessionEntry.getValue();
                // If the session is too old, replace it.
                if (getPeriod(sessionType) != getPeriod(sessionType, session.timeStart)) {
                    // Remember capitalization of the old session.
                    BigDecimal capitalization = session.capitalizationCurrent;
                    // Record stopping time.
                    session.stop ();
                    // Notify everyone of session change.
                    eventDispatcher.broadcast(new EventSessionFinished(session));
                    // Replace stored session.
                    session = new Session(sessionType);
                    // Record starting time.
                    session.start();
                    // Record last known capitalization.
                    session.recordCapitalization(capitalization);
                }
            }
        }
        if (event instanceof Connector.EventMarketplaceInitialized) {
            Connector.EventMarketplaceInitialized eventTyped = (Connector.EventMarketplaceInitialized) event;
            Marketplace marketplace = marketplacesManager.marketplaces.get(eventTyped.marketplaceType);
            if (marketplace != null) {
                Purses purses = marketplace.purses;
                for (Currency currency : marketplace.getCurrencies()) {
                    this.purses.put(new Session.TurnoverKey(eventTyped.marketplaceType, currency), purses.get0(currency));
                }
            }
        }
        // recordCapitalization
    }

    private void scheduleNextPeriodCheck() {
        long delay = NEVER;
        for (Session.Type sessionType : sessions.keySet()) {
            delay = Math.min(delay, getMillisToPeriod(sessionType));
        }

        if (delay != NEVER) {
            scheduledFuturePeriodCheck = scheduledExecutorService.schedule(() -> { listen(new Timer.EventTimer()); }, delay, TimeUnit.MILLISECONDS);
        }
    }

    //////////////////////////////////////////////////
    // Session time.

    private long getPeriod(Session.Type sessionType) {
        return getPeriod(sessionType, System.currentTimeMillis());
    }

    private long getPeriod(Session.Type sessionType, long time) {
        if (!sessionType.periodical) {
            return 0;
        }

        LocalDateTime timeParam = LocalDateTime.ofInstant(Instant.ofEpochMilli(time), ZoneId.systemDefault());
        LocalDateTime timeEpoch = LocalDateTime.ofInstant(Instant.EPOCH, ZoneId.systemDefault());

        switch (sessionType) {
            case HOURLY:
                return ChronoUnit.HOURS .between(timeEpoch, timeParam);
            case DAILY:
                return ChronoUnit.DAYS  .between(timeEpoch, timeParam);
            case MONTHLY:
                return ChronoUnit.MONTHS.between(timeEpoch, timeParam);
        }

        // Unreachable.
        return 0;
    }

    public long getMillisToPeriod(Session.Type sessionType) {
        if (!sessionType.periodical) {
            return NEVER;
        }

        Instant instantNow = Instant.now();
        ZonedDateTime timeNow = ZonedDateTime.ofInstant(instantNow, ZoneId.systemDefault());
        ZonedDateTime timeNextPeriod = null;

        switch (sessionType) {
            case HOURLY:
                timeNextPeriod = timeNow.truncatedTo(ChronoUnit.HOURS).plusHours(1);
                break;
            case DAILY:
                timeNextPeriod = timeNow.truncatedTo(ChronoUnit.DAYS).plusDays(1);
                break;
            case MONTHLY:
                timeNextPeriod = timeNow.truncatedTo(ChronoUnit.DAYS);
                timeNextPeriod = timeNextPeriod.minusDays(timeNextPeriod.getDayOfMonth() - 1).plusMonths(1);
                break;
        }

        return ChronoUnit.MILLIS.between(instantNow, timeNextPeriod.toInstant());
    }

    //////////////////////////////////////////////////
    // Data manipulation.

    public void reset(Session.Type sessionType) {
        Session session = sessions.get(sessionType);
        if (session != null) {
            session.reset();
        }
    }

    public void recordPlanned(Marketplace.Type marketplaceType, DirectionPair directionPair, BigDecimal amountBase, BigDecimal amountQuote) {
        Session.TurnoverKey turnoverKeyBase  = new Session.TurnoverKey(marketplaceType, directionPair.base );
        Session.TurnoverKey turnoverKeyQuote = new Session.TurnoverKey(marketplaceType, directionPair.quote);

        for (Map.Entry<Session.Type, Session> sessionEntry : sessions.entrySet()) {
            Session session = sessionEntry.getValue();
            session.recordPlanned(turnoverKeyBase,  amountBase );
            session.recordPlanned(turnoverKeyQuote, amountQuote);
        }

        save();
    }

    public void cancelPending(Marketplace.Type marketplaceType, DirectionPair directionPair, BigDecimal amountBase, BigDecimal amountQuote) {
        Session.TurnoverKey turnoverKeyBase  = new Session.TurnoverKey(marketplaceType, directionPair.base );
        Session.TurnoverKey turnoverKeyQuote = new Session.TurnoverKey(marketplaceType, directionPair.quote);

        for (Map.Entry<Session.Type, Session> sessionEntry : sessions.entrySet()) {
            Session session = sessionEntry.getValue();
            session.cancelPending(turnoverKeyBase,  amountBase );
            session.cancelPending(turnoverKeyQuote, amountQuote);
        }

        save();
    }

    public void recordReported(Marketplace.Type marketplaceType, DirectionPair directionPair, BigDecimal amountBase, BigDecimal amountQuote) {
        Session.TurnoverKey turnoverKeyBase  = new Session.TurnoverKey(marketplaceType, directionPair.base );
        Session.TurnoverKey turnoverKeyQuote = new Session.TurnoverKey(marketplaceType, directionPair.quote);

        for (Map.Entry<Session.Type, Session> sessionEntry : sessions.entrySet()) {
            Session session = sessionEntry.getValue();
            session.recordReported(turnoverKeyBase,  amountBase );
            session.recordReported(turnoverKeyQuote, amountQuote);
        }

        save();
    }

    public void recordPurse(Marketplace.Type marketplaceType, DirectionPair directionPair, BigDecimal purseBase, BigDecimal purseQuote) {
        Session.TurnoverKey turnoverKeyBase  = new Session.TurnoverKey(marketplaceType, directionPair.base );
        Session.TurnoverKey turnoverKeyQuote = new Session.TurnoverKey(marketplaceType, directionPair.quote);
        BigDecimal purseBaseOld  = purses.get(turnoverKeyBase );
        BigDecimal purseQuoteOld = purses.get(turnoverKeyQuote);

        BigDecimal purseDeltaBase  = (purseBaseOld  == null) ? null : purseBase .subtract(purseBaseOld );
        BigDecimal purseDeltaQuote = (purseQuoteOld == null) ? null : purseQuote.subtract(purseQuoteOld);

        if (purseDeltaBase != null || purseDeltaQuote != null) {
            for (Map.Entry<Session.Type, Session> sessionEntry : sessions.entrySet()) {
                Session session = sessionEntry.getValue();
                if (purseDeltaBase  != null) {
                    session.recordPurseDelta(turnoverKeyBase,  purseDeltaBase );
                }
                if (purseDeltaQuote != null) {
                    session.recordPurseDelta(turnoverKeyQuote, purseDeltaQuote);
                }
            }

            save();
        }

        purses.put(turnoverKeyBase,  purseBase );
        purses.put(turnoverKeyQuote, purseQuote);
    }

    public void recordCapitalization(BigDecimal capitalization) {
        for (Map.Entry<Session.Type, Session> sessionEntry : sessions.entrySet()) {
            sessionEntry.getValue().recordCapitalization(capitalization);
        }

        save();
    }

    private void save() {
        for (Map.Entry<Session.Type, Session> sessionEntry : sessions.entrySet()) {
            // Create shortcuts.
            Session.Type sessionType = sessionEntry.getKey();
            Session session = sessionEntry.getValue();

            if (sessionType.saved) {
                preferencesSession.setSession(sessionType, session);
            }
        }

        preferencesSession.save();
    }
}