//package Model.strategy2;
//
//import Application.Events.EventListener;
//import Application.Literals;
//import Model.Market.Market;
//import Model.Market.MarketDirection;
//import Model.Market.MarketPair;
//import Model.Market.MarketsManager;
//import org.apache.logging.log4j.LogManager;
//import org.apache.logging.log4j.Logger;
//
//import java.math.BigDecimal;
//import java.math.RoundingMode;
//import java.util.ArrayList;
//import java.util.List;
//
//public class Strategy implements EventListener {
//
//    private abstract static class Action {
//
//        abstract User.HTTPQueryResult execute(User user);
//
//        abstract String getDescription();
//    }
//
//    private static class ActionSplit extends Action {
//
//        private OfferMarket offer;
//        private BigDecimal amountIn;
//        private BigDecimal amountOut;
//
//        ActionSplit(OfferMarket offer, BigDecimal amountIn, BigDecimal amountOut) {
//            this.offer     = offer;
//            this.amountIn  = amountIn;
//            this.amountOut = amountOut;
//        }
//
//        @Override
//        public User.HTTPQueryResult execute(User user) {
//            return user.split(offer, amountIn, amountOut);
//        }
//
//        @Override
//        public String getDescription() {
//            return "Split " + offer.getId() + " " + amountIn + " " + amountOut;
//        }
//    }
//
//    private static class ActionMerge extends Action {
//        OfferMarket offer;
//        OfferMarket offerMerge;
//
//        ActionMerge(OfferMarket offer, OfferMarket offerMerge) {
//            this.offer      = offer;
//            this.offerMerge = offerMerge;
//        }
//
//        @Override
//        public User.HTTPQueryResult execute(User user) {
//            return user.merge(offer, offerMerge);
//        }
//
//        @Override
//        public String getDescription() {
//            return "Merge " + offer.getId() + " " + offerMerge.getId();
//        }
//    }
//
//    private static class ActionModify extends Action {
//        OfferMarket offer;
//        BigDecimal amountOut;
//        BigDecimal rate;
//        BigDecimal rateTarget;
//
//        public ActionModify(OfferMarket offer, BigDecimal amountOut, BigDecimal rate, BigDecimal rateTarget) {
//            this.offer      = offer;
//            this.amountOut  = amountOut;
//            this.rate       = rate;
//            this.rateTarget = rateTarget;
//        }
//
//        @Override
//        public User.HTTPQueryResult execute(User user) {
//            return user.modify(offer, amountOut, rate, rateTarget);
//        }
//
//        @Override
//        public String getDescription() {
//            return "Modify " + offer.getId() + " " + rate;
//        }
//    }
//
//    private static class ActionBuy extends Action {
//        OfferMarket offer;
//        OfferMarket offerBuy;
//
//        ActionBuy(OfferMarket offer, OfferMarket offerBuy) {
//            this.offer    = offer;
//            this.offerBuy = offerBuy;
//        }
//
//        @Override
//        public User.HTTPQueryResult execute(User user) {
//            return user.buy(offer, offerBuy);
//        }
//
//        @Override
//        public String getDescription() {
//            return "Buy " + offer.getId() + " " + offerBuy.getId();
//        }
//    }
//
//    private static class ActionBuyMulti extends Action {
//        OfferMarket offer;
//        List<OfferMarket> offersBuy;
//
//        ActionBuyMulti(OfferMarket offer, List<OfferMarket> offersBuy) {
//            this.offer    = offer;
//            this.offersBuy = offersBuy;
//        }
//
//        @Override
//        public User.HTTPQueryResult execute(User user) {
//            User.HTTPQueryResult result = User.HTTPQueryResult.BUY_SUCCESS;
//            if (offersBuy != null) {
//                for (OfferMarket offerBuy : offersBuy) {
//                    if ((result = user.buy(offer, offerBuy)) != User.HTTPQueryResult.BUY_SUCCESS) {
//                        break;
//                    }
//                }
//            }
//            return result;
//        }
//
//        @Override
//        public String getDescription() {
//            return "Buy multi " + offer.getId();
//        }
//    }
//
//    private static class ActionSafe extends Action {
//
//        @Override
//        public User.HTTPQueryResult execute(User user) {
//            return user.safe();
//        }
//
//        @Override
//        public String getDescription() {
//            return "Safe";
//        }
//    }
//
//    private static Logger log = LogManager.getLogger(Strategy.class.getName());
//
//    private static final BigDecimal rateChangeDampSlice = new BigDecimal("0.0025");
//    private static final BigDecimal rateChangeDampBulk  = new BigDecimal("0.0200");
//    private static final BigDecimal rateRatioDampSliceMin = BigDecimal.ONE.subtract(rateChangeDampSlice);
//    private static final BigDecimal rateRatioDampSliceMax = BigDecimal.ONE.add     (rateChangeDampSlice);
//    private static final BigDecimal rateRatioDampBulkMin  = BigDecimal.ONE.subtract(rateChangeDampBulk );
//    private static final BigDecimal rateRatioDampBulkMax  = BigDecimal.ONE.add     (rateChangeDampBulk );
//    private static final int scaleRateChange = 4;
//
//    private static final BigDecimal sliceMergeAndSplitRatio = new BigDecimal("0.5");
//
//    private static final BigDecimal depthRatioSlice = new BigDecimal("0.0025");
//    private static final int scaleDepthRatio = 4;
//
//    private static final BigDecimal rateSliceDeviation = new BigDecimal("0.0025");
//    private static final int rateSliceDeviationScale = 10;
//
//    // Webmoney rate quant.
//    private static final BigDecimal WMQuantRate = new BigDecimal("0.0001");
//
//    private MarketsManager marketsManager;
//    User user;
//
//    MarketPair marketPair;
//    MarketDirection marketDirectionAsk;
//    MarketDirection marketDirectionBid;
//    private List<Action> actions;
//
//    public Strategy(MarketsManager marketsManager, User user) {
//        this.marketsManager = marketsManager;
//        this.user = user;
//        this.marketsManager.addListener(this);
//        actions = new ArrayList<Action>();
//    }
//
//    public void setMarketPair(MarketPair marketPair) {
//        MarketPair marketPairOld = this.marketPair;
//        this.marketPair = marketPair;
//        MarketPair marketPairNew = this.marketPair;
//
//        if (this.marketPair != null) {
//            marketDirectionAsk = marketPair.getMdAsk();
//            marketDirectionBid = marketPair.getMdBid();
//        }
//
//        if (marketPairOld != marketPairNew) {
//            actions.clear();
//        }
//    }
//
//    @Override
//    public void listen(Event event) {
//        if (event instanceof MarketsManager.EventMarketUpdated) {
//            MarketDirection marketDirectionEvent = ((MarketsManager.EventMarketUpdated) event).marketDirection;
//                // Invalidate (remove) all decisions if one the markets has changed.
//            if (marketDirectionAsk == marketDirectionEvent
//             || marketDirectionBid == marketDirectionEvent) {
//                actions.clear();
//            }
//        }
//    }
//
//    public void decide() {
//        actions.clear();
//
//        // Fetch markets that are going to be used throughout the function.
//        Market marketAsk = marketsManager.getMarket(marketDirectionAsk);
//        Market marketBid = marketsManager.getMarket(marketDirectionBid);
//
//        // If markets are unavailable no decision can be made.
//        if (marketAsk == null
//         || marketBid == null) {
//            return;
//        }
//
//        // Go/stay safe decision.
//        if (user.getOffersListInTrade(marketDirectionAsk).size() > 2
//         || user.getOffersListInTrade(marketDirectionBid).size() > 2
//         || marketAsk.getOfferListSize() == 0
//         || marketBid.getOfferListSize() == 0
//         || user.getSafeRate() == null
//         || !user.getSafeRate().isValid()) {
//            actions.add(new ActionSafe());
//            return;
//        }
//
//        Action action;
//
//        // Buy decision.
//        boolean buyAsk = false;
//        boolean buyBid = false;
//        if ((action = createActionBuy(marketDirectionAsk, marketDirectionBid, true, false)) != null) {
//            actions.add(action);
//            buyAsk = true;
//        }
//        if ((action = createActionBuy(marketDirectionBid, marketDirectionAsk, true, false)) != null) {
//            actions.add(action);
//            buyBid = true;
//        }
//
//        OfferMarket offerUserAskBulk  = user.findFirstUserOfferByType(marketDirectionAsk, OfferMarket.Type.BULK);
//        OfferMarket offerUserAskSlice = user.findFirstUserOfferByType(marketDirectionAsk, OfferMarket.Type.SLICE);
//        OfferMarket offerUserBidBulk  = user.findFirstUserOfferByType(marketDirectionBid, OfferMarket.Type.BULK);
//        OfferMarket offerUserBidSlice = user.findFirstUserOfferByType(marketDirectionBid, OfferMarket.Type.SLICE);
//        OfferBuilder offerRateCreate;
//        OfferBuilder offerRateModify;
//
//        // Bulk decision.
//        if (offerUserAskBulk != null) {
//            offerRateModify = createTestUserOffer(offerUserAskBulk);
//            if (offerRateModify != null && offerRateModify.getRateTarget().compareTo(offerUserAskBulk.getRate()) != 0) {
//                actions.add(new ActionModify(offerUserAskBulk, offerRateModify.getAmountOut(), offerRateModify.getRate(), offerRateModify.getRateTarget()));
//            }
//        }
//
//        if (offerUserBidBulk != null) {
//            offerRateModify = createTestUserOffer(offerUserBidBulk);
//            if (offerRateModify != null && offerRateModify.getRateTarget().compareTo(offerUserBidBulk.getRate()) != 0) {
//                actions.add(new ActionModify(offerUserBidBulk, offerRateModify.getAmountOut(), offerRateModify.getRate(), offerRateModify.getRateTarget()));
//            }
//        }
//
//        // Slice decision.
//        if (offerUserAskSlice != null) {
//            if (!buyAsk) {
//                offerRateModify = createTestUserOffer(offerUserAskSlice);
//                if (offerRateModify != null) {
//                    offerRateCreate = createTestUserOffer(marketDirectionAsk);
//                    BigDecimal amountInCreate = (offerRateCreate == null) ? BigDecimal.ZERO : offerRateCreate.getAmountIn();
//                    if (offerUserAskBulk != null && offerUserAskSlice.getAmountIn().compareTo(amountInCreate.multiply(sliceMergeAndSplitRatio)) < 0) {
//                        actions.add(new ActionMerge(offerUserAskBulk, offerUserAskSlice));
//                        if (offerRateCreate != null) {
//                            actions.add(new ActionSplit(offerUserAskBulk, offerRateCreate.getAmountIn(), offerRateCreate.getAmountOut()));
//                        }
//                    } else if (offerRateModify.getRateTarget().compareTo(offerUserAskSlice.getRate()) != 0) {
//                        actions.add(new ActionModify(offerUserAskSlice, offerRateModify.getAmountOut(), offerRateModify.getRate(), offerRateModify.getRateTarget()));
//                    }
//                } else {
//                    actions.add(new ActionMerge(offerUserAskBulk, offerUserAskSlice));
//                }
//            }
//        } else {
//            if (offerUserAskBulk != null) {
//                offerRateCreate = createTestUserOffer(marketDirectionAsk);
//                if (offerRateCreate != null) {
//                    actions.add(new ActionSplit(offerUserAskBulk, offerRateCreate.getAmountIn(), offerRateCreate.getAmountOut()));
//                }
//            }
//        }
//
//        if (offerUserBidSlice != null) {
//            if (!buyBid) {
//                offerRateModify = createTestUserOffer(offerUserBidSlice);
//                if (offerRateModify != null) {
//                    offerRateCreate = createTestUserOffer(marketDirectionBid);
//                    BigDecimal amountInCreate = (offerRateCreate == null) ? BigDecimal.ZERO : offerRateCreate.getAmountIn();
//                    if (offerUserBidBulk != null && offerUserBidSlice.getAmountIn().compareTo(amountInCreate.multiply(sliceMergeAndSplitRatio)) < 0) {
//                        actions.add(new ActionMerge(offerUserBidBulk, offerUserBidSlice));
//                        if (offerRateCreate != null) {
//                            actions.add(new ActionSplit(offerUserBidBulk, offerRateCreate.getAmountIn(), offerRateCreate.getAmountOut()));
//                        }
//                    } else if (offerRateModify.getRateTarget().compareTo(offerUserBidSlice.getRate()) != 0) {
//                        actions.add(new ActionModify(offerUserBidSlice, offerRateModify.getAmountOut(), offerRateModify.getRate(), offerRateModify.getRateTarget()));
//                    }
//                } else {
//                    actions.add(new ActionMerge(offerUserBidBulk, offerUserBidSlice));
//                }
//            }
//        } else {
//            if (offerUserBidBulk != null) {
//                offerRateCreate = createTestUserOffer(marketDirectionBid);
//                if (offerRateCreate != null) {
//                    actions.add(new ActionSplit(offerUserBidBulk, offerRateCreate.getAmountIn(), offerRateCreate.getAmountOut()));
//                }
//            }
//        }
//
//        if (actions.size() > 0) {
//            log.trace(Literals.logTraceStrategyHasActions);
//            for (Action actionTrace : actions) {
//                log.trace(actionTrace.getDescription());
//            }
//        } else {
//            log.trace(Literals.logTraceStrategyNoActions);
//        }
//    }
//
//    private Action createActionBuy(MarketDirection marketDirection, MarketDirection marketDirectionBuy, boolean repeat, boolean force) {
//        // Для скупки глупых слайс должен существовать. Существование слайса также подтверждает, что
//        // мы находимся в пределах орбиты.
//        OfferMarket offer = user.findFirstUserOfferByType(marketDirection, OfferMarket.Type.SLICE);
//        if (offer == null) {
//            return null;
//        }
//
//        Market marketBuy = marketsManager.getMarket(marketDirectionBuy);
//        List<OfferMarket> offersBuy = new ArrayList<OfferMarket>();
//
//        for (int indexBuy = 0; repeat || indexBuy == 0; ++indexBuy) {
//            // Следующая лучшая заявка противоположного рынка.
//            OfferMarket offerBuyBest;
//            try {
//                offerBuyBest = marketBuy.getOffersList().get(indexBuy);
//            } catch (IndexOutOfBoundsException e) {
//                break;
//            }
//            if (offerBuyBest == null || offerBuyBest.isPersonal()) {
//                break;
//            }
//
//            // Skip tests if force flag is set.
//            boolean allow = force;
//            if (!allow) {
//                boolean isAsk = marketDirection.isAsk();
//                if (user.preferencesUser.getAllowGrabDumb(marketDirection)) {
//                    // Если установлен дополнительный флаг скупки глупых заявок, пороговый курс равняется
//                    // ограничению на курс слайса.
//                    allow = MarketDirection.compareRatesByDirection(offerBuyBest.getRateD(), isAsk ? user.calcAskCapRate() : user.calcBidCapRate(), isAsk) <= 0;
//                } else {
//                    // Если не установлен дополнительный флаг скупки глупых заявок, пороговый курс равняется
//                    // курсу слайса.
//                    allow = MarketDirection.compareRatesByDirection(offerBuyBest.getRate(), offer.getRate(), isAsk) <= 0;
//                }
//                // Заявка является глупой, если ее курс конкурентнее порогового курса.
//            }
//            // If either force flag is set or tests were passed, create an action.
//            if (allow) {
//                if (repeat) {
//                    offersBuy.add(offerBuyBest);
//                } else {
//                    return new ActionBuy(offer, offerBuyBest);
//                }
//            } else {
//                break;
//            }
//        }
//
//        if (offersBuy.size() == 0) {
//            return null;
//        }
//
//        return new ActionBuyMulti(offer, offersBuy);
//    }
//
//    public List<User.HTTPQueryResult> execute() {
//        List<User.HTTPQueryResult> results = new ArrayList<User.HTTPQueryResult>(actions.size());
//        for (Action action : actions) {
//            results.add(action.execute(user));
//        }
//        actions.clear();
//        return results;
//    }
//
//    public void split(MarketDirection marketDirection) {
//        OfferMarket offerBulk = user.findFirstUserOfferByType(marketDirection, OfferMarket.Type.BULK);
//        if (offerBulk == null) {
//            log.trace(Literals.logTraceStrategySplitNoBulk);
//            return;
//        }
//
//        OfferBuilder offerCreate = createTestUserOffer(marketDirection);
//        if (offerCreate == null) {
//            log.trace(Literals.logTraceStrategySliceZeroSize);
//            return;
//        }
//
//        user.split(offerBulk, offerCreate.getAmountIn(), offerCreate.getAmountOut());
//    }
//
//    public void merge(MarketDirection marketDirection) {
//        OfferMarket offerSlice = user.findFirstUserOfferByType(marketDirection, OfferMarket.Type.SLICE);
//        if (offerSlice == null) {
//            log.trace(Literals.logTraceStrategyMergeNoSlice);
//            return;
//        }
//
//        OfferMarket offerBulk  = user.findFirstUserOfferByType(marketDirection, OfferMarket.Type.BULK);
//        if (offerBulk == null) {
//            log.trace(Literals.logTraceStrategyMergeNoBulk);
//            return;
//        }
//
//        user.merge(offerBulk, offerSlice);
//    }
//
//    public void modify(MarketDirection marketDirection) {
//        OfferMarket offerSlice = user.findFirstUserOfferByType(marketDirection, OfferMarket.Type.SLICE);
//        if (offerSlice == null) {
//            log.trace(Literals.logTraceStrategySliceZeroSize);
//            return;
//        }
//
//        OfferBuilder offerModify = createTestUserOffer(marketDirection);
//        if (offerModify == null) {
//            log.trace(Literals.logTraceStrategyModifyNoSlice);
//            return;
//        }
//
//        user.modify(offerSlice, offerModify.getAmountOut(), offerModify.getRate(), offerModify.getRateTarget());
//    }
//
//    public void buy(MarketDirection marketDirection, MarketDirection marketDirectionBuy) {
//        Action action = createActionBuy(marketDirection, marketDirectionBuy, false, true);
//        if (action != null) {
//            action.execute(user);
//        }
//    }
//
//    //////////////////////////////////////////////////
//    // Rate calculations.
//
//    // Get the closest rate above provided rate. The provided rate is assumed to be of the regular scale. If it's not,
//    // it's reduced to the regular scale first.
//    private static BigDecimal calcRateAbove(BigDecimal rate, boolean ask) {
//        BigDecimal rateRound = Offer.setScaleForRate(rate);
//        return ask ? rateRound.subtract(WMQuantRate) : rateRound.add(WMQuantRate);
//    }
//
//    // Get the closest rate below provided rate. The provided rate is assumed to be of the regular scale. If it's not,
//    // it's reduced to the regular scale first.
//    private static BigDecimal calcRateBelow(BigDecimal rate, boolean ask) {
//        BigDecimal rateRound = Offer.setScaleForRate(rate);
//        return ask ? rateRound.add(WMQuantRate) : rateRound.subtract(WMQuantRate);
//    }
//
//    // Get the correct rounding direction for market direction.
//    private static RoundingMode getRoundingModeRateAbovePrecise(boolean ask) {
//        return ask ? RoundingMode.FLOOR : RoundingMode.CEILING;
//    }
//
//    // Get the correct rounding direction for market direction.
//    private static RoundingMode getRoundingModeRateBelowPrecise(boolean ask) {
//        return ask ? RoundingMode.CEILING : RoundingMode.FLOOR;
//    }
//
//    // Get the closest rate above provided rate with an arbitrary scale.
//    private static BigDecimal calcRateAbovePrecise(BigDecimal rate, boolean ask) {
//        return calcRateAbove(rate.setScale(Offer.scaleRate, getRoundingModeRateBelowPrecise(ask)), ask);
//    }
//
//    // Get the closest rate below provided rate with an arbitrary scale.
//    private static BigDecimal calcRateBelowPrecise(BigDecimal rate, boolean ask) {
//        return calcRateBelow(rate.setScale(Offer.scaleRate, getRoundingModeRateAbovePrecise(ask)), ask);
//    }
//
//    private static BigDecimal calcBestRateAroundDepth(Market market, double depth, boolean around) {
//        // Get market section at given depth.
//        Market.Section section = market.getSectionAtDepth(depth);
//        // Get an offer at that section.
//        OfferMarket offerDepth = section.offer;
//        // If it exists,
//        if (offerDepth != null) {
//            // Proceed with the main calculations.
//            final boolean ask = market.getDirection().isAsk();
//            // Get a rate on top of that offer.
//            BigDecimal rateDepth = calcRateAbove(offerDepth.getRate(), ask);
//            // If looking around isn't requested,
//            if (!around) {
//                // Return simple rate at given depth.
//                return rateDepth;
//            }
//
//            // Find the boundaries of the rate range.
//            BigDecimal rateDepthMin = Offer.divideScaleForRate  (rateDepth, rateSliceDeviation.add(BigDecimal.ONE));
//            BigDecimal rateDepthMax = Offer.multiplyScaleForRate(rateDepth, rateSliceDeviation.add(BigDecimal.ONE));
//            BigDecimal rateDepthTop = ask ? rateDepthMin : rateDepthMax;
//            BigDecimal rateDepthBot = ask ? rateDepthMax : rateDepthMin;
//
//            // Get market section at the top of the rate range.
//            section = market.getSectionAtRate(rateDepthTop);
//            // The offer at that section is guaranteed to exist.
//            OfferMarket offerNext = section.offer;
//            // The best offer is currently the first offer.
//            OfferMarket offerBest = offerNext;
//            BigDecimal sumBase = BigDecimal.ZERO;
//
//            BigDecimal ratioRateDepth = rateSliceDeviation.divide(new BigDecimal(depth), rateSliceDeviationScale, RoundingMode.UP);
//
//            // Repeat until there are offers left.
//            while (section.iteratorOffer.hasNext()) {
//                // All personal offers should be skipped.
//                if (!offerNext.isPersonal()) {
//                    // Add last offer's base amount to the total base amount change.
//                    sumBase = sumBase.add(offerNext.getAmountBase());
//                }
//                // Go to the next offer.
//                offerNext = section.iteratorOffer.next();
//                if (!offerNext.isPersonal()) {
//                    // Calculate total rate change as the difference between current rate and best rate.
//                    BigDecimal sumRate = offerNext.getRate().subtract(offerBest.getRate()).abs();
//                    // If the total rate change per total base amount change is better than the threshold,
//                    if (sumRate.divide(sumBase, rateSliceDeviationScale, RoundingMode.UP).compareTo(ratioRateDepth) > 0) {
//                        // Assume new best offer.
//                        offerBest = offerNext;
//                        sumBase = BigDecimal.ZERO;
//                    }
//                    // If the offer falls below rate range,
//                    if (offerNext.compareRateByDirection(rateDepthBot) < 0) {
//                        // Stop.
//                        break;
//                    }
//                }
//            }
//
//            // Assume the rate on top of the best offer.
//            return calcRateAbove(offerBest.getRate(), ask);
//        }
//
//        // If there's at least one offer,
//        offerDepth = market.getOfferBot();
//        if (offerDepth != null) {
//            // Assume the rate of the bottom offer.
//            return offerDepth.getRate();
//        }
//
//        // If there are no offers on the market, a new offer cannot be created.
//        return null;
//    }
//
//    private static BigDecimal applyMult(BigDecimal rate, double mult) {
//        return rate.multiply(new BigDecimal(mult));
//    }
//
//    // Find parameters for a new slice offer.
//    public OfferBuilder createTestUserOffer(MarketDirection marketDirection) {
//
//        ////////////////////////////////////////
//        // Preliminary actions.
//
//        // Create shortcuts.
//        final boolean ask = marketDirection.isAsk();
//        final Market market = marketsManager.getMarket(marketDirection);
//        final OfferMarket.Type offerType = OfferMarket.Type.SLICE;
//
//        // Size of the new offer.
//        BigDecimal amountIn = OfferMarket.setScaleForAmount(new BigDecimal(user.getDynamicSliceSize(marketDirection)));
//
//        // If the input amount is still 0 we're unable to create any offer.
//        if (amountIn.compareTo(BigDecimal.ZERO) == 0) {
//            return null;
//        }
//
//        // Create new offer's template.
//        OfferBuilder offerNew = new OfferBuilder(marketDirection.isAsk(), amountIn);
//
//        ////////////////////////////////////////
//        // Rate calculations.
//
//        BigDecimal rateTarget = calcBestRateAroundDepth(market, user.preferencesUser.getPlast(marketDirection, offerType), offerType == OfferMarket.Type.SLICE);
//        if (rateTarget == null) {
//            return null;
//        }
//
//        final double mult = user.preferencesUser.getMult(marketDirection, offerType);
//        if (mult != 1.0 && mult != 0.0) {
//            rateTarget = applyMult(rateTarget, mult);
//        }
//
//        offerNew.setRateTarget(rateTarget);
//
//        ////////////////////////////////////////
//        // Rate limitations.
//
//        // Get rate cap.
//        BigDecimal rateCap = new BigDecimal(user.getOfferCapRate(marketDirection, offerType));
//
//        // Use rate cap for the first step.
//        BigDecimal rateStep = rateCap;
//        while (true) {
//            // If the target rate is below cap rate,
//            if (offerNew.compareRateTargetByDirection(rateCap) <= 0) {
//                // Use it.
//                break;
//            }
//
//            // Find the next offer below.
//            OfferMarket offerStep = market.getNextOffer(rateStep);
//            // If there are no more offers,
//            if (offerStep == null) {
//                // Set target to the closest rate below rate cap.
//                offerNew.setRateTarget(calcRateBelowPrecise(rateCap, ask));
//                break;
//            }
//
//            // Get the offer's rate and use this rate to start the next step if needed.
//            rateStep = offerStep.getRate();
//            // Set target on top of it.
//            offerNew.setRateTarget(calcRateAbove(rateStep, ask));
//        }
//
//        // Calculate offer parameters that will produce the target.
//        offerNew.fitRateTargetForActionCreate();
//        return offerNew;
//    }
//
//    // Find parameters for an existing slice or bulk offer.
//    public OfferBuilder createTestUserOffer(OfferMarket offerCur) {
//
//        ////////////////////////////////////////
//        // Preliminary actions.
//
//        // Create shortcuts.
//        final MarketDirection marketDirection = offerCur.getDirection();
//        final boolean ask = marketDirection.isAsk();
//        final Market market = marketsManager.getMarket(marketDirection);
//        final OfferMarket.Type offerType = offerCur.getType();
//
//        // Parameters of the existing offer.
//        BigDecimal amountInCur = offerCur.getAmountIn();
//        BigDecimal rateCur = offerCur.getRate();
//
//        // Create new offer's template.
//        OfferBuilder offerNew = new OfferBuilder(ask, amountInCur);
//
//        ////////////////////////////////////////
//        // Rate calculations.
//
//        BigDecimal rateTarget = calcBestRateAroundDepth(market, user.preferencesUser.getPlast(marketDirection, offerType), offerType == OfferMarket.Type.SLICE);
//        if (rateTarget == null) {
//            return null;
//        }
//
//        final double mult = user.preferencesUser.getMult(marketDirection, offerType);
//        if (mult != 1.0 && mult != 0.0) {
//            rateTarget = applyMult(rateTarget, mult);
//        }
//
//        offerNew.setRateTarget(rateTarget);
//
//        ////////////////////////////////////////
//        // Rate limitations.
//
//        // Get rate cap.
//        BigDecimal rateCap = new BigDecimal(user.getOfferCapRate(marketDirection, offerType));
//
//        // Use rate cap for the first step.
//        BigDecimal rateStep = rateCap;
//        while (true) {
//            // If the target rate is below cap rate,
//            if (offerNew.compareRateTargetByDirection(rateCap) <= 0) {
//                // Make note of whether target rate has been modified or not to avoid repeating costly comparison.
//                boolean targetModified = false;
//                // Check for new offer being too close to the current one.
//                while (true) {
//                    // If rates are equal,
//                    if (offerNew.getRateTarget().compareTo(offerCur.getRate()) == 0) {
//                        // Let new offer be equal to the current offer.
//                        break;
//                    }
//                    // Calculate offer parameters that will produce the target.
//                    offerNew.fitRateTargetForActionModify();
//                    // If the output amount differs,
//                    if (offerNew.getAmountOut().compareTo(offerCur.getAmountOut()) != 0) {
//                        // The offers don't clash and the result is acceptable.
//                        break;
//                    }
//                    // Adjust target rate up.
//                    offerNew.setRateTarget(calcRateAbove(offerNew.getRateTarget(), ask));
//                    targetModified = true;
//                }
//
//                // If the target rate is still below cap rate,
//                if (!targetModified || offerNew.compareRateTargetByDirection(rateCap) <= 0) {
//                    // Use it.
//                    break;
//                }
//            }
//
//            // Find the next offer below.
//            OfferMarket offerStep = market.getNextOffer(rateStep);
//            // If there are no more offers,
//            if (offerStep == null) {
//                // Set target to the closest rate below rate cap.
//                offerNew.setRateTarget(calcRateBelowPrecise(rateCap, ask));
//                offerNew.fitRateTargetForActionModify();
//                break;
//            }
//
//            // Get the offer's rate and use this rate to start the next step if needed.
//            rateStep = offerStep.getRate();
//            // Set target on top of it.
//            offerNew.setRateTarget(calcRateAbove(rateStep, ask));
//        }
//
//        ////////////////////////////////////////
//        // Apply rate change dampener.
//
//        int sign = offerNew.getRateTarget().compareTo(rateCur);
//        // If there are pending changes to the offer,
//        if (sign != 0) {
//            // Prevent small changes unless it's a floating slice (one that is within the allowed depth range).
//            if (offerType == OfferMarket.Type.BULK || (MarketDirection.compareRatesByDirection(rateTarget, offerNew.getRateTarget(), ask) > 0
//                                                    && MarketDirection.compareRatesByDirection(rateTarget, rateCur,                  ask) > 0)) {
//                // Get proper bounds for the offer type.
//                BigDecimal rateRatioDampMin = (offerType == OfferMarket.Type.BULK ) ? rateRatioDampBulkMin
//                                            : (offerType == OfferMarket.Type.SLICE) ? rateRatioDampSliceMin : null;
//                BigDecimal rateRatioDampMax = (offerType == OfferMarket.Type.BULK ) ? rateRatioDampBulkMax
//                                            : (offerType == OfferMarket.Type.SLICE) ? rateRatioDampSliceMax : null;
//                // Calculate the ratio of the rate change.
//                BigDecimal rateRatio = offerNew.getRateTarget().divide(rateCur, scaleRateChange,
//                        (sign > 0) ? RoundingMode.CEILING : RoundingMode.FLOOR);
//                // If the rate change is small enough,
//                if (rateRatio.compareTo(rateRatioDampMin) >= 0
//                 && rateRatio.compareTo(rateRatioDampMax) <= 0) {
//                    // Set new offer to be equal to the current offer signaling no rate change is needed.
//                    offerNew.setRateTarget(rateCur);
//                }
//            }
//        }
//
//        return offerNew;
//    }
//}
