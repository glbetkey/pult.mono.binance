package Model.Marketplace.Base;

import java.math.BigDecimal;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import Application.Timer;
import Application.Events.EventListener;
import Application.Events.EventListenerAsync;
import Model.Marketplace.MarketplacesManager;
import Model.Marketplace.Base.Marketplace.ResultDataSummary;
import Model.Marketplace.Base.Marketplace.ResultDataTake;
import Model.Marketplace.Base.Currency.Direction;
import Model.Marketplace.Base.Currency.DirectionPair;
import Model.Marketplace.Base.Currency.DirectionPairLocal;
import Model.Marketplace.Base.Market.Market;
import Model.Marketplace.Base.Market.Offer;
import Model.Marketplace.Base.Market.Order;
import Model.Marketplace.Base.Market.Order.Status;
import Model.Marketplace.Base.Market.Order.TimeInForce;
import Model.Marketplace.Base.Market.Trade;
import Model.Marketplace.Base.Market.TradeSummary;
import Utils.ScaleOp;

public abstract class Connector {
    // used in TakeWith delay for delay for order update (ms)
    private static final int ORDER_WAIT_TIME = 1000;
    // purses updates variable
    protected 	volatile 	long 	pursesLastUpdated = 0;
    protected 				Object	pursesUpdatedLock = new Object();


    protected static Logger log = LogManager.getLogger(Connector.class.getName());

    public class RequestListener extends EventListenerAsync {

        protected void listenAsync(Event event) {
            Connector.this.listenAsync(event);
        }
    }

    public static class EventMarketplaceInitialized extends EventListener.Event {

        public Marketplace.Type marketplaceType;

        public EventMarketplaceInitialized(Marketplace.Type marketplaceType) {
            this.marketplaceType = marketplaceType;
        }
    }

    public static class EventMarketChanged extends EventListener.Event {

        public Marketplace.Type marketplaceType;
        public Direction direction;

        public EventMarketChanged(Marketplace.Type marketplaceType, Direction direction) {
            this.marketplaceType = marketplaceType;
            this.direction = direction;
        }
    }

    public static class EventRequest extends EventListener.Event {

        public int commID;
        public Marketplace.RequestData requestData;

        public EventRequest(int commID, Marketplace.RequestData requestData) {
            this.commID = commID;
            this.requestData = requestData;
        }
    }

    public static class EventResult extends EventListener.Event {

        public int commID;
        public Marketplace.ResultData resultData;

        public EventResult(int commID, Marketplace.ResultData resultData) {
            this.commID = commID;
            this.resultData = resultData;
        }
    }

    public	static 	String 	USER_AGENT = "TradeApi Manager";

    protected MarketplacesManager marketplacesManager;
    protected Marketplace marketplace;

    RequestListener requestListener;
    private Timer timer;
    // used for diff updates
    protected Map<Direction, BigDecimal> initialRates = Collections.synchronizedMap(new HashMap<>());
    protected Map<DirectionPair, Long>  marketUpdateIds = new HashMap<>();


    //////////////////////////////////////////////////
    // Constructor.

    public Connector(MarketplacesManager marketplacesManager, Marketplace marketplace) {
        this.marketplacesManager = marketplacesManager;
        this.marketplace = marketplace;

        requestListener = new RequestListener();
        requestListener.start();
        timer = new Timer();
        timer.addListener(requestListener);
    }

    public void done() {
        requestListener.stop();
        timer.done();
    }

    protected void startTimer(int timeout) {
        timer.stop();
        timer.setTimeout(timeout);
        timer.start();
    }

    void stopTimer() {
        timer.stop();
    }

    //////////////////////////////////////////////////
    // Event listener.

    protected void listenAsync(EventListener.Event event) {
        if (event instanceof Timer.EventTimer) {
            tick();
        }
        if (event instanceof EventRequest) {
            EventRequest eventTyped = (EventRequest) event;
            Marketplace.RequestData requestData = eventTyped.requestData;
            if (requestData instanceof Marketplace.RequestDataMake) {
                marketplace.result(eventTyped.commID, make((Marketplace.RequestDataMake) requestData));
            }
            if (requestData instanceof Marketplace.RequestDataTakeWithDelay) {
                Marketplace.RequestDataTakeWithDelay requestDataTyped = (Marketplace.RequestDataTakeWithDelay) requestData;
//                log.trace(marketplace.type.name + " " + requestDataTyped.getDescription() + ".");
                Marketplace.ResultDataTake resultDataTake = takeWithDelay(requestDataTyped);
                marketplace.result(eventTyped.commID, resultDataTake);
//                log.trace(marketplace.type.name + " " + resultDataTake.getDescription() + ".");
            } else if (requestData instanceof Marketplace.RequestDataTake) {
                Marketplace.RequestDataTake requestDataTyped = (Marketplace.RequestDataTake) requestData;
//                log.trace(marketplace.type.name + " " + requestDataTyped.getDescription() + ".");
                Marketplace.ResultDataTake resultDataTake = take(requestDataTyped);
                marketplace.result(eventTyped.commID, resultDataTake);
//                log.trace(marketplace.type.name + " " + resultDataTake.getDescription() + ".");
            }
            if (requestData instanceof Marketplace.RequestDataCancel) {
                marketplace.result(eventTyped.commID, cancel((Marketplace.RequestDataCancel) requestData));
            }
            if (requestData instanceof Marketplace.RequestDataSummary) {
                marketplace.result(eventTyped.commID, summary((Marketplace.RequestDataSummary) requestData));
            }
        }
    }

    protected void tick() {
    }

    protected abstract Marketplace.ResultDataMake make(Marketplace.RequestDataMake requestDataMake);

    protected Marketplace.ResultDataTake take(Marketplace.RequestDataTake requestDataTake) {
        Marketplace.RequestDataMake requestDataMake = new Marketplace.RequestDataMake(requestDataTake.direction, requestDataTake.amount, requestDataTake.rate);
        Marketplace.ResultDataMake resultDataMake = make(requestDataMake);
        if (resultDataMake.result == Marketplace.ResultDataMake.Result.FAILURE) {
            return Marketplace.ResultDataTake.FAILURE;
        }

        if (resultDataMake.result == Marketplace.ResultDataMake.Result.SUCCESS) {
            Marketplace.RequestDataCancel requestDataCancel = new Marketplace.RequestDataCancel(requestDataTake.direction, resultDataMake.order);
            Marketplace.ResultDataCancel resultDataCancel = cancel(requestDataCancel);
            if (resultDataCancel.result == Marketplace.ResultDataCancel.Result.FAILURE) {
                // Todo: Somehow indicate that the offer has been created but cannot be deleted.
                //return Marketplace.ResultDataTake.FAILURE;
            }
        }

        Marketplace.RequestDataSummary requestDataSummary = new Marketplace.RequestDataSummary(requestDataTake.direction, resultDataMake.order);
        Marketplace.ResultDataSummary resultDataSummary = summary(requestDataSummary);
        if (resultDataSummary.result == Marketplace.ResultDataSummary.Result.SUCCESS) {
            if (resultDataSummary.amountBase.signum() != 0 && resultDataSummary.amountQuote.signum() != 0) {
                // TODO change ResultDataTake to use summary object
                return new Marketplace.ResultDataTake(Marketplace.ResultDataTake.Result.SUCCESS, resultDataSummary.amountBase, resultDataSummary.amountQuote);
            }
        }

        return Marketplace.ResultDataTake.FAILURE;
    }
    protected Marketplace.ResultDataTake takeWithDelay(Marketplace.RequestDataTakeWithDelay requestDataTakeWithDelay) {
        Marketplace.RequestDataMake requestDataMake = new Marketplace.RequestDataMake(requestDataTakeWithDelay.direction, requestDataTakeWithDelay.amount, requestDataTakeWithDelay.rate);
        Marketplace.ResultDataMake resultDataMake = make(requestDataMake);
        if (resultDataMake.result == Marketplace.ResultDataMake.Result.FAILURE) {
            //log.trace("Failed result for order make");
            return Marketplace.ResultDataTake.FAILURE;
        }

        if (resultDataMake.result == Marketplace.ResultDataMake.Result.SUCCESS) {
            // wait on order before cancel
            waitOnOrderToBeFinished(requestDataTakeWithDelay.direction, resultDataMake.order, true, requestDataTakeWithDelay.delay);
        }

        Marketplace.RequestDataSummary requestDataSummary = new Marketplace.RequestDataSummary(requestDataTakeWithDelay.direction, resultDataMake.order);
        Marketplace.ResultDataSummary resultDataSummary = summary(requestDataSummary);
        if (resultDataSummary.result == Marketplace.ResultDataSummary.Result.SUCCESS) {
            // TODO change ResultDataTake to use summary object
            log.trace("Connector return summary: " + resultDataSummary.amountBase.toPlainString() + " " + resultDataSummary.amountQuote.toPlainString());
            if (resultDataSummary.amountBase.signum() != 0 && resultDataSummary.amountQuote.signum() != 0) {
                return new Marketplace.ResultDataTake(Marketplace.ResultDataTake.Result.SUCCESS, resultDataSummary.amountBase, resultDataSummary.amountQuote);
            }
        }

        return Marketplace.ResultDataTake.FAILURE;
    }

    protected void waitOnOrderToBeFinished(Direction direction, Order order, boolean forceCancel, long delay) {

        boolean cancelNeeded = forceCancel;
        long startTime = System.currentTimeMillis();
        long lastOrderCheckTime = System.currentTimeMillis();


        log.trace("Waiting on order to be finished " + order.id);
        if (order != null) {
            // update market order immediate to see if it is completed
            updateMarketOrder(direction, order, false);
            //log.trace("order exists");
            synchronized (order) {
                while (!order.isFinished() && System.currentTimeMillis() - startTime < delay) {

                    order.waitForFinish(ORDER_WAIT_TIME);

                    if (!order.isFinished()) updateMarketOrder(direction, order, false);

                    lastOrderCheckTime = System.currentTimeMillis();
                }
                cancelNeeded = cancelNeeded && order.isActive();
            }
        }
        long beforeCancelTime = 0;
        // if order is active, but forceCancel initially set to true
        if (cancelNeeded) {
            //log.trace("Order not finished within delay. Trying to cancel");
            beforeCancelTime = System.currentTimeMillis();
            Marketplace.RequestDataCancel requestDataCancel = new Marketplace.RequestDataCancel(direction, order);
            Marketplace.ResultDataCancel resultDataCancel = cancel(requestDataCancel);
            if (resultDataCancel.result == Marketplace.ResultDataCancel.Result.FAILURE) {
                // Todo: Somehow indicate that the offer has been created but cannot be deleted.
                //return Marketplace.ResultDataTake.FAILURE;
            }
        }
        long fromTime = cancelNeeded ? beforeCancelTime : lastOrderCheckTime;

        waitForPursesUpdatedAfter(fromTime, 2000);
    }

    protected void waitForPursesUpdatedAfter(long time, int timeout) {
        long start = System.currentTimeMillis();
        synchronized (pursesUpdatedLock) {
            while (time > pursesLastUpdated && (start + timeout) > System.currentTimeMillis()) {
                try {
                    updatePurses(false);
                    // if updated, break
                    if (time > pursesLastUpdated) {
                        break;
                    }
                    pursesUpdatedLock.wait(50);
                } catch (InterruptedException e) {
                    // TODO Auto-generated catch block
                    //e.printStackTrace();
                    break;
                }
            }
            if (time <= pursesLastUpdated) {
                updatePurses(true);
            }
        }
    }

    protected Order createMarketOrder(Direction direction, String orderID, BigDecimal amount, BigDecimal rate) {
        return createMarketOrder(direction, orderID, amount, rate, TimeInForce.GTC);
    }
    protected Order createMarketOrder(Direction direction, String orderID, BigDecimal amount, BigDecimal rate,
                                      TimeInForce tif) {

        return createMarketOrder(direction, orderID, amount, rate, tif, System.currentTimeMillis());
    }
    protected Order createMarketOrder(Direction direction, String orderID, BigDecimal amount, BigDecimal rate,
                                      TimeInForce tif, Long created) {

        return marketplace.createMarketOrder(direction, orderID, amount, rate, tif, created);
    }

    protected abstract Marketplace.ResultDataCancel cancel(Marketplace.RequestDataCancel requestDataCancel);

    protected Marketplace.ResultDataSummary summary(Marketplace.RequestDataSummary requestDataSummary) {
        // web-socket connectors must ignore order update request
        updateMarketOrder(requestDataSummary.direction, requestDataSummary.order, true);
        return summaryFromOrder(requestDataSummary.direction, requestDataSummary.order);
    }

    protected ResultDataSummary summaryFromOrder(Direction direction, Order order) {

        TradeSummary summary = marketplace.getTradeSummary(direction, order.id);

        BigDecimal amountBase  = summary.amount;
        BigDecimal amountQuote = summary.price;

        if (direction.ask) {
            amountBase  = amountBase .negate();
        } else {
            amountQuote = amountQuote.negate();
        }

        ResultDataSummary result = new ResultDataSummary(ResultDataSummary.Result.SUCCESS, amountBase, amountQuote);

        return result;
    }
    //////////////////////////////////////////////////
    // Event dispatcher.

    protected void broadcastEventMarketplaceInitialized() {
        marketplacesManager.broadcast(new EventMarketplaceInitialized(marketplace.type));
        log.debug("INIT");
    }

    protected void broadcastEventMarketChanged(Direction direction) {
        marketplacesManager.broadcast(new EventMarketChanged(marketplace.type, direction));
    }

    public Marketplace.Type getMarketplaceType() {
        return marketplace.type;
    }
    public void updateMarketData(List<Offer> offers, Direction direction, boolean diff) {
        if (diff)
            updateMarketDataDiff(offers, direction);
        else
            updateMarketData(direction, offers);
    }
    // update market data from offers list
    public void updateMarketData(Direction direction, List<Offer> offers) {
        // bid comparator
        Comparator<Offer> bidComparator = new Comparator<Offer>() {
            @Override
            public int compare(Offer o2, Offer o1) {
                // TODO Auto-generated method stub
                return o1.getRate().compareTo(o2.getRate());
            }
        };
        // ask comparator
        Comparator<Offer> askComparator = new Comparator<Offer>() {
            @Override
            public int compare(Offer o1, Offer o2) {
                // TODO Auto-generated method stub
                return o1.getRate().compareTo(o2.getRate());
            }
        };

        Collections.sort(offers, direction.ask ? askComparator : bidComparator);

        Market market = marketplace.marketsManager.markets.get(direction);
        if (market == null) {
            log.error("No market record created for: " + direction);
            return;
        }
        //log.trace(direction + " " + offers);

        market.setOffers(offers);

        broadcastEventMarketChanged(direction);

        return;
    }
    /**
     * Process web-services changes received for order book
     * @param dir
     */
    protected void updateMarketDataDiff(List<Offer> doffers, Direction dir) {

        Boolean offersModified = false;
        BigDecimal initialTrustRate = initialRates.get(dir);
        if (initialTrustRate == null) {
            if (marketplace.marketsManager.markets.get(dir).offers.size() < 100) {
                initialTrustRate = BigDecimal.ZERO;
            } else {
                initialTrustRate = marketplace.marketsManager.markets.get(dir).offers.get(marketplace.marketsManager.markets.get(dir).offers.size() - 1).getRate();
            };
            initialRates.put(dir, initialTrustRate);
        }
        Market market = marketplace.marketsManager.markets.get(dir);
        List<Offer> offers = market.offers;
        synchronized (offers) {
            for (Offer off: doffers) {
                Offer needed = null;
                BigDecimal rate = off.getRate();
                BigDecimal qty  = off.getAmountBase();
                //System.out.println(rate);
                for (Iterator<Offer> it2 = offers.iterator(); it2.hasNext();) {
                    Offer current = it2.next();
                    if (current.getRate().equals(rate) || current.getRate().compareTo(rate) == 0) {
                        needed = current;
                        break;
                    }
                }
                //System.out.println(needed);
                if (needed != null) {
                    if (BigDecimal.ZERO.equals(qty) || BigDecimal.ZERO.compareTo(qty) == 0) {
                        offers.remove(needed);
                        offersModified = true;
                    } else {
                        needed.setAmountBase(qty);
                        offersModified = true;
                    }
                } else {
                    if (BigDecimal.ZERO.equals(qty) || BigDecimal.ZERO.compareTo(qty) == 0) {
                        continue;
                    };
                    // Ignore outer offers to initialTrustRate
                    // TODO may be add '=' to offers comparitions?
                    if (!initialTrustRate.equals(BigDecimal.ZERO)) {
                        if (dir.ask ? (off.getRate().compareTo(initialTrustRate) > 0)
                                : (off.getRate().compareTo(initialTrustRate) < 0)) {
                            continue;
                        }
                    }

                    needed = off;
                    Integer index = null;
                    for (int i = 0; i < offers.size(); i++) {
                        if (dir.ask ? (offers.get(i).getRate().compareTo(needed.getRate()) < 0)
                                : (offers.get(i).getRate().compareTo(needed.getRate()) > 0)) {
                            //System.out.println(offers.get(i).getRate() + ":" + needed.getRate() + ":" + offers.get(i).getRate().compareTo(needed.getRate()));
                            continue;
                        } else {
                            index = new Integer(i);
                            break;
                        }
                    }
                    if (index == null) {
                        offersModified = true;
                        offers.add(needed);
                    } else {
                        offersModified = true;
                        offers.add(index, needed);
                    }
                };
            }
        };

        //log.trace(market.offers);
        if (offersModified)	{
            broadcastEventMarketChanged(dir);
        }
    }
    protected void invalidateMarkets() {
        marketplace.marketsManager.marketsRequired.forEach((DirectionPair pair) -> {
            invalidateMarkets(pair);
            marketUpdateIds.put(pair, Long.valueOf(0));
        });
    }
    protected void invalidateMarkets(DirectionPair pair) {
        invalidateMarket(marketplace.marketsManager.markets.get(pair.bid));
        invalidateMarket(marketplace.marketsManager.markets.get(pair.ask));
    }

    protected void invalidateMarket(Market m) {

        synchronized (m) {
            m.offers.clear();
        }
    }
    // update order from network
    protected abstract void updateMarketOrder(Direction direction, Order order, boolean beforeSummary);

    protected abstract void updatePurses(boolean force);

    protected void updatePursesLastUpdated() {
        synchronized (pursesUpdatedLock) {
            pursesLastUpdated = System.currentTimeMillis();
            pursesUpdatedLock.notifyAll();
        }
    }


}
