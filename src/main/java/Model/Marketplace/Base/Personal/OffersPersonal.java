package Model.Marketplace.Base.Personal;

import Model.Marketplace.Base.Currency.Direction;
import Model.Marketplace.Base.Market.Offer;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

public class OffersPersonal {

    public                Map<String, Offer>  offers;
    public Map<Direction, Map<String, Offer>> offersByDirection;

    private Map<String, Direction> directions;

    public OffersPersonal() {
        offers = new HashMap<>();
        offersByDirection = new HashMap<>();
        directions = new HashMap<>();
    }

    public void put(String id, Offer offer, Direction direction) {
        offers.put(id, offer);
        directions.put(id, direction);

        Map<String, Offer> offersDirection = offersByDirection.get(direction);
        if (offersDirection == null) {
            offersDirection = new HashMap<>();
            offersByDirection.put(direction, offersDirection);
        }

        offersDirection.put(id, offer);
    }

    public void rem(String id) {
        Offer offer = offers.remove(id);
        if (offer != null) {
            Direction direction = directions.get(id);
            if (direction != null) {
                Map<String, Offer> offersDirection = offersByDirection.get(direction);
                if (offersDirection != null) {
                    offersDirection.remove(id);
                }
            }
        }
    }

    public boolean isPersonal(Offer offer, Direction direction) {
        Map<String, Offer> offersDirection = offersByDirection.get(direction);
        if (offersDirection == null) {
            return false;
        }

        BigDecimal rate = offer.getRate();
        for (Offer offerPersonal : offersDirection.values()) {
            if (rate.equals(offerPersonal.getRate())) {
                return true;
            }
        }

        return false;
    }
}
