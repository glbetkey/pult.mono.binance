package Model.Marketplace.Base.Currency;

import Utils.ScaleOp;

public class CurrencyLocal {

    public ScaleOp scaleOp;

    //////////////////////////////////////////////////
    // Constructor.

    public CurrencyLocal() {
        this.scaleOp = new ScaleOp();
    }

    public CurrencyLocal(int scale) {
        this.scaleOp = new ScaleOp(scale);
    }
}
