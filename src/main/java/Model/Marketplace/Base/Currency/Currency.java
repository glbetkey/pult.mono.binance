package Model.Marketplace.Base.Currency;

import Model.Marketplace.MarketplacesManager;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.KeyDeserializer;

import java.io.IOException;

public class Currency {

    public String name;

    public static class _KeyDeserializer extends KeyDeserializer {
        private MarketplacesManager marketplacesManager;

        public _KeyDeserializer(MarketplacesManager marketplacesManager) {
            this.marketplacesManager = marketplacesManager;
        }

        @Override
        public Object deserializeKey(final String key, final DeserializationContext context) throws IOException, JsonProcessingException {
            return marketplacesManager.getCurrency(key);
        }
    }

    //////////////////////////////////////////////////
    // Constructor.

    public Currency(String name) {
        this.name = name.toUpperCase();
    }

    //////////////////////////////////////////////////
    // General purpose.

    @Override
    public boolean equals(Object other) {
        return (other instanceof Currency) && matches((Currency) other);
    }

    @Override
    public int hashCode() { return name.hashCode(); }

    public boolean matches(Currency other) {
        return matches(other.name);
    }

    public boolean matches(String name) {
        return this.name.equalsIgnoreCase(name);
    }

    @Override
    public String toString() { return getName(); }

    public String getName() { return name; }

}