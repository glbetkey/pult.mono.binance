package Model.Marketplace.Base.Currency;

import Utils.ScaleOp;

import java.math.BigDecimal;

public class DirectionPairLocal {

    public BigDecimal feeTradeMake;
    public BigDecimal feeTradeTake;
    public ScaleOp scaleOpBase;
    public ScaleOp scaleOpQuote;
    public ScaleOp scaleOpRate;
    public BigDecimal amountBaseMin;
    public BigDecimal amountBaseMax;
    public BigDecimal amountQuoteMin;
    public BigDecimal amountQuoteMax;

    public static final BigDecimal AMOUNT_INVALID = new BigDecimal("-1");

    //////////////////////////////////////////////////
    // Constructor.

    public DirectionPairLocal(
    		BigDecimal feeTradeMake, 
    		BigDecimal feeTradeTake,
            int        scaleBase,
    		int        scaleQuote,
            int 	   scaleRate,
    		BigDecimal amountBaseMin,
    		BigDecimal amountBaseMax,
    		BigDecimal amountQuoteMin,
    		BigDecimal amountQuoteMax
    ) {
        this.feeTradeMake   = feeTradeMake;
        this.feeTradeTake   = feeTradeTake;
        this.scaleOpBase    = new ScaleOp(scaleBase);
        this.scaleOpQuote   = new ScaleOp(scaleQuote);
        this.scaleOpRate 	= new ScaleOp(scaleRate);
        this.amountBaseMin  = amountBaseMin;
        this.amountBaseMax  = amountBaseMax;
        this.amountQuoteMin = amountQuoteMin;
        this.amountQuoteMax = amountQuoteMax;
    }
}
