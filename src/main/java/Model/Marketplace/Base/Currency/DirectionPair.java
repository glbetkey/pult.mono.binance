package Model.Marketplace.Base.Currency;

import Application.Literals;

import java.util.Objects;

public class DirectionPair {

    public Currency base;
    public Currency quote;

    public Direction ask;
    public Direction bid;

    //////////////////////////////////////////////////
    // Constructor.

    public DirectionPair(Currency base, Currency quote) {
        this.base  = base;
        this.quote = quote;
    }

    public void setDirections(Direction ask, Direction bid) {
        this.ask = ask;
        this.bid = bid;
    }

    //////////////////////////////////////////////////
    // General purpose.

    @Override
    public boolean equals(Object other) {
        return (other instanceof DirectionPair) && matches((DirectionPair) other);
    }

    @Override
    public int hashCode() {
        return Objects.hash(base, quote);
    }

    public boolean matches(DirectionPair other) {
        return matches(other.base, other.quote);
    }

    public boolean matches(Currency base, Currency quote) {
        return this.base .matches(base )
            && this.quote.matches(quote);
    }

    @Override
    public String toString() { return getName(); }

    public String getName() {
        return String.format(Literals.nameDirectionPair, base.name, quote.name).toUpperCase();
    }
}