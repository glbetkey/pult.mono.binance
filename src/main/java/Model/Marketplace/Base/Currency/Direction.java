package Model.Marketplace.Base.Currency;

import Application.Literals;

import java.util.Objects;

public class Direction {

    public DirectionPair directionPair;
    public boolean ask;

    //////////////////////////////////////////////////
    // Constructor.

    public Direction(DirectionPair directionPair, boolean ask) {
        this.directionPair = directionPair;
        this.ask = ask;
    }

    //////////////////////////////////////////////////
    // General purpose.

    @Override
    public boolean equals(Object other) {
        return (other instanceof Direction) && matches((Direction) other);
    }

    @Override
    public int hashCode() { return Objects.hash(directionPair, ask); }

    public boolean matches(Direction other) {
        return matches(other.directionPair, other.ask);
    }

    public boolean matchesOpposite(Direction other) {
        return matches(other.directionPair, !other.ask);
    }

    public boolean matches(DirectionPair directionPair, boolean ask) {
        return this.directionPair.matches(directionPair) && this.ask == ask;
    }

    @Override
    public String toString() { return getName(); }

    public String getName() {
        return String.format(Literals.nameDirection, directionPair.getName(), ask ? Literals.nameDirectionAsk : Literals.nameDirectionBid);
    }

    public String getName2() {
        return String.format(Literals.nameDirection, directionPair.getName(), ask ? Literals.nameDirectionAsk2 : Literals.nameDirectionBid2);
    }

    //////////////////////////////////////////////////
    // Simple derived properties.

    public Currency getBase() { return directionPair.base; }

    public Currency getQuote() { return directionPair.quote; }

    public Currency getIn() { return ask ? directionPair.base : directionPair.quote; }

    public Currency getOut() { return ask ? directionPair.quote : directionPair.base; }
}
