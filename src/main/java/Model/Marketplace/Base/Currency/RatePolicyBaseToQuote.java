package Model.Marketplace.Base.Currency;

import java.math.BigDecimal;
import java.math.RoundingMode;

public class RatePolicyBaseToQuote extends RatePolicy {

    private static class FeeModImpl implements FeeMod {

        private BigDecimal mult;
        private int scale;

        FeeModImpl(Direction direction, DirectionPairLocal directionPairLocal, BigDecimal feeTransfer, BigDecimal margin) {
            // Add 8 to rate's scale. It's an arbitrary number.
            scale = directionPairLocal.scaleOpRate.scale + 8;
            if (direction.ask) {
                // rate = rate * (1 + trade) / (1 - transfer) * (1 + margin)
                mult = (BigDecimal.ONE.add(directionPairLocal.feeTradeTake)).multiply(BigDecimal.ONE.add(margin)).divide(BigDecimal.ONE.subtract(feeTransfer), scale, RoundingMode.HALF_EVEN);
            } else {
                // rate = rate * (1 - trade       - transfer) / (1 + margin)
                mult = (BigDecimal.ONE.subtract(feeTransfer).subtract(directionPairLocal.feeTradeTake)).divide(BigDecimal.ONE.add(margin), scale, RoundingMode.HALF_EVEN);
            }
        }

        public BigDecimal applyFees(BigDecimal rate) {
            return rate.multiply(mult).setScale(scale, RoundingMode.HALF_EVEN);
        }
    }

    @Override
    protected BigDecimal convert(DirectionPairLocal directionPairLocal, BigDecimal amount, BigDecimal rate, boolean fromBase, RoundingMode roundingMode) {
        if (fromBase) {
            return directionPairLocal.scaleOpQuote.mul(amount, rate, roundingMode);
        } else {
            if (rate.compareTo(BigDecimal.ZERO) == 0) {
                return null;
            } else {
                return directionPairLocal.scaleOpBase.div(amount, rate, roundingMode);
            }
        }
    }

    @Override
    public BigDecimal getRate(DirectionPairLocal directionPairLocal, BigDecimal amountBase, BigDecimal amountQuote) {
        return directionPairLocal.scaleOpRate.div(amountQuote, amountBase);
    }

    @Override
    protected BigDecimal modifyRate(Direction direction, DirectionPairLocal directionPairLocal, BigDecimal rate, BigDecimal factor, boolean increase, ScaleMode scaleMode, int scale, RoundingMode roundingMode) {
        boolean multiply = increase ^ direction.ask;

        switch (scaleMode) {
            case DEFAULT:
                return multiply ? directionPairLocal.scaleOpRate.mul(rate, factor, roundingMode)
                        : directionPairLocal.scaleOpRate.div(rate, factor, roundingMode);
            case PRECISE:
                return multiply ? rate.multiply(factor).setScale(scale, roundingMode)
                        : rate.divide  (factor,          scale, roundingMode);
            case UNLIMITED:
                return multiply ? rate.multiply(factor)
                        : rate.divide  (factor, rate.scale() + factor.scale(), roundingMode);
        }

        // Error.
        return null;
    }

    public FeeMod createFeeMod(Direction direction, DirectionPairLocal directionPairLocal, BigDecimal feeTransfer, BigDecimal margin) {
        return new FeeModImpl(direction, directionPairLocal, feeTransfer, margin);
    }

    @Override
    public int compareRates(Direction direction, BigDecimal rate1, BigDecimal rate2) {
        return compareRatesStatic(direction, rate1, rate2);
    }

    public static int compareRatesStatic(Direction direction, BigDecimal rate1, BigDecimal rate2) {
        return direction.ask ? -rate1.compareTo(rate2) : rate1.compareTo(rate2);
    }

    @Override
    protected int compareRates2(/* this == ratePolicy1*/Direction direction1, BigDecimal rate1,
                                RatePolicy ratePolicy2,                       BigDecimal rate2) {
        return ratePolicy2.compareRates(this, direction1, rate1, rate2);
    }

    @Override
    protected int compareRates(RatePolicyBaseToQuote ratePolicy1, Direction direction1, BigDecimal rate1,
                                          /* this == ratePolicy2*/                      BigDecimal rate2) {
        return compareRatesBaseToQuoteBaseToQuote(direction1, rate1, rate2);
    }

}
