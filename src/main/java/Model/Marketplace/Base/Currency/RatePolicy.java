package Model.Marketplace.Base.Currency;

import java.math.BigDecimal;
import java.math.RoundingMode;

public abstract class RatePolicy {

    public enum ScaleMode {
        // Use currency's precision.
        DEFAULT,
        // Use maximal possible precision for multiplication and the sum of the precisions of the operands for division.
        UNLIMITED,
        // Use specified precision.
        PRECISE
    }

    public enum RoundingModeRate {
        INC,
        DEC;
    }

    public interface FeeMod {

        BigDecimal applyFees(BigDecimal rate);
    }

    public BigDecimal convert(DirectionPairLocal directionPairLocal, BigDecimal amount, BigDecimal rate) {
        return convert(directionPairLocal, amount, rate, true,  RoundingMode.HALF_UP);
    }

    public BigDecimal convertFromQuote(DirectionPairLocal directionPairLocal, BigDecimal amount, BigDecimal rate) {
        return convert(directionPairLocal, amount, rate, false, RoundingMode.HALF_UP);
    }

    public BigDecimal convert(DirectionPairLocal directionPairLocal, BigDecimal amount, BigDecimal rate, RoundingMode roundingMode) {
        return convert(directionPairLocal, amount, rate, true,  roundingMode);
    }

    public BigDecimal convertFromQuote(DirectionPairLocal directionPairLocal, BigDecimal amount, BigDecimal rate, RoundingMode roundingMode) {
        return convert(directionPairLocal, amount, rate, false, roundingMode);
    }

    protected abstract BigDecimal convert(DirectionPairLocal directionPairLocal, BigDecimal amount, BigDecimal rate, boolean fromBase, RoundingMode roundingMode);

    public abstract BigDecimal getRate(DirectionPairLocal directionPairLocal, BigDecimal amountBase, BigDecimal amountQuote);

    // Make rate more competitive by a factor.
    public BigDecimal incRate(Direction direction, DirectionPairLocal directionPairLocal, BigDecimal rate, BigDecimal factor, ScaleMode scaleMode, int scale, RoundingMode roundingMode) {
        return modifyRate(direction, directionPairLocal, rate, factor, true,  scaleMode, scale, roundingMode);
    }

    // Make rate less competitive by a factor.
    public BigDecimal decRate(Direction direction, DirectionPairLocal directionPairLocal, BigDecimal rate, BigDecimal factor, ScaleMode scaleMode, int scale, RoundingMode roundingMode) {
        return modifyRate(direction, directionPairLocal, rate, factor, false, scaleMode, scale, roundingMode);
    }

    // Make rate more competitive by a factor.
    public BigDecimal incRate(Direction direction, DirectionPairLocal directionPairLocal, BigDecimal rate, BigDecimal factor, ScaleMode scaleMode, int scale, RoundingModeRate roundingModeRate) {
        return modifyRate(direction, directionPairLocal, rate, factor, true,  scaleMode, scale, getRoundingModeForRate(direction, roundingModeRate));
    }

    // Make rate less competitive by a factor.
    public BigDecimal decRate(Direction direction, DirectionPairLocal directionPairLocal, BigDecimal rate, BigDecimal factor, ScaleMode scaleMode, int scale, RoundingModeRate roundingModeRate) {
        return modifyRate(direction, directionPairLocal, rate, factor, false, scaleMode, scale, getRoundingModeForRate(direction, roundingModeRate));
    }

    public abstract FeeMod createFeeMod(Direction direction, DirectionPairLocal directionPairLocal, BigDecimal feeTransfer, BigDecimal margin);

    public BigDecimal applyFees(Direction direction, DirectionPairLocal directionPairLocal, BigDecimal feeTransfer, BigDecimal margin, BigDecimal rate) {
        return createFeeMod(direction, directionPairLocal, feeTransfer, margin).applyFees(rate);
    }

    private RoundingMode getRoundingModeForRate(Direction direction, RoundingModeRate roundingModeRate) {
        if (roundingModeRate == RoundingModeRate.INC) { return direction.ask ? RoundingMode.FLOOR : RoundingMode.CEILING; }
        if (roundingModeRate == RoundingModeRate.DEC) { return direction.ask ? RoundingMode.CEILING : RoundingMode.FLOOR; }
        return RoundingMode.HALF_EVEN;
    }

    protected abstract BigDecimal modifyRate(Direction direction, DirectionPairLocal directionPairLocal, BigDecimal rate, BigDecimal factor, boolean increase, ScaleMode scaleMode, int scale, RoundingMode roundingMode);

    // Returns 1 if rate1 is above rate2, 0 if they're equal, -1 otherwise.
    public abstract int compareRates(Direction direction, BigDecimal rate1, BigDecimal rate2);

    // Comparison between different markets with possibly different rate policies.
    public int compareRates(/* this == ratePolicy1*/Direction direction1, BigDecimal rate1,
                            RatePolicy ratePolicy2, Direction direction2, BigDecimal rate2) {
        if (!direction1.directionPair.matches(direction2.directionPair)) {
            // These rates aren't comparable.
            return 0;
        }
        return compareRates2(direction1, rate1, ratePolicy2, rate2);
    }

    // First dispatching intermediate.
    protected abstract int compareRates2(/* this == ratePolicy1*/Direction direction1, BigDecimal rate1,
                                         RatePolicy ratePolicy2, BigDecimal rate2);

    // Double dispatching intermediate.
    protected abstract int compareRates(RatePolicyBaseToQuote ratePolicy1, Direction direction1, BigDecimal rate1,
                                                   /* this == ratePolicy2*/                      BigDecimal rate2);

    // Double dispatching final.
    protected static int compareRatesBaseToQuoteBaseToQuote(Direction direction, BigDecimal rate1, BigDecimal rate2) {
        return RatePolicyBaseToQuote.compareRatesStatic(direction, rate1, rate2);
    }
}
