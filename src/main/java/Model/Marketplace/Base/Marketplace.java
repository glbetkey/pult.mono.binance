package Model.Marketplace.Base;

import Application.Events.EventListener;
import Application.Preferences.PreferencesApp;
import Model.Marketplace.Base.Currency.*;
import Model.Marketplace.Base.Market.Market;
import Model.Marketplace.Base.Market.MarketsManager;
import Model.Marketplace.Base.Market.Order;
import Model.Marketplace.Base.Market.Order.Status;
import Model.Marketplace.Base.Market.Order.TimeInForce;
import Model.Marketplace.Base.Market.TradeSummary;
import Model.Marketplace.MarketplacesManager;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public class Marketplace {

    protected static Logger log = LogManager.getLogger(Marketplace.class.getName());

    public enum Type {
//        TEST1("Test1", "TST1", "test1"),
//        TEST2("Test2", "TST2", "test2"),
//        TEST3("Test3", "TST3", "test3"),
//        TEST4("Test4", "TST4", "test4"),
//
//        BITTREX("Bittrex", "BIT", "bittrex"),
//        KRAKEN ("Kraken",  "KRA", "kraken" ),
//        LIQUI  ("Liqui",   "LIQ", "liqui"  ),
        BINANCE("Binance", "BIN", "binance");
//        HITBTC ("HitBTC",  "HIT", "hitbtc" );

        public String name;
        public String nameShort;
        public String providerName; // LocalSettings key.

        Type(String name, String nameShort, String providerName) {
            this.name = name;
            this.nameShort = nameShort;
            this.providerName = providerName;
        }
    }

    public MarketplacesManager marketplacesManager;
    public Type type;
    public PreferencesApp preferencesApp;

    private final Map<Currency, CurrencyLocal> currenciesLocal;
    private final Map<Direction, DirectionLocal> directionsLocal;
    private final Map<DirectionPair, DirectionPairLocal> directionPairsLocal;

    public MarketsManager marketsManager;
    public Purses purses;

    public RatePolicy ratePolicy;
    public Connector connector;

    //////////////////////////////////////////////////
    // Constructor.

    public Marketplace(MarketplacesManager marketplacesManager, Type type, PreferencesApp preferencesApp) {
        this.marketplacesManager = marketplacesManager;
        this.type = type;
        this.preferencesApp = preferencesApp;

        currenciesLocal     = new HashMap<>();
        directionsLocal     = new HashMap<>();
        directionPairsLocal = new HashMap<>();

        marketsManager = new MarketsManager();
        purses = new Purses(0);

        requests = new HashMap<>();
    }

    public void init(RatePolicy ratePolicy, Connector connector) {
        this.ratePolicy = ratePolicy;
        this.connector  = connector;
    }

    public void done() {
        if (connector != null) {
            connector.done();
        }
    }

    //////////////////////////////////////////////////
    // Local data.

    public void addLocal(Currency currency, CurrencyLocal currencyLocal) {
        if (getLocal(currency) == null) {
            synchronized (currenciesLocal) {
                currenciesLocal.put(currency, currencyLocal);
            }
        }
    }

    public void addLocal(Currency currency) {
        if (getLocal(currency) == null) {
            synchronized (currenciesLocal) {
                currenciesLocal.put(currency, new CurrencyLocal());
            }
        }
    }

    public void addLocal(Currency currency, int scale) {
        if (getLocal(currency) == null) {
            synchronized (currenciesLocal) {
                currenciesLocal.put(currency, new CurrencyLocal(scale));
            }
        }
    }

    public void addLocal(Direction direction, DirectionLocal directionLocal) {
        if (getLocal(direction) == null) {
            synchronized (directionsLocal) {
                directionsLocal.put(direction, directionLocal);
            }
        }
    }

    public void addLocal(Direction direction) {
        if (getLocal(direction) == null) {
            synchronized (directionsLocal) {
                directionsLocal.put(direction, new DirectionLocal());
            }
        }
    }

    public void addLocal(DirectionPair directionPair, DirectionPairLocal directionPairLocal) {
        if (getLocal(directionPair) == null) {
            synchronized (directionPairsLocal) {
                directionPairsLocal.put(directionPair, directionPairLocal);
            }
        }
    }

    public void addLocal(DirectionPair directionPair,
                         BigDecimal feeTradeMake, BigDecimal feeTradeTake,
                         boolean useScaleBase, int scaleBase,
                         boolean useScaleRate, int scaleRate,
                         BigDecimal amountBaseMin,  BigDecimal amountBaseMax,
                         BigDecimal amountQuoteMin, BigDecimal amountQuoteMax) {
        if (getLocal(directionPair) == null) {
            CurrencyLocal baseLocal  = getLocal(directionPair.base );
            CurrencyLocal quoteLocal = getLocal(directionPair.quote);

            int scaleQuote;

            if (useScaleBase) {
                scaleBase = Math.min(scaleBase, baseLocal.scaleOp.scale);
            } else {
                scaleBase = baseLocal.scaleOp.scale;
            }

            if (useScaleBase && useScaleRate) {
                scaleQuote = Math.min(scaleBase + scaleRate, quoteLocal.scaleOp.scale);
            } else {
                scaleQuote = quoteLocal.scaleOp.scale;
            }

            if (useScaleRate) {
                scaleRate = Math.min(scaleRate, baseLocal.scaleOp.scale + quoteLocal.scaleOp.scale);
            } else {
                scaleRate = baseLocal.scaleOp.scale + quoteLocal.scaleOp.scale;
            }

            synchronized (directionPairsLocal) {
                directionPairsLocal.put(directionPair, new DirectionPairLocal(feeTradeMake, feeTradeTake, scaleBase, scaleQuote, scaleRate, amountBaseMin, amountBaseMax, amountQuoteMin, amountQuoteMax));
            }
        }
    }

    public Set<Currency> getCurrencies() {
        synchronized (currenciesLocal) {
            return currenciesLocal.keySet();
        }
    }

    public Set<Direction> getDirections() {
        synchronized (directionsLocal) {
            return directionsLocal.keySet();
        }
    }

    public Set<DirectionPair> getDirectionPairs() {
        synchronized (directionPairsLocal) {
            return directionPairsLocal.keySet();
        }
    }

    public CurrencyLocal getLocal(Currency currency) {
        synchronized (currenciesLocal) {
            return currenciesLocal.get(currency);
        }
    }

    public DirectionLocal getLocal(Direction direction) {
        synchronized (directionsLocal) {
            return directionsLocal.get(direction);
        }
    }

    public DirectionPairLocal getLocal(DirectionPair directionPair) {
        synchronized (directionPairsLocal) {
            return directionPairsLocal.get(directionPair);
        }
    }

    //////////////////////////////////////////////////
    // Functions propagation.

    public BigDecimal setToScaleBase(DirectionPair directionPair, BigDecimal amount) {
        return getLocal(directionPair).scaleOpBase.set(amount);
    }

    public BigDecimal convert(DirectionPair directionPair, BigDecimal amount, BigDecimal rate) {
        return ratePolicy.convert(getLocal(directionPair), amount, rate);
    }

    public BigDecimal convertFromQuote(DirectionPair directionPair, BigDecimal amount, BigDecimal rate) {
        return ratePolicy.convertFromQuote(getLocal(directionPair), amount, rate);
    }

    public BigDecimal convert(DirectionPair directionPair, BigDecimal amount, BigDecimal rate, RoundingMode roundingMode) {
        return ratePolicy.convert(getLocal(directionPair), amount, rate, roundingMode);
    }

    public BigDecimal convertFromQuote(DirectionPair directionPair, BigDecimal amount, BigDecimal rate, RoundingMode roundingMode) {
        return ratePolicy.convertFromQuote(getLocal(directionPair), amount, rate, roundingMode);
    }

    public BigDecimal getRate(DirectionPair directionPair, BigDecimal amountBase, BigDecimal amountQuote) {
        return ratePolicy.getRate(getLocal(directionPair), amountBase, amountQuote);
    }

    // Make rate more competitive by a factor.
    public BigDecimal incRate(Direction direction, BigDecimal rate, BigDecimal factor, RatePolicy.ScaleMode scaleMode, int scale, RoundingMode roundingMode) {
        return ratePolicy.incRate(direction, getLocal(direction.directionPair), rate, factor, scaleMode, scale, roundingMode);
    }

    // Make rate less competitive by a factor.
    public BigDecimal decRate(Direction direction, BigDecimal rate, BigDecimal factor, RatePolicy.ScaleMode scaleMode, int scale, RoundingMode roundingMode) {
        return ratePolicy.decRate(direction, getLocal(direction.directionPair), rate, factor, scaleMode, scale, roundingMode);
    }

    // Make rate more competitive by a factor.
    public BigDecimal incRate(Direction direction, BigDecimal rate, BigDecimal factor, RatePolicy.ScaleMode scaleMode, int scale, RatePolicy.RoundingModeRate roundingModeRate) {
        return ratePolicy.incRate(direction, getLocal(direction.directionPair), rate, factor, scaleMode, scale, roundingModeRate);
    }

    // Make rate less competitive by a factor.
    public BigDecimal decRate(Direction direction, BigDecimal rate, BigDecimal factor, RatePolicy.ScaleMode scaleMode, int scale, RatePolicy.RoundingModeRate roundingModeRate) {
        return ratePolicy.decRate(direction, getLocal(direction.directionPair), rate, factor, scaleMode, scale, roundingModeRate);
    }

    public RatePolicy.FeeMod createFeeMod(Direction direction, BigDecimal feeTransfer, BigDecimal margin) {
        return ratePolicy.createFeeMod(direction, getLocal(direction.directionPair), feeTransfer, margin);
    }

    // Returns 1 if rate1 is above rate2, 0 if they're equal, -1 otherwise.
    public int compareRates(Direction direction, BigDecimal rate1, BigDecimal rate2) {
        return ratePolicy.compareRates(direction, rate1, rate2);
    }

    // Comparison between different markets with possibly different rate policies.
    public int compareRates(/* this  == marketplace1*/Direction direction1, BigDecimal rate1,
                            Marketplace marketplace2, Direction direction2, BigDecimal rate2) {
        return ratePolicy.compareRates(direction1, rate1, marketplace2.ratePolicy, direction2, rate2);
    }

    // Comparison between different markets with possibly different rate policies.
    public static int compareRates(Marketplace marketplace1, Direction direction1, BigDecimal rate1,
                                   Marketplace marketplace2, Direction direction2, BigDecimal rate2) {
        return marketplace1.ratePolicy.compareRates(direction1, rate1,
               marketplace2.ratePolicy,             direction2, rate2);
    }

    //////////////////////////////////////////////////
    // Connector event chain.

    public static abstract class RequestData {
        public abstract String getDescription();
    }

    public static abstract class ResultData {
        public abstract String getDescription();
    }

    public static abstract class RequestDataDirection extends RequestData {

        public Direction direction;

        public RequestDataDirection(Direction direction) {
            this.direction = direction;
        }
    }

    public static class RequestDataMake extends RequestDataDirection {

        public BigDecimal amount;
        public BigDecimal rate;

        public RequestDataMake(Direction direction, BigDecimal amount, BigDecimal rate) {
            super(direction);
            this.amount = amount;
            this.rate = rate;
       }

        @Override
        public String getDescription() {
            return "Make " + direction.getName() + " " + amount.toString() + " " + rate.toString();
        }
    }

    public static class RequestDataTake extends RequestDataDirection {

        public BigDecimal amount;
        public BigDecimal rate;

        public RequestDataTake(Direction direction, BigDecimal amount, BigDecimal rate) {
            super(direction);
            this.amount = amount;
            this.rate = rate;
        }

        @Override
        public String getDescription() {
            return "Take " + direction.getName() + " " + amount.toString() + " " + rate.toString();
        }
    }

    public static class RequestDataTakeWithDelay extends RequestDataTake {

        public long	delay;

        public RequestDataTakeWithDelay(Direction direction, BigDecimal amount, BigDecimal rate, long delay) {
            super(direction, amount, rate);
            this.delay = delay;
        }

        @Override
        public String getDescription() {
            return "Take with Delay " + direction.getName() + " " + amount.toString() + " " + rate.toString() + " " + delay;
        }
    }

    public static class RequestDataCancel extends RequestDataDirection {

        public Order order;

        public RequestDataCancel(Direction direction, Order order) {
            super(direction);
            this.order = order;
        }

        @Override
        public String getDescription() {
            return "Cancel " + direction.getName() + " " + order.id;
        }
    }

    public static class RequestDataSummary extends RequestDataDirection {

        public Order order;

        public RequestDataSummary(Direction direction, Order order) {
            super(direction);
            this.order = order;
        }

        @Override
        public String getDescription() {
            return "Summary " + direction.getName() + " " + order.id;
        }
    }

    public static class ResultDataMake extends ResultData {

        public enum Result {
            SUCCESS,
            SUCCESS_COMPLETE,
            FAILURE
        }

        public Result result;
        public Order order;

        public static final ResultDataMake FAILURE = new ResultDataMake(Result.FAILURE, null);

        public ResultDataMake(Result result, Order order) {
            this.result = result;
            this.order = order;
        }

        @Override
        public String getDescription() {
            return "Make " + result.toString();
        }
    }

    public static class ResultDataTake extends ResultData {

        public enum Result {
            SUCCESS,
            FAILURE
        }

        public Result result;
        public BigDecimal amountBase;
        public BigDecimal amountQuote;

        public static final ResultDataTake FAILURE  = new ResultDataTake(Result.FAILURE,  BigDecimal.ZERO, BigDecimal.ZERO);

        public ResultDataTake(Result result, BigDecimal amountBase, BigDecimal amountQuote) {
            this.result = result;
            this.amountBase  = amountBase;
            this.amountQuote = amountQuote;
        }

        @Override
        public String getDescription() {
            return "Take " + result.toString() + " " + amountBase.toString() + " " + amountQuote.toString();
        }
    }

    public static class ResultDataCancel extends ResultData {

        public enum Result {
            SUCCESS,
            FAILURE
        }

        public Result result;

        public static final ResultDataCancel SUCCESS = new ResultDataCancel(Result.SUCCESS);
        public static final ResultDataCancel FAILURE = new ResultDataCancel(Result.FAILURE);

        public ResultDataCancel(Result result) {
            this.result = result;
        }

        @Override
        public String getDescription() {
            return "Cancel " + result.toString();
        }
    }

    public static class ResultDataSummary extends ResultData {

        public enum Result {
            SUCCESS,
            FAILURE
        }

        public Result 		result;
        public BigDecimal 	amountBase;
        public BigDecimal	amountQuote;
 
        public static final ResultDataSummary FAILURE = new ResultDataSummary(Result.FAILURE, BigDecimal.ZERO, BigDecimal.ZERO);

        public ResultDataSummary(Result result, BigDecimal amountBase, BigDecimal amountQuote) {
            this.result     = result;
            this.amountBase = amountBase;
            this.amountQuote= amountQuote;
        }

        @Override
        public String getDescription() {
            return "Summary " + result.toString() + " " + amountBase.toString() + " " + amountQuote.toString();
        }
    }

    private Map<Integer, EventListener> requests;

    // Synchronized property is required because commID should be accessed concurrently.
    public int request(EventListener commClient, RequestData requestData) {
        int commID = marketplacesManager.getCommID();
        synchronized (this) {
            requests.put(commID, commClient);
        }
        connector.requestListener.listen(new Connector.EventRequest(commID, requestData));
        return commID;
    }

    void result(int commID, ResultData resultData) {
        EventListener commClient;
        synchronized (this) {
            commClient = requests.remove(commID);
        }
        if (commClient != null) {
            commClient.listen(new Connector.EventResult(commID, resultData));
        }
    }

    public int make(EventListener commClient, Direction direction, BigDecimal quantity, BigDecimal rate) {
        return request(commClient, new RequestDataMake(direction, quantity, rate));
    }

    public int take(EventListener commClient, Direction direction, BigDecimal quantity, BigDecimal rate) {
        return request(commClient, new RequestDataTake(direction, quantity, rate));
    }
    
    public int takeWithDelay(EventListener commClient, Direction direction, BigDecimal quantity, BigDecimal rate, long delay) {
        return request(commClient, new RequestDataTakeWithDelay(direction, quantity, rate, delay));
    }

    public int cancel(EventListener commClient, Direction direction, Order order) {
        return request(commClient, new RequestDataCancel(direction, order));
    }

    public int cancel(EventListener commClient, Direction direction, String orderID) {
        Order order = getOrder(direction, orderID);
        return cancel(commClient, direction, order);
    }

    public Order getOrder(Direction direction, String orderID) {
        return marketsManager.markets.get(direction).getOrder(orderID);
    }

    public boolean getOrderIsFinished(Direction direction, String offerID) {
        boolean result = false;
        if (marketsManager.markets.get(direction) != null)
            result = marketsManager.markets.get(direction).getOrderIsFinished(offerID);
        return result;
    }

    public void updateMarketOrderStatus(Direction direction, String orderID, Status status) {
        marketsManager.markets.get(direction).updateOrder(orderID, status);
    }

    public void updateMarketOrderTradeSummary(Direction direction, String id, BigDecimal amount, BigDecimal price,
                                              BigDecimal fee, Currency feeCurrency) {
        DirectionPairLocal pairLocal = getLocal(direction.directionPair);
        CurrencyLocal currLocal = getLocal(feeCurrency);

        TradeSummary summary = new TradeSummary(
                pairLocal.scaleOpBase .set(amount),
                pairLocal.scaleOpQuote.set(price ),
                currLocal.scaleOp     .set(fee   ),
                feeCurrency
        );

        marketsManager.markets.get(direction).updateOrderTradeSummary(id, summary);
    }

    public TradeSummary getTradeSummary(Direction direction, String orderID) {
        DirectionPairLocal pairLocal = getLocal(direction.directionPair);

        Order order = marketsManager.markets.get(direction).getOrder(orderID);

        if (order == null) return new TradeSummary();

        return order.getTradeSummary(pairLocal);
    }

    public void addTradeToOrder(Direction direction, String orderID, String tradeID, String amount, String rate, String fee,
                                String feeCurrencyString, Long timestamp) {
        Currency feeCurrency = marketplacesManager.getCurrency(feeCurrencyString);

        addTradeToOrder(direction, orderID, tradeID, amount, rate, fee, feeCurrency, timestamp);
    }

    public void addTradeToOrder(Direction direction, String orderID, String tradeID, String amount, String rate, String fee,
                                Currency feeCurrency, Long timestamp) {
        //log.trace(String.format("Adding trade to order %s, %s, %s, %s, %s, %s, %s, %s", direction, orderID, tradeID, new BigDecimal(amount), new BigDecimal(rate), new BigDecimal(fee), feeCurrency, timestamp));
        addTradeToOrder(direction, orderID, tradeID, new BigDecimal(amount), new BigDecimal(rate), new BigDecimal(fee), feeCurrency, timestamp);
    }

    public void addTradeToOrder(Direction direction, String orderID, String tradeID, BigDecimal amount, BigDecimal rate, BigDecimal fee,
                                Currency feeCurrency, Long timestamp) {

        Market market = marketsManager.markets.get(direction);
        DirectionPairLocal pairLocal = getLocal(direction.directionPair);
        CurrencyLocal currLocal = getLocal(feeCurrency);
        //log.trace(String.format("Adding trade to market order %s, %s, %s, %s, %s, %s, %s, %s, %s", market, direction, orderID, tradeID,amount, rate, fee, feeCurrency, timestamp));
        if (currLocal == null) {
            log.error("Cannot find Local value for fee currency: " + feeCurrency);
            return;
        }
        market.addTradeToOrder(
                orderID,
                tradeID,
                pairLocal.scaleOpBase.set(amount),
                pairLocal.scaleOpRate.set(rate),
                currLocal.scaleOp.set(fee),
                feeCurrency,
                timestamp
        );
    }

    public Order createMarketOrder(Direction direction, String orderID, String amount, String rate,
                                   TimeInForce tif) {
        return createMarketOrder(direction, orderID, amount, rate, tif, System.currentTimeMillis());
    }

    public Order createMarketOrder(Direction direction, String orderID, String amount, String rate,
                                   TimeInForce tif, Long created) {
        DirectionPairLocal pairLocal = getLocal(direction.directionPair);

        BigDecimal amountLocal = pairLocal.scaleOpBase.set(new BigDecimal(amount));
        BigDecimal rateLocal   = pairLocal.scaleOpRate.set(new BigDecimal(rate  ));

        return createMarketOrder(direction, orderID, amountLocal, rateLocal, tif, created);
    }

    public Order createMarketOrder(Direction direction, String orderID, BigDecimal amount, BigDecimal rate,
                                   TimeInForce tif, Long created) {
        //log.trace("Creating new order");
        return marketsManager.markets.get(direction).createOrder(orderID, amount, rate, tif);
    }

}