package Model.Marketplace.Base;

import Application.Events.EventListener;
import Application.Timer;
import Model.Marketplace.Base.Currency.Currency;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

public class Purses implements EventListener {

    private Map<Currency, BigDecimal> purses;
    private long timePursesUpdatePeriod;
    private long timePursesUpdateLast;

    public Purses(long timePursesUpdatePeriod) {
        this.timePursesUpdatePeriod = timePursesUpdatePeriod;
        purses = new HashMap<>();
    }

    public void set(Currency currency, BigDecimal amount) {
        purses.put(currency, amount);
    }

    public BigDecimal get(Currency currency) {
        return purses.get(currency);
    }

    public BigDecimal get0(Currency currency) {
        BigDecimal purse = purses.get(currency);
        return (purse == null) ? BigDecimal.ZERO : purse;
    }

    public void add(Currency currency, BigDecimal amount) {
        BigDecimal purse = get0(currency);
        purses.put(currency, purse.add(amount));
    }

    public void sub(Currency currency, BigDecimal amount) {
        BigDecimal purse = get0(currency);
        purses.put(currency, purse.add(amount));
    }

    @Override
    public void listen(Event event) {
        if (event instanceof Timer.EventTimer) {
            if (timePursesUpdateLast + timePursesUpdatePeriod < System.currentTimeMillis()) {
                updatePurses();
            }
        }
    }

    public void updatePurses() {
        // Note the time of the last update.
        timePursesUpdateLast = System.currentTimeMillis();
    }
}