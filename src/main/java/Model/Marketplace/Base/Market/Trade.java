package Model.Marketplace.Base.Market;

import Model.Marketplace.Base.Currency.Currency;

import java.math.BigDecimal;

public class Trade {
	public String 		id;
	public BigDecimal 	amount;
	public BigDecimal 	rate;	
	public BigDecimal 	fee;
	public Currency feeCurrency;
	public long			created;
	
	public Trade(String id, String amount, String rate, long timestamp) {
		this(id, new BigDecimal(amount), new BigDecimal(rate), timestamp);
	}
	public Trade(String id, BigDecimal amount, BigDecimal rate, long timestamp) {
		this(id, amount, rate, BigDecimal.ZERO, null, timestamp);
	}
	public Trade(String id, BigDecimal amount, BigDecimal rate, BigDecimal fee, Currency feeCurrency, long timestamp) {
		this.id 		= id;
		this.amount 	= amount;
		this.rate 		= rate;
		this.created 	= timestamp;
		this.fee		= fee;
		this.feeCurrency= feeCurrency;
	}

}
