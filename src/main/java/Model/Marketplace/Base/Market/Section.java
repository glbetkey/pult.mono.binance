package Model.Marketplace.Base.Market;

import Model.Marketplace.Base.Currency.RatePolicy;

import java.math.BigDecimal;
import java.util.Iterator;

// This is a pointer to an offer at given depth in base currency along with additional information.
public class Section {

    public void atDepth(Market market, double depth, RatePolicy ratePolicy) {
        atDepth(market, new BigDecimal(depth),ratePolicy);
    }

    public void atDepth(Market market, BigDecimal depth,RatePolicy ratePolicy) {
        create(market, depth, null, true,ratePolicy);
    }

    public void atRate(Market market, double rate,RatePolicy ratePolicy) {
        atRate(market, new BigDecimal(rate),ratePolicy);
    }

    public void atRate(Market market, BigDecimal rate,RatePolicy ratePolicy) {
        create(market, null, rate, false,ratePolicy);
    }

    private void create(Market market, BigDecimal depth, BigDecimal rate, boolean useDepth, RatePolicy ratePolicy) {
        this.ratePolicy = ratePolicy;
        this.market = market;

        depthLeft = useDepth ? depth : BigDecimal.ZERO;
        sumBase = BigDecimal.ZERO;
        sumQuot = BigDecimal.ZERO;

        iteratorOffer = this.market.offers.iterator();
        while (iteratorOffer.hasNext()) {
            offer = iteratorOffer.next();
            BigDecimal amountBase = offer.getAmountBase();
            BigDecimal amountQuot = offer.getAmountQuote();
            if ( useDepth && depthLeft.compareTo(amountBase) > 0
             || !useDepth && ratePolicy.compareRates(market.direction,offer.getRate(),rate) >= 0) {
                if (useDepth) { depthLeft = depthLeft.subtract(amountBase); }
                sumBase = sumBase.add(amountBase);
                sumQuot = sumQuot.add(amountQuot);
            } else {
                return;
            }
        }

        offer = null;
    }

    public BigDecimal getAverageRate() {
//        BigDecimal sumBaseRes;
//        BigDecimal sumQuotRes;
//
//        // If there's an offer at specified depth,
//        if (offer != null) {
//            // Add a proportion of it.
//            sumBaseRes = sumBase.add(depthLeft);
//            sumQuotRes = sumQuot.add(offer.divideToScaleQuot(offer.multiplyToScaleQuot(depthLeft, offer.getAmountQuot()), offer.getAmountBase()));
//        } else {
//            sumBaseRes = sumBase;
//            sumQuotRes = sumQuot;
//        }
//
//        // Calculate the rate as total quote amount divided by total base amountBase.
//        return Currency.divToScale(sumQuotRes, sumBaseRes, Math.max(offer.direction.currencyPair.base.scale,
//                                                                       offer.direction.currencyPair.quot.scale));
        return BigDecimal.ZERO;
    }

    public Market market;
    // Offer at given depth or null if there are no offers at given depth.
    public Offer offer;
    // Iterator to the next offer.
    public Iterator<Offer> iteratorOffer;
    // Sum of base currency amounts up to the given depth.
    public BigDecimal sumBase;
    // Sum of quote currency amounts up to the given depth.
    public BigDecimal sumQuot;
    // Unaccounted depth. Either the next offer is smaller than the unaccounted depth or there are no more
    // offers left.
    public BigDecimal depthLeft;

    RatePolicy ratePolicy;
}
