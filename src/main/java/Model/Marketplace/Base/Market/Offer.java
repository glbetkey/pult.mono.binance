package Model.Marketplace.Base.Market;

import Application.Literals;

import Model.Marketplace.Base.Currency.Direction;
import Model.Marketplace.Base.Currency.DirectionPair;
import Model.Marketplace.Base.Currency.DirectionPairLocal;
import Model.Marketplace.Base.Marketplace;

import java.math.BigDecimal;

public class Offer {

    // Location.
    public Marketplace marketplace;
    public Marketplace.Type marketplaceType;
    public Direction direction;
    public DirectionPair directionPair;
    public DirectionPairLocal directionPairLocal;
    // Numbers.
    private BigDecimal amountBase;
    private BigDecimal amountQuote;
    private BigDecimal rate;
    // Special.
    private boolean autoAmountQuote;
    private boolean amountQuoteUpToDate;

    public Offer(Marketplace marketplace, Direction direction, boolean autoAmountQuote) {
        init(marketplace, direction, autoAmountQuote);
    }

    public Offer(Marketplace marketplace, Direction direction, boolean autoAmountQuote, BigDecimal amountBase, BigDecimal amountQuote, BigDecimal rate) {
        init(marketplace, direction, autoAmountQuote);
        init(amountBase, amountQuote, rate);
    }

    public Offer(Marketplace marketplace, Direction direction, BigDecimal amountBase, BigDecimal rate) {
        init(marketplace, direction, true);
        init(amountBase, rate);
    }

    public Offer(Marketplace marketplace, Direction direction, BigDecimal amountBase, BigDecimal amountQuote, BigDecimal rate) {
        init(marketplace, direction, false);
        init(amountBase, amountQuote, rate);
    }

    public Offer(Marketplace marketplace, Direction direction, double amountBase, double rate) {
        init(marketplace, direction, true);
        init(amountBase, rate);
    }

    public Offer(Marketplace marketplace, Direction direction, double amountBase, double amountQuote, double rate) {
        init(marketplace, direction, false);
        init(amountBase, amountQuote, rate);
    }

    public Offer(Marketplace marketplace, Direction direction, String amountBase, String rate) {
        init(marketplace, direction, true);
        init(amountBase, rate);
    }

    public Offer(Marketplace marketplace, Direction direction, String amountBase, String amountQuote, String rate) {
        init(marketplace, direction, false);
        init(amountBase, amountQuote, rate);
    }

    private void init(Marketplace marketplace, Direction direction, boolean autoAmountQuote) {
        this.marketplace = marketplace;
        marketplaceType = marketplace.type;
        this.direction = direction;
        directionPair = direction.directionPair;
        directionPairLocal = marketplace.getLocal(directionPair);
        this.autoAmountQuote = autoAmountQuote;
        amountQuoteUpToDate = true;
    }

    public void init(BigDecimal amountBase, BigDecimal rate) {
        this.amountBase = amountBase;
        this.rate       = rate;
        amountQuoteUpToDate = false;
    }

    public void init(BigDecimal amountBase, BigDecimal amountQuote, BigDecimal rate) {
        this.amountBase  = amountBase;
        this.amountQuote = amountQuote;
        this.rate        = rate;
        amountQuoteUpToDate = true;
    }

    public void init(double amountBase, double rate) {
        this.amountBase = new BigDecimal(amountBase);
        this.rate       = new BigDecimal(rate);
        amountQuoteUpToDate = false;
    }

    public void init(double amountBase, double amountQuote, double rate) {
        this.amountBase  = new BigDecimal(amountBase);
        this.amountQuote = new BigDecimal(amountQuote);
        this.rate        = new BigDecimal(rate);
        amountQuoteUpToDate = true;
    }

    public void init(String amountBase, String rate) {
        this.amountBase = new BigDecimal(amountBase);
        this.rate       = new BigDecimal(rate      );
        amountQuoteUpToDate = false;
    }

    public void init(String amountBase, String amountQuote, String rate) {
        this.amountBase  = new BigDecimal(amountBase);
        this.amountQuote = new BigDecimal(amountQuote);
        this.rate        = new BigDecimal(rate);
        amountQuoteUpToDate = true;
    }

    //////////////////////////////////////////////////
    // Properties access.

    public void setAmountBase(BigDecimal amountBase) {
        this.amountBase = amountBase;
        amountQuoteUpToDate = false;
    }

    public BigDecimal getAmountBase() {
        return amountBase;
    }

    public BigDecimal getAmountBaseForMaker() {
        return direction.ask ? getAmountBase().negate() : getAmountBase();
    }

    public BigDecimal getAmountBaseForTaker() {
        return direction.ask ? getAmountBase() : getAmountBase().negate();
    }

    public void setAmountQuote(BigDecimal amountQuote) {
        this.amountQuote = amountQuote;
        amountQuoteUpToDate = true;
    }

    public BigDecimal getAmountQuote() {
        if (autoAmountQuote && !amountQuoteUpToDate) {
            amountQuote = marketplace.convert(directionPair, amountBase, rate);
            amountQuoteUpToDate = true;
        }
        return amountQuote;
    }

    public BigDecimal getAmountQuoteForMaker() {
        return direction.ask ? getAmountQuote() : getAmountQuote().negate();
    }

    public BigDecimal getAmountQuoteForTaker() {
        return direction.ask ? getAmountQuote().negate() : getAmountQuote();
    }

    public void setRate(BigDecimal rate) {
        this.rate = rate;
        amountQuoteUpToDate = false;
    }

    public BigDecimal getRate() {
        return rate;
    }

    public void validateAmountQuote() {
        amountQuoteUpToDate = true;
    }

    public String toString() {
        return String.format(Literals.nameOffer, amountBase.toPlainString(), rate.toPlainString());
    }
}
