package Model.Marketplace.Base.Market;

import Model.Marketplace.Base.Currency.Currency;
import Model.Marketplace.Base.Currency.Direction;
import Model.Marketplace.Base.Currency.RatePolicy;
import Model.Marketplace.Base.Market.Order.Status;
import Model.Marketplace.Base.Market.Order.TimeInForce;

import java.math.BigDecimal;
import java.util.*;

public class Market {

    public Direction direction;
    public List<Offer> offers;
    public Map<String, Order> orders;

    //////////////////////////////////////////////////
    // Constructor.

    public Market(Direction direction) {
        this.direction = direction;
        offers = new ArrayList<>();
        orders = Collections.synchronizedMap(new HashMap<>());
    }

    public void setOffers(List<Offer> offers) {
        synchronized (this) {
            this.offers = offers;
        }
    }

    //////////////////////////////////////////////////
    // General purpose.

    @Override
    public String toString() { return getName(); }

    public String getName() { return (direction == null) ? "" : direction.getName(); }

    public Section getSectionAtDepth(double depth, RatePolicy ratePolicy) {
        Section section = new Section();
        section.atDepth(this, depth, ratePolicy);
        return section;
    }

    public Section getSectionAtDepth(BigDecimal depth, RatePolicy ratePolicy) {
        Section section = new Section();
        section.atDepth(this, depth, ratePolicy);
        return section;
    }

    public Section getSectionAtRate(double rate, RatePolicy ratePolicy) {
        Section section = new Section();
        section.atRate(this, rate, ratePolicy);
        return section;
    }

    public Section getSectionAtRate(BigDecimal rate, RatePolicy ratePolicy) {
        Section section = new Section();
        section.atRate(this, rate, ratePolicy);
        return section;
    }

    public Offer getOfferTop() {
        if (offers.size() == 0) {
            return null;
        }

        return offers.get(0);
    }

    public Offer getOfferBot() {
        if (offers.size() == 0) {
            return null;
        }

        return offers.get(offers.size() - 1);
    }

    public BigDecimal getRateTop() {
        if (offers.size() == 0) {
            return null;
        }

        return offers.get(0).getRate();
    }

    public Order getOrder(String offerID) {
        synchronized (orders) {
            return orders.get(offerID);
        }
    }

    public Order createOrder(String orderID, String amount, String rate, TimeInForce tif) {
        return createOrder(orderID, new BigDecimal(amount), new BigDecimal(rate), tif);
    }

    public Order createOrder(String orderID, BigDecimal amount, BigDecimal rate, TimeInForce tif) {
        return createOrder(orderID, amount, rate, tif, System.currentTimeMillis());
    }

    public Order createOrder(String orderID, BigDecimal amount, BigDecimal rate, TimeInForce tif, long market_created) {
        Order order = null;
        synchronized (orders) {
            // create only if not exists
            if (!orders.containsKey(orderID)) {
                System.out.println("Order not found. creating new one for " + orderID);
                order  = new Order();

                order.id     = orderID;
                order.amount = amount;
                order.rate   = rate;
                order.tif	 = tif;
                orders.put(orderID, order);

                order.setStatus(Status.CREATED);
            } else {
                order = orders.get(orderID);
            }
        }
        return order;
    }

    public boolean getOrderIsFinished(String offerID) {
        Order order = getOrder(offerID);

        return order == null ? false : order.isFinished();
    }

    public void updateOrder(String orderID, Status status) {
        Order order = orders.get(orderID);

        if (order == null) return;

        order.setStatus(status);
    }

    public void updateOrderTradeSummary(String orderID, TradeSummary summary) {
        Order order = orders.get(orderID);

        if (order == null) return;

        order.updateTradeSummary(summary);
    }

    public void addTradeToOrder(String orderID, String tradeID, BigDecimal amount, BigDecimal rate, BigDecimal fee,
            Currency feeCurrency) {
        addTradeToOrder(orderID, tradeID, amount, rate, fee, feeCurrency, System.currentTimeMillis());
    }

    public void addTradeToOrder(String orderID, String tradeID, BigDecimal amount, BigDecimal rate, BigDecimal fee,
                                Currency feeCurrency, Long timestamp) {
        //System.out.println("Market: AddTradeToOrder: before get" + orderID);
        Order order = getOrder(orderID);
        //System.out.println("Market: AddTradeToOrder: " + order);
        if (order != null) {
            order.addTrade(tradeID, amount, rate, fee, feeCurrency, timestamp);
        }
    }
}
