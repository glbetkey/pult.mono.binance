package Model.Marketplace.Base.Market;

import Model.Marketplace.Base.Currency.Currency;

import java.math.BigDecimal;

public class TradeSummary {
	public BigDecimal 	amount;
	public BigDecimal 	price;
	public BigDecimal 	fee;
	public Currency feeCurrency;

	public TradeSummary(BigDecimal amount, BigDecimal price, BigDecimal fee, Currency feeCurrency) {
		this.amount 	 = amount;
		this.price  	 = price;
		this.fee    	 = fee;
		this.feeCurrency = feeCurrency;
	}

	public TradeSummary() {
		this(BigDecimal.ZERO, BigDecimal.ZERO, BigDecimal.ZERO, null);
	}

	public String getDescription() {
		return String.format("Trade summary: %s, %s, %s, %s", amount.toPlainString(), price.toPlainString(), fee.toPlainString(), feeCurrency.getName());
	}
}
