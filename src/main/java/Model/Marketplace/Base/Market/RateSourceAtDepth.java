package Model.Marketplace.Base.Market;

import Model.Marketplace.Base.Currency.DirectionPair;
import Model.Marketplace.Base.Marketplace;

import java.math.BigDecimal;
import java.math.RoundingMode;

/**
 * Created by FlyingPirate on 03.12.2017.
 */
public class RateSourceAtDepth {

    Marketplace marketplace;

    public RateSourceAtDepth(Marketplace marketplace) {
        this.marketplace = marketplace;
    }

    public BigDecimal rateAtPlast(DirectionPair directionPair, double plast){

        Section sectionAsk = new Section();
        Section sectionBid = new Section();
        Market marketAsk =marketplace.marketsManager.markets.get(directionPair.ask);
        Market marketBid =marketplace.marketsManager.markets.get(directionPair.bid);

        sectionAsk.atDepth(marketAsk, plast, marketplace.ratePolicy);
        sectionBid.atDepth(marketBid, plast, marketplace.ratePolicy);

        return sectionAsk.offer.getRate().add(sectionBid.offer.getRate()).divide(new BigDecimal(2),marketplace.getLocal(directionPair).scaleOpRate.scale, RoundingMode.HALF_EVEN);
    }
}
