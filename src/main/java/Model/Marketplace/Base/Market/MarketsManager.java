package Model.Marketplace.Base.Market;

import Model.Marketplace.Base.Currency.Direction;
import Model.Marketplace.Base.Currency.DirectionPair;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class MarketsManager {

    public Set<DirectionPair> marketsRequired;
    public Map<Direction, Market> markets;

    public MarketsManager() {
        marketsRequired = new HashSet<>();
        markets         = new HashMap<>();
    }

    public void requireMarkets(Set<DirectionPair> marketsRequired) {
        if (marketsRequired == null) {
            this.marketsRequired.clear();
            markets.clear();
        } else {
            for (DirectionPair directionPair : this.marketsRequired) {
                if (!marketsRequired.contains(directionPair)) {
                    markets.remove(directionPair.ask);
                    markets.remove(directionPair.bid);
                }
            }

            for (DirectionPair directionPair : marketsRequired) {
                if (!this.marketsRequired.contains(directionPair)) {
                    markets.put(directionPair.ask, new Market(directionPair.ask));
                    markets.put(directionPair.bid, new Market(directionPair.bid));
                }
            }

            this.marketsRequired = marketsRequired;
        }
    }
}
