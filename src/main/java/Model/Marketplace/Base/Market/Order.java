package Model.Marketplace.Base.Market;

import Model.Marketplace.Base.Currency.Currency;
import Model.Marketplace.Base.Currency.DirectionPairLocal;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

public class Order {
	//
	// Order vars
	public Status 		status;
	public String 		id;
	public BigDecimal 	amount;
	public BigDecimal 	rate;
	public BigDecimal 	filled;
	public long 		created;
	public long 		market_created;
	public long			closed;
	public TimeInForce  tif;
	public List<Trade>	fills;
	public TradeSummary summary;
	// status
	public enum Status {
		OBJECT_CREATED,
		CREATING,
		CREATED,
		CANCELLING,
		CANCELLED,
		PARTIALLY_FILLED,
		FILLED, 
		REJECTED,
		EXPIRED;
		
	}
	// side
	public enum Side {
		SELL, BUY;
	}
	// type
	public enum Type {
		LIMIT, MARKET;
	}
	// TIF
	public enum TimeInForce {
		GTC, IOC;
	}

	public Order() {
		super();
		setStatus(Status.OBJECT_CREATED);
		amount 	= BigDecimal.ZERO;
		rate 	= BigDecimal.ZERO;
		filled 	= BigDecimal.ZERO;
		created = 0;
		closed  = 0;
		tif     = TimeInForce.GTC;
		fills 	= new ArrayList<>();
	}
	// set status with notify
	public void setStatus(Status status) {
		synchronized (this) {
			this.status = status;
			switch (this.status.name()) {
			case "OBJECT_CREATED":
				this.created = System.currentTimeMillis();
				break;
			case "CANCELLED":
			case "FILLED":
				this.closed = System.currentTimeMillis();
				break;
			}
			this.notifyAll();
		};
		System.out.println("Order status changed to: " + this.id + " : " + this.status.name());
	}
	public Trade addTrade(String id, String amount, String rate, String fee,
                          Currency feeCurrency) {
		return addTrade(id, new BigDecimal(amount), new BigDecimal(rate), new BigDecimal(fee), feeCurrency, System.currentTimeMillis());
	}
	public Trade addTrade(String id, BigDecimal amount, BigDecimal rate, BigDecimal fee, Currency feeCurrency, long timestamp) {
		Trade trade = new Trade(id, amount, rate, fee, feeCurrency, timestamp);
		addTrade(trade);
		return trade;
	}
	public void addTrade(Trade trade) {
		Trade existingTrade = null;
		synchronized (this) {
			synchronized (fills) {
				for (Trade tradeLocal : fills) {
					if (tradeLocal.equals(trade)) {
						existingTrade = tradeLocal;
					}
				}
				if (existingTrade == null) {
					fills.add(trade);
					filled = filled.add(trade.amount);
					fills.notifyAll();
				}
			}
			if (filled.equals(amount)) setStatus(Status.FILLED);
			notifyAll();
		}
	}
	public boolean isActive() {
		boolean result = false;
		synchronized (this) {
			//System.out.println("Order status: " + status.name() + " " + this);
			if (this.status.equals(Status.CREATED			) 
			 || this.status.equals(Status.CREATING			) 
			 || this.status.equals(Status.PARTIALLY_FILLED	)
			) {
				result = true;
			}
		}
		return result;
	}
	public boolean isFinished() {
		boolean result = false;
		synchronized (this) {
			//System.out.println("Order status: " + status.name() + " " + this);
			if (this.status.equals(Status.FILLED	) 
			 || this.status.equals(Status.EXPIRED	) 
			 || this.status.equals(Status.CANCELLED	)
			 || this.status.equals(Status.REJECTED	)
			) {
				result = true;
			}
		}
		return result;
	}
	
	public void clearTrades() {
		synchronized (fills) {
			fills.clear();
		}
		
	}
	public void updateTradeSummary(TradeSummary summary) {
		synchronized (this) {
			if (this.summary == null) {
				this.summary = summary;
			} else {
				synchronized (summary) {
					this.summary.amount 	 = summary.amount;
					this.summary.price  	 = summary.price;
					this.summary.fee    	 = summary.fee;
					this.summary.feeCurrency = summary.feeCurrency;
				}
			}
			notifyAll();
		}
	}
	public TradeSummary getTradeSummary(DirectionPairLocal pairLocal) {
		if (summary == null) {
			TradeSummary summaryLocal = new TradeSummary();
			//calculate summary from fills
			for (Trade trade : fills) {
				if (summaryLocal.feeCurrency == null) {
					summaryLocal.feeCurrency = trade.feeCurrency;
				}
				summaryLocal.amount = summaryLocal.amount.add(trade.amount);
				summaryLocal.price  = summaryLocal.price .add(
						pairLocal.scaleOpQuote.mul(trade.amount, trade.rate)
				);
				summaryLocal.fee    = summaryLocal.fee   .add(trade.fee);
			}
			summary = summaryLocal;
		}
		return summary;
	}
	
	public void waitForFinish() {
		waitForFinish(-1);
	}
	public void waitForFinish(long timeout) {
		Long started = System.currentTimeMillis();
		synchronized (this) {
			while (!isFinished()) {
				if (timeout > 0 && System.currentTimeMillis() - started > timeout) break;
				try {
					wait(100);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					//e.printStackTrace();
					break;
				}
			}
		}
	}
}
