package Model.Marketplace.Binance;


import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.URL;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;
import javax.net.ssl.HttpsURLConnection;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;


/**
 * @author user
 *
 */
public class ClientBinance {

	protected static Logger log = LogManager.getLogger(ClientBinance.class.getName());

	private String	websiteUrl 		= "https://www.binance.com/";

	public	static 	String USER_AGENT = "TradeApi Manager";

	private String 	apiKey;
	private String 	apiSecret;

	private volatile long timeDiff = 0;
	private volatile long timeDiffUpdated = 0;
	// time sync expires within 1 mins
	private static long timeDiffExpires =  60 * 1000;

	public enum ReqType {
		WEBSITE,
		PUBLIC,
		APIKEY,
		SIGNED;
	}
	public enum Action {
		PRODUCTS		(ReqType.WEBSITE,"GET", 	"",		"exchange/public/product"),
		PING			(ReqType.PUBLIC, "GET", 	"v1",	"ping"),
		TIME			(ReqType.PUBLIC, "GET", 	"v1",	"time"),
		DEPTH			(ReqType.PUBLIC, "GET", 	"v1",	"depth"),
		AGG_TRADES		(ReqType.PUBLIC, "GET", 	"v1",	"aggTrades"),
		KLINES			(ReqType.PUBLIC, "GET", 	"v1",	"klines"),
		TICKER_24H		(ReqType.PUBLIC, "GET", 	"v1",	"ticker/24h"),
		TICKER_PRICES	(ReqType.PUBLIC, "GET", 	"v1",	"ticker/allPrices"),
		TICKER_BOOKS	(ReqType.PUBLIC, "GET", 	"v1",	"ticker/allBookTickers"),
		ORDER_NEW 		(ReqType.SIGNED, "POST", 	"v3",	"order"),
		ORDER_NEW_TEST 	(ReqType.SIGNED, "POST", 	"v3",	"order/test"),
		ORDER_INFO		(ReqType.SIGNED, "GET",  	"v3", 	"order"),
		ORDER_CANCEL	(ReqType.SIGNED, "DELETE",	"v3", 	"order"),
		ORDERS_OPEN		(ReqType.SIGNED, "GET",		"v3",	"openOrders"),
		ORDERS_ALL		(ReqType.SIGNED, "GET", 	"v3", 	"allOrders"),
		ACCOUNT			(ReqType.SIGNED, "GET",		"v3",	"account"),
		ACCOUNT_TRADES	(ReqType.SIGNED, "GET",		"v3", 	"myTrades"),
		STREAM_CREATE	(ReqType.APIKEY, "POST",	"v1", 	"userDataStream"),
		STREAM_PING		(ReqType.APIKEY, "PUT",		"v1",	"userDataStream"),
		STREAM_CLOSE	(ReqType.APIKEY, "DELETE",	"v1",	"userDataStream");

		public String 	method;
		public String 	apiVersion;
		public String 	apiAction;
		public ReqType 	type;

		Action(ReqType type, String method, String apiVersion, String apiAction) {
			this.type 		= type;
			this.method 	= method;
			this.apiVersion = apiVersion;
			this.apiAction 	= apiAction;
		}
	}

	public ClientBinance() {
	}

	public ClientBinance(String apiKey, String apiSecret) {
		this.apiKey = apiKey;
		this.apiSecret = apiSecret;
	}

	protected void updateTimeDiff() {
		long ourTime = System.currentTimeMillis();
		if (timeDiffUpdated > 0 && ourTime - timeDiffUpdated < timeDiffExpires) {
			return;
		}

		JsonNode node = this.queryJson(Action.TIME);
		if (node != null && node.get("serverTime") != null) {
			long serverTime = node.get("serverTime").asLong();
			timeDiff = serverTime - ourTime - (System.currentTimeMillis() - ourTime) / 2;
			timeDiffUpdated = System.currentTimeMillis();
			log.trace("Time sync: " + ourTime + " server: " + serverTime + " diff: " + timeDiff);
		} else {
			log.error("Cannot sync time");
		}

	}

	public String query(Action action) {
		return query(action, new HashMap<String, String>());
	}
	public String query(Action action, Map<String, String> params) {

		String url = websiteUrl;

		if (action.type == ReqType.WEBSITE) {
			url += action.apiAction;
		} else {
			url += String.format("api/%s/%s", action.apiVersion, action.apiAction);
		}

		if (action.type == ReqType.SIGNED) {
			params.put("timestamp", String.valueOf(System.currentTimeMillis() + timeDiff));
		}

		String queryString = buildQueryString(params);

		if (action.type == ReqType.SIGNED) {
			queryString += "&signature=" + getApiSign(queryString);
		}
		if (!queryString.isEmpty())	url += '?' + queryString;

		//log.trace(action.method + ": " + url);

		HttpsURLConnection conn = getConnection(url);

		if (conn == null) return null;

		if (action.type == ReqType.APIKEY || action.type == ReqType.SIGNED) {
			conn.addRequestProperty("X-MBX-APIKEY", apiKey);
		}

		try {
			conn.setRequestMethod(action.method);

			if (conn.getResponseCode() != 200) {
				log.trace(String.format("%s : %s", url, conn.getResponseCode()));
				log.trace(conn.getResponseMessage());
				log.trace(readResponse(conn));
				return null;
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}

		return readResponse(conn);
	}

	public JsonNode queryJson(Action action) {
		return queryJson(action, new HashMap<String, String>());
	}

	public JsonNode queryJson(Action action, Map<String, String> params) {
		String res = query(action, params);

		if (res == null) return null;

		ObjectMapper mapper = new ObjectMapper();
		JsonNode result = null;

		try {
			result = mapper.readTree(res);

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		if (result.get("code") != null) {
			log.error("queryJson: response error: " + result.get("code").asText() + ": " + result.get("msg").asText());
			return null;
		}

		return result;

	}


	/**
	 * @param conn
	 * @return String - http response body
	 */
	protected String readResponse(HttpsURLConnection conn) {
		try {
			BufferedReader in = new BufferedReader(
					new InputStreamReader(conn.getInputStream())
			);
			String inputLine;
			StringBuffer response = new StringBuffer();

			while ((inputLine = in.readLine()) != null) {
				response.append(inputLine);
			}

			in.close();

			return response.toString();
		} catch (IOException e) {
			return null;
		}
	}
	/**
	 * @param url
	 * @return HttpsURLConnection
	 * Return HttpsURLConnection object for given String url
	 */
	protected HttpsURLConnection getConnection(String url) {
		HttpsURLConnection conn = null;
		try {
			conn = (HttpsURLConnection) ((new URL(url)).openConnection());
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
		conn.setRequestProperty("User-Agent", USER_AGENT	    );
		//conn.setRequestProperty("Accept",	  "application/json");
		return conn;
	}

	/**
	 * @param params
	 * @return Strring
	 * Generate requestString based on map params (used for both, GET and POST)
	 */
	protected String buildQueryString(Map<String, String> params) {
		String queryString = "";

		for (Entry<String, String> e : params.entrySet()) {
			queryString += (queryString.isEmpty()) ? "" : "&";
			queryString += e.getKey() + "=" + e.getValue();
		}

		return queryString;
	}

	private String getApiSign(String queryString) {
		Mac m = null;
		byte[] byteKey;
		try {
			byteKey = apiSecret.getBytes("UTF-8");
			m = Mac.getInstance("HmacSHA256");
			SecretKeySpec keySpec = new SecretKeySpec(byteKey, "HmacSHA256");
			m.init(keySpec);
		} catch (UnsupportedEncodingException | NoSuchAlgorithmException | InvalidKeyException e) {
			e.printStackTrace();
		}

		return bytesToHex(m.doFinal(queryString.getBytes()));
	}
	// convert bytes to HEX String
	protected String bytesToHex(byte[] bytes) {
		char[] hexArray = "0123456789ABCDEF".toCharArray();
		char[] hexChars = new char[bytes.length * 2];
		for ( int j = 0; j < bytes.length; j++ ) {
			int v = bytes[j] & 0xFF;
			hexChars[j * 2] = hexArray[v >>> 4];
			hexChars[j * 2 + 1] = hexArray[v & 0x0F];
		}
		return new String(hexChars);
	}



	private JsonNode StringToJson(String str) {

		if (str == null) return null;

		ObjectMapper mapper = new ObjectMapper();
		JsonNode result = null;

		try {
			result = mapper.readTree(str);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return result;
	}

}
