package Model.Marketplace.Binance;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.TreeMap;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectReader;
import com.neovisionaries.ws.client.WebSocket;
import com.neovisionaries.ws.client.WebSocketAdapter;
import com.neovisionaries.ws.client.WebSocketException;
import com.neovisionaries.ws.client.WebSocketFactory;
import com.neovisionaries.ws.client.WebSocketFrame;
import com.neovisionaries.ws.client.WebSocketState;

import Application.Events.EventListener;
import Application.Events.EventListenerAsync;
import Application.Events.EventListener.Event;
import Application.Preferences.LocalSettings.InfoProviderAuth;
import Model.Marketplace.MarketplacesManager;
import Model.Marketplace.Base.Connector;
import Model.Marketplace.Base.Marketplace;
import Model.Marketplace.Base.Marketplace.ResultDataSummary;
import Model.Marketplace.Base.Marketplace.ResultDataTake;
import Model.Marketplace.Base.Currency.Currency;
import Model.Marketplace.Base.Currency.CurrencyLocal;
import Model.Marketplace.Base.Currency.Direction;
import Model.Marketplace.Base.Currency.DirectionPair;
import Model.Marketplace.Base.Currency.DirectionPairLocal;
import Model.Marketplace.Base.Market.Market;
import Model.Marketplace.Base.Market.MarketsManager;
import Model.Marketplace.Base.Market.Offer;
import Model.Marketplace.Base.Market.Order;
import Model.Marketplace.Base.Market.Order.TimeInForce;
import Model.Marketplace.Base.Market.Trade;
import Model.Marketplace.Binance.ClientBinance.Action;
import Model.Marketplace.Binance.ConnectorBinance.BinanceOrder.BinanceTimeInForce;
import Model.Marketplace.Binance.ConnectorBinance.BinanceOrder.OrderSide;
import Model.Marketplace.Binance.ConnectorBinance.BinanceOrder.OrderStatus;
import Model.Marketplace.Binance.ConnectorBinance.BinanceOrder.OrderType;
import Utils.ScaleOp;

/**
 * @author user
 *
 */
public class ConnectorBinance extends Connector {

	protected static Logger log = LogManager.getLogger(ConnectorBinance.class.getName());

	// Max time to wait for full trade result
	private static final long MAX_TRADE_WAIT_TIMEOUT = 5000;
	// place order for validating but not forward to market
	private static final boolean USE_TEST_ORDERS = false;

	private static final BigDecimal TRADE_FEE = new BigDecimal("0.0005");
	// Use web-sockets for depth updates or not
	private static final boolean USE_WEBSOCKET = true;
	// Max message queue size. If queue gets more messages, then disconnect web-socket and reinit.
	private static final int DEPTH_MESSAGE_QUEUE_MAX_SIZE = 20;
	// minimum offers table size, if less - restart web-socket with initial download of rates
	private static final int MIN_OFFERS_SIZE = 20;

	private static final String WEB_SOCKET_BASE_URL = "wss://stream.binance.com:9443/ws/";

	private static final boolean USER_SOCKET_DISCONNECT_TEST = false;

	// Max order book records to load when not use web-sockets
	protected static int MAX_OFFERS = 20;

	/* Thread cycle wait time in ms */
	private static int TIMER_TIMEOUT = 100;
	/* Market update time in ms without web-sockets */
	private static long MARKET_UPDATE_TIME = 1000;

	private volatile boolean in_shutdown = false;

	public static class BinanceMarket {
		public String quoteAsset;
		public String quoteAssetName;
		public int activeBuy;
		public double prevClose;
		public String symbol;
		public String withdrawFee;
		public String status;
		public String minQty;
		public String minTrade;
		public String baseAssetUnit;
		public String quoteAssetUnit;
		public int activeSell;
		public int lastAggTradeId;
		public String close;
		public int decimalPlaces;
		public String open;
		public String baseAsset;
		public String baseAssetName;
		public String volume;
		public Boolean active;
		public double tradedMoney;
		public String high;
		public String low;
		public String tickSize;
		public String matchingUnitType;

		public BinanceMarket() {
			super();
		}
	}

	public static class BinanceOrder {
		public enum OrderSide {
			BUY ("BUY"),
			SELL("SELL");

			public String value;

			OrderSide(String value) {
				this.value = value;
			}
		}
		public enum OrderStatus {
			NEW 			("NEW"),
			PARTIALLY_FILLED("PARTIALLY_FILLED"),
			FILLED			("FILLED"),
			CANCELED		("CANCELED"),
			PENDING_CANCEL	("PENDING_CANCEL"),
			REJECTED		("REJECTED"),
			EXPIRED			("EXPIRED");

			public String value;

			OrderStatus (String value) {
				this.value = value;
			}

		}
		public enum OrderType {
			LIMIT	("LIMIT"),
			MARKET	("MARKET");

			public String value;

			OrderType (String value) {
				this.value = value;
			}
		}
		public enum BinanceTimeInForce {
			GTC		("GTC"),
			IOC		("IOC");

			public String value;

			BinanceTimeInForce(String value) {
				this.value = value;
			}
		}

		public String	symbol;
		public String 	orderId;
		public String 	clientOrderId;
		public String 	price;
		public String	origQty;
		public String	executedQty;
		public String 	status;
		public String 	timeInForce;
		public String 	type;
		public String 	side;
		public String 	stopPrice;
		public String 	icebergQty;
		public long 	time;
		public boolean 	isWorking;
		public List<BinanceTrade> trades;

		public BinanceOrder() {
			super();
			trades = new ArrayList<>();
		}
		public void fillOrder(Order order) {
			syncOrderStatus(this.status, order);
		}
	}

	public static class BinanceAccountInformation {
		public BigDecimal	makerCommission;
		public BigDecimal	takerCommission;
		public BigDecimal	buyerCommission;
		public BigDecimal	sellerCommission;

		public Boolean 		canTrade;
		public Boolean 		canWithdraw;
		public Boolean 		canDeposit;
		public Map<String, BinanceBalance> 		balances;


	}
	public static class BinanceBalance {
		public String		asset;
		public BigDecimal	free;
		public BigDecimal	locked;
	}
	public static class BinanceBalanceSmall {
		public String		a;
		public BigDecimal	f;
		public BigDecimal	l;

		public void fillNormal(BinanceBalance b) {
			b.asset  = a;
			b.free 	 = f;
			b.locked = l;
		}
	}
	public enum BinanceTradeStatus {
		PARTIALLY_FILLED,
		FILLED;
	}
	public static class BinanceTrade {
		public String id;
		public String pair;
		public String type;
		public String side;
		public String status;
		public String qty;
		public String price;
		public String accAmount;
		public String orderId;
		public int is_your_order;
		public long time;
		public String commission;
		public String commissionAsset;
		public boolean isBuyer;
		public boolean isMaker;
		public boolean isBestMatch;
		public long clientOrderId;

		public BinanceTrade() {
			super();
		}

		public Trade getTrade() {
			return new Trade(
					String.valueOf(id),
					qty,
					price,
					time
			);
		}
	}
	class MessageQueue {
		private Map<Long, String> queue;
		private ObjectMapper mapper;
		private JsonNode root;
		private DirectionPair pair;

		MessageQueue (DirectionPair pair) {
			queue = new TreeMap<>();
			mapper = new ObjectMapper();
			this.pair = pair;
		}

		public synchronized void put(String message) {
			//System.out.println("Adding message to queue: " + message);
			try {
				root = mapper.readTree(message);
				if (root == null) return;
				JsonNode fromUpdate = root.get("U");
				if (fromUpdate == null) {
					fromUpdate = root.get("u");
				}
				queue.put(fromUpdate.asLong(), message);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		public synchronized void process() {

			while(!queue.isEmpty()) {

				if (marketUpdateIds.get(pair) == null) return;

				Long updateId = marketUpdateIds.get(pair) + 1;
				//log.trace("trying to process update with ID: " + updateId);
				if (queue.containsKey(updateId)) {
					processMessage(queue.get(updateId));
					queue.remove(updateId);

				} else {
					// first update
					// try to find message with U < updateId and u > updateId
					String needed = null;
					for (String msg : queue.values()) {
						try {
							root = mapper.readTree(msg);
							if (root.get("U") != null) {
								if (root.get("U").asLong() <= updateId && root.get("u").asLong() >= updateId) {
									needed = msg;
								}
							}
						} catch (IOException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}
					if (needed != null) {
						processMessage(needed);

					}
				}
				if (!queue.isEmpty()) {
					// remove old items
					for (Long i : queue.keySet()) {
						//log.trace("trying to clear queue: " + i + " < " + updateId);
						if (i < updateId) queue.remove(i);
					}
				}

			}
		}

		private void processMessage(String msg) {
			try {
				root = mapper.readTree(msg);
				//log.trace(String.format("Processing %s => %s for %s", root.get("U") == null ? root.get("u") : root.get("U"), root.get("u"), root.get("s")));
				processOffersDiff(root.get("b"), pair.bid);
				processOffersDiff(root.get("a"), pair.ask);
				//log.trace("Set next update to " + root.get("u").asLong());
				marketUpdateIds.put(pair, root.get("u").asLong());
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		public Boolean isOverloaded() {
			return queue.size() > DEPTH_MESSAGE_QUEUE_MAX_SIZE;
		}
	}

	private volatile Boolean initiated = false;
	private volatile Boolean init_started = false;


	private volatile ClientBinance client;

	private ObjectMapper				mapper = new ObjectMapper();
	private BinanceAccountInformation 	accountInformation;

	protected Map<DirectionPair, BinanceMarket> markets;
	protected Map<String, DirectionPair> name2pairs;
	protected Map<DirectionPair, Long> marketsUpdated = new HashMap<DirectionPair, Long>();
	protected List<BinanceTrade> tradeHistory = new ArrayList<>();

	protected volatile Map<DirectionPair, WebSocket> websockets = new HashMap<>();
	protected volatile WebSocket userDataWS = null;
	//55 mins in msec
	private   static   int 		 userDataWSPingTime = 10 * 60 * 1000;
	protected long 				 userDataWSLastPing = 0;

	protected volatile Map<DirectionPair, Long> marketUpdateIds = new HashMap<>();
	protected volatile Map<DirectionPair, MessageQueue> messageQueues = new HashMap<>();

	private MarketsManager marketsManager;

	//TODO: remove, temporary for tests
	private String userDataStreamUri = null;// = WEB_SOCKET_BASE_URL + "DdpP4sutNhVR1Gw5qdfsjdqgKAo5Kl8TUax522Y0EKtBV0o2TD7X9Aj9oP3U";

	private long lastTestTime = 0;

	public ConnectorBinance(MarketplacesManager marketplacesManager, Marketplace marketplace, InfoProviderAuth infoProviderAuth) {
		super(marketplacesManager, marketplace);
		this.marketsManager = marketplace.marketsManager;
		client = new ClientBinance(infoProviderAuth.publicKey, infoProviderAuth.secretKey);

		log.trace("Binance running.");

		startTimer(TIMER_TIMEOUT);

	}

	// transfor json to Offer array and call Connector updateDiff method
	public void processOffersDiff(JsonNode jsonNode, Direction dir) {
		//log.trace("Processing Diff " + dir);
		DirectionPairLocal directionPairLocal = marketplace.getLocal(dir.directionPair);

		List<Offer> offers = new ArrayList<>();

		jsonNode.forEach((JsonNode node) -> {
			BigDecimal rate = new BigDecimal(node.get(0).asText());
			BigDecimal qty  = new BigDecimal(node.get(1).asText());

			Offer off = new Offer(marketplace, dir,
					directionPairLocal.scaleOpBase.set(qty ),
					directionPairLocal.scaleOpRate.set(rate));

			offers.add(off);
		});

		updateMarketDataDiff(offers, dir);

	}


	@Override
	public void done() {

		in_shutdown = true;

		super.done();

		System.out.println("Binance: Stop signal received");
		// disconnect active sockets
		boolean open_found = true;
		while (open_found) {
			open_found = false;
			//log.trace("Search for open WebSockets");
			synchronized (websockets) {
				for (WebSocket ws: websockets.values()) {
					//log.trace(ws + " " + ws.getState());
					if (ws != null && ws.getState() != WebSocketState.CLOSED && ws.getState() != WebSocketState.CLOSING) {
						ws.sendClose();
						open_found = true;
						//log.trace("Open websocket found " + ws.getState());
					}

				}
			}
			if (userDataWS != null) {
				synchronized (userDataWS) {
					if (userDataWS.getState() != WebSocketState.CLOSED && userDataWS.getState() != WebSocketState.CLOSING)	{
						userDataWS.sendClose();
						open_found = true;
					}
				}
			}
		}



		Map<String, String> params = new HashMap<>();
		params.put("listenKey", userDataStreamUri.replace(WEB_SOCKET_BASE_URL, ""));
		client.query(ClientBinance.Action.STREAM_CLOSE, params);

	}

	protected void initMarketplace() {


		initMarkets();
		// Init account after markets to get currency scale

		loadAccountInfo();

		// add required markets
		Set<DirectionPair> req = new HashSet<>();
		//req.add(marketplacesManager.getDirectionPair("SNM", "BTC"));
		req.add(marketplacesManager.getDirectionPair("BNB", "BTC"));

		marketsManager.requireMarkets(req);

		// load active orders
		initOrders();
		initiated = true;

		broadcastEventMarketplaceInitialized();
	}
	private void initOrders() {

		for (DirectionPair pair : marketsManager.marketsRequired) {
			loadOrdersByPair(pair);
		}

		return;

	}

	private void loadOrdersByPair(DirectionPair pair) {
		BinanceMarket bm = markets.get(pair);

		if (bm == null) {
			log.error("loadOrdersByPair: wrong pair, no market for " + pair);
			return;
		}
		Map <String, String> params = new HashMap<>();
		params.put("symbol", bm.symbol);
		JsonNode root = null;
		while (root == null) {
			root = client.queryJson(Action.ORDERS_OPEN, params);
			if (root == null) {
				log.error("loadOrdersByPair: null response received, sleep and retry");
				try {
					Thread.sleep(500);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					log.error(e);
				}
			}
		}

		ObjectReader reader = mapper.readerFor(BinanceOrder.class);

		root.forEach( node -> {
			try {
				BinanceOrder border = reader.readValue(node);

				// query trades
				loadOrderTrades(bm, border);

				syncToMarketOrder(border);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				log.error(e);
			}
		});


	}

	private void loadOrderTrades(BinanceMarket bm, BinanceOrder border) {
		Map<String, String> tparams = new HashMap<>();
		tparams.put("symbol", bm.symbol);

		JsonNode troot = client.queryJson(Action.ACCOUNT_TRADES, tparams);
		if (troot != null) {
			ObjectReader treader = mapper.readerFor(BinanceTrade.class);

			troot.forEach(tnode -> {
				try {
					BinanceTrade btrade = treader.readValue(tnode);

					if (border.orderId.equals(btrade.orderId)) {
						border.trades.add(btrade);
					}

				} catch (IOException e) {
					// TODO Auto-generated catch block
					log.error(e);
				}
			});
		}
	}

	protected void tick() {
		if (in_shutdown) return;

		client.updateTimeDiff();

		if (!initiated) {
			if (!init_started) {
				init_started = true;
				initMarketplace();
			}
			return;
		}
		if (!USE_WEBSOCKET) {
			serviceMarketplace();
		} else {
			// check user data web-socket
			checkUserDataWebsocket();
			// check web-sockets
			synchronized (websockets) {
				for (DirectionPair pair : marketsManager.marketsRequired) {
					WebSocket ws = websockets.get(pair);
					if (ws == null) {
						websockets.put(pair, initWebsocketMarket(pair));
					} else if (!Arrays.asList(WebSocketState.CREATED, WebSocketState.CONNECTING, WebSocketState.OPEN).contains(ws.getState())) {
						System.err.println(String.format("Websocket for %s in %s state. Restarting...", pair, ws.getState()));
						ws.disconnect();
						websockets.remove(pair);
						marketUpdateIds.remove(pair);
						invalidateMarkets(pair);
						ws = null;
						System.gc();
						websockets.put(pair, initWebsocketMarket(pair));
					} else if (messageQueues.get(pair) != null && messageQueues.get(pair).isOverloaded()) {
						ws.disconnect();
					} else if (
							(  marketsManager.markets.get(pair.ask).offers.size() < MIN_OFFERS_SIZE
									|| marketsManager.markets.get(pair.bid).offers.size() < MIN_OFFERS_SIZE
							) && marketUpdateIds.get(pair) != null
							) {
						if (ws.isOpen()) ws.disconnect();
					}
				}
			}
		}

//		if (lastTestTime == 0) {
//			lastTestTime = System.currentTimeMillis();
//		} else if (lastTestTime + 60 * 1000 < System.currentTimeMillis()) {
//	    	DirectionPair pair = marketplacesManager.getDirectionPair("BTC", "USDT");
//	    	Direction direction = marketplacesManager.getDirection(pair, false);
//	    	marketplace.takeWithDelay(new RequestListener() {
//	    		@Override
//	            protected void listenAsync(Event event) {
//	    			//ResultDataTake res = (ResultDataTake) event;
//	                //log.trace("takeWithDelay result received");// + ((ResultDataTake) event).getDescription());
//	            }
//
//	    	}, direction, new BigDecimal("0.001"), new BigDecimal("5000"), (long) 5000);
//
//	    	lastTestTime = System.currentTimeMillis();
//		}
	}
	private void checkUserDataWebsocket() {
//		if (USER_SOCKET_DISCONNECT_TEST && userDataWS != null) {
//			if (userDataWSLastPing + 5000 < System.currentTimeMillis())	userDataWS.disconnect();
//		}
		if (userDataWS == null) {
			WebSocketFactory factory = new WebSocketFactory().setConnectionTimeout(5000);
			String uri = getUserDataWSUri();
			if (uri == null) return;
			try {
				userDataWS = factory.createSocket(uri);
				userDataWS.addListener(new WebSocketAdapter() {
					@Override
					public void onTextMessage(WebSocket websocket, String message) throws Exception {
						log.trace("Received: " + message);
						processWebsocketAccountMessage(websocket, message);
					}
					@Override
					public void onConnected(WebSocket websocket, Map<String, List<String>> headers) {
						log.trace(String.format("Websocket for %s connected", websocket.getURI()));
//				    	DirectionPair pair = marketplacesManager.getDirectionPair("BTC", "USDT");
//				    	Direction direction = marketplacesManager.getDirection(pair, true);
//				    	marketplace.takeWithDelay(new RequestListener() {
//				    		@Override
//				            protected void listenAsync(Event event) {
//				    			//ResultDataTake res = (ResultDataTake) event;
//				                log.trace("takeWithDelay result received");// + ((ResultDataTake) event).getDescription());
//				            }
//
//				    	}, direction, new BigDecimal("0.001"), new BigDecimal("9880"), (long) 5000);
					}
					@Override
					public void onConnectError(WebSocket websocket, WebSocketException exception) {
						log.trace(String.format("connected error for %s, messgage: %s", websocket.getURI(), exception.getMessage()));
					}
					@Override
					public void onDisconnected(WebSocket websocket,
											   WebSocketFrame serverCloseFrame, WebSocketFrame clientCloseFrame,
											   boolean closedByServer) throws Exception
					{
						log.trace("Disconnected: " + websocket.getURI());
					}

				});
				userDataWS.connectAsynchronously();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		} else if (!Arrays.asList(WebSocketState.CREATED, WebSocketState.CONNECTING, WebSocketState.OPEN).contains(userDataWS.getState())) {
			userDataWS.disconnect();
			userDataWS = null;
		} else if (System.currentTimeMillis() - userDataWSLastPing > userDataWSPingTime) {
			Map<String, String> params = new HashMap<>();
			params.put("listenKey", userDataStreamUri.replace(WEB_SOCKET_BASE_URL, ""));
			JsonNode result = client.queryJson(ClientBinance.Action.STREAM_PING, params);

			if (result != null) {
				userDataWSLastPing = System.currentTimeMillis();
			}
			// reconnect socket after ping
			userDataWS.disconnect();
			userDataWS = null;

		}
	}

	private void processWebsocketAccountMessage(WebSocket ws, String msg) {
		try {
			JsonNode node = mapper.readTree(msg);
			switch (node.get("e").asText()) {
				case "outboundAccountInfo":
					processAccountUpdate(node);
					break;
				case "executionReport":
					processExecutionUpdate(node);
					break;
				default:
					System.err.println("Unknown event received for AccountMessage:" + msg);
					break;
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			System.err.println("Wrong json received for AccountMessage:" + msg);
		}

	}

	private void processExecutionUpdate(JsonNode node) {
		switch (node.get("x").asText()) {
			case "TRADE":
				updateTrade(node);
				break;
			default:
				updateActiveOrders(getOrderFromExecutionReport(node));
				break;

		}
		log.trace(node);
	}

	private void updateTrade(JsonNode node) {
		// get order id
		String order_id = node.get("i").asText();
		// create trade
		BinanceTrade bt 	= new BinanceTrade();
		bt.id 				= node.get("t").asText();
		bt.pair 			= node.get("s").asText();
		bt.type 			= node.get("o").asText();
		bt.side 			= node.get("S").asText();
		bt.qty 				= node.get("l").asText();
		bt.price 			= node.get("L").asText();
		bt.accAmount		= node.get("z").asText();
		bt.orderId 			= order_id;
		bt.clientOrderId	= node.get("c").asLong();
		bt.time				= node.get("T").asLong();
		bt.status			= node.get("X").asText();
		bt.commission		= node.get("n").asText();
		bt.commissionAsset  = node.get("N").asText();
		// get direction pair

		syncToMarketOrder(bt);

	}

	private void syncToMarketOrder(BinanceTrade bt) {
		Direction 	direction 	= getDirectionBySymbolAndSide(bt.pair, bt.side);
		Order 		order 		= marketplace.getOrder(direction, bt.orderId);
		// if order not exists - create it
		if (order == null) {
			log.error("Received trade for non existing order");
			return;
		}
		log.trace("Adding market trade");
		marketplace.addTradeToOrder(
				direction,
				bt.orderId,
				bt.id,
				bt.qty,
				bt.price,
				bt.commission,
				bt.commissionAsset,
				bt.time
		);
		log.trace("Sync order status to " + bt.status);
		syncOrderStatus(bt.status, order);


	}

	private void updateActiveOrders(BinanceOrder bo) {
		syncToMarketOrder(bo);
	}

	private BinanceOrder getOrderFromExecutionReport(JsonNode node) {
		BinanceOrder bo = new BinanceOrder();

		bo.orderId 		 = node.get("i").asText();
		bo.clientOrderId = node.get("c").asText();
		bo.origQty 		 = node.get("q").asText();
		bo.executedQty   = node.get("z").asText();
		bo.price 		 = node.get("p").asText();
		bo.side 		 = node.get("S").asText();
		bo.symbol 		 = node.get("s").asText();
		bo.status 		 = node.get("X").asText();
		bo.time 		 = node.get("T").asLong();
		bo.timeInForce 	 = node.get("f").asText();
		bo.type 		 = node.get("o").asText();

		return bo;
	}

	private DirectionPair getDirectionPairBySymbol(String symbol) {
		return name2pairs.get(symbol);
	}

	private void processAccountUpdate(JsonNode node) {
		JsonNode balances = node.get("B");
		if (balances != null) {
			processBalancesUpdateSmall(balances);
		}
	}

	private String getUserDataWSUri() {
		if (userDataStreamUri  == null) {
			JsonNode node = client.queryJson(ClientBinance.Action.STREAM_CREATE);
			if (!isApiError(node, "getUserDataWSUri")) {
				if (node.get("listenKey") != null) {
					userDataStreamUri = WEB_SOCKET_BASE_URL + node.get("listenKey").asText();
					userDataWSLastPing = System.currentTimeMillis();
				}
			};
		}
		return userDataStreamUri;
	}

	private WebSocket initWebsocketMarket(DirectionPair pair) {
		WebSocketFactory factory = new WebSocketFactory().setConnectionTimeout(5000);

		if (markets == null || markets.get(pair) == null) return null;

		String url = String.format(WEB_SOCKET_BASE_URL + "%s@depth",
				markets.get(pair).symbol.toLowerCase());

		WebSocket ws = null;
		try {
			ws = factory.createSocket(url);
			ws.addListener(new WebSocketAdapter() {
				@Override
				public void onTextMessage(WebSocket websocket, String message) throws Exception {
					//log.trace(System.currentTimeMillis() + " Received: " + message);
					processWebsocketDepthMessage(pair, websocket, message);
				}
				@Override
				public void onConnected(WebSocket websocket, Map<String, List<String>> headers) {
					System.out.println(String.format("Websocket for %s connected", websocket.getURI()));
					initMarketForUpdates(pair);
				}
				@Override
				public void onConnectError(WebSocket websocket, WebSocketException exception) {
					System.out.println(String.format("connected error for %s, messgage: %s", websocket.getURI(), exception.getMessage()));
				}
				@Override
				public void onDisconnected(WebSocket websocket,
										   WebSocketFrame serverCloseFrame, WebSocketFrame clientCloseFrame,
										   boolean closedByServer) throws Exception
				{
					log.trace("Disconnected: " + websocket.getURI());
				}

			});
			ws.connectAsynchronously();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return ws;
	}

	private void initMarketForUpdates(DirectionPair pair) {
		serviceOffers(pair);
	}

	private void processWebsocketDepthMessage(DirectionPair pair, WebSocket websocket, String message) {
		if (messageQueues.get(pair) == null) {
			messageQueues.put(pair, new MessageQueue(pair));
		}
		messageQueues.get(pair).put(message);
		messageQueues.get(pair).process();
	}

	protected void initMarkets() {
		System.out.println(client);
		JsonNode root = client.queryJson(ClientBinance.Action.PRODUCTS);
		markets = new HashMap<DirectionPair, BinanceMarket>();
		name2pairs = new HashMap<String, DirectionPair>();

		if (isApiError(root, "Binance InitMarkets ")) {
			return;
		}

		ObjectReader reader = (new ObjectMapper()).readerFor(BinanceMarket.class);

		// System.out.println(root.get("pairs"));
		for (Iterator<JsonNode> it = root.get("data").iterator(); it.hasNext();) {
			JsonNode node = it.next();

			try {
				BinanceMarket bm = null;
				// System.out.println(en.getValue());
				bm = reader.readValue(node);

				DirectionPair pair = getDirectionPair(bm);

				if (pair == null)
					continue;

				markets.put(pair, bm);
				name2pairs.put(bm.symbol, pair);
				marketsUpdated.put(pair, new Long(0));

			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return;
	}

	protected DirectionPair getDirectionPair(BinanceMarket bm) {
		String[] currs = { bm.baseAsset, bm.quoteAsset };

		if (currs.length != 2)
			return null;

		marketplace.addLocal(marketplacesManager.getCurrency(currs[0]), bm.decimalPlaces);
		marketplace.addLocal(marketplacesManager.getCurrency(currs[1]), bm.decimalPlaces);
		DirectionPair directionPair = marketplacesManager.getDirectionPair(currs[0], currs[1]);
		marketplace.addLocal(marketplacesManager.getDirection(directionPair, true ));
		marketplace.addLocal(marketplacesManager.getDirection(directionPair, false));
		BigDecimal tickSize = new BigDecimal(bm.tickSize);
		BigDecimal minTrade = new BigDecimal(bm.minTrade);
		BigDecimal minTotal = DirectionPairLocal.AMOUNT_INVALID;
		switch (bm.quoteAsset) {
			case "BTC":
				minTotal = BigDecimal.valueOf(0.001);
				break;
			case "ETH":
				minTotal = BigDecimal.valueOf(0.01);
				break;
			case "USDT":
				minTotal = BigDecimal.valueOf(1);
				break;
		}
		marketplace.addLocal(directionPair, TRADE_FEE, TRADE_FEE,
				true, -(minTrade.precision() - minTrade.scale() - 1),
				true, -(tickSize.precision() - tickSize.scale() - 1),
				new BigDecimal(bm.minTrade), DirectionPairLocal.AMOUNT_INVALID,
				minTotal					, DirectionPairLocal.AMOUNT_INVALID);

		return directionPair;

	}

	protected void serviceMarketplace() {
		for (DirectionPair dp : marketsManager.marketsRequired) {
			if (System.currentTimeMillis() - marketsUpdated.get(dp) > MARKET_UPDATE_TIME) {
				serviceOffers(dp);
			}
		}

	}

	/**
	 * Service offers identified by pair, return true or false
	 *
	 * @param pair
	 * @return
	 */
	public Boolean serviceOffers(DirectionPair pair) {
		// load from
		JsonNode root = null;

		String marketName = markets.get(pair).symbol;

		if (marketName == null)
			return false;

		Map<String, String> params = new HashMap<String, String>();
		params.put("symbol", marketName);
		if (!USE_WEBSOCKET)	params.put("limit", String.format("%d", MAX_OFFERS));

		if (params.size() > 0) {
			System.out.println(params);
		}

		root = client.queryJson(ClientBinance.Action.DEPTH, params);
		if (isApiError(root, "Binance ServiceOffers " + pair)) {
			return false;
		}
		//System.out.println(root);
		processOffers(root.get("bids"), pair.bid);
		processOffers(root.get("asks"), pair.ask);

		Long updateId = root.get("lastUpdateId").asLong();

		marketsUpdated.put(pair, System.currentTimeMillis());
		marketUpdateIds.put(pair,  updateId);

		return true;

	}


	/**
	 * Process full order book update
	 * @param nodes
	 * @param dir
	 */
	public void processOffers(JsonNode nodes, Direction dir) {

		List<Offer> offers = new ArrayList<>();

		DirectionPairLocal directionPairLocal = marketplace.getLocal(dir.directionPair);

		for (JsonNode node : nodes) {
			offers.add(new Offer(marketplace, dir,
					directionPairLocal.scaleOpBase.set(new BigDecimal(node.get(1).asDouble())),
					directionPairLocal.scaleOpRate.set(new BigDecimal(node.get(0).asDouble()))));
		}

		updateMarketData(dir, offers);
	}

	@Override
	protected Marketplace.ResultDataMake make(Marketplace.RequestDataMake requestDataMake) {
		Order order = placeOrder(requestDataMake.direction, requestDataMake.amount, requestDataMake.rate, BinanceTimeInForce.GTC);
		if (order == null) {
			// Todo: specify reasons why the offer couldn't be created.
			return Marketplace.ResultDataMake.FAILURE;
		}

		return new Marketplace.ResultDataMake(Marketplace.ResultDataMake.Result.SUCCESS, order);
	}

	@Override
	protected Marketplace.ResultDataTake take(Marketplace.RequestDataTake requestDataTake) {
		long timeStart = System.currentTimeMillis();

		Order order = placeOrder(requestDataTake.direction, requestDataTake.amount, requestDataTake.rate, BinanceTimeInForce.IOC);

		order.waitForFinish();

		if (order != null) waitForPursesUpdatedAfter(timeStart, 2000);

		ResultDataSummary summary = summaryFromOrder(requestDataTake.direction, order);

		if (summary.amountBase.signum() != 0) {
			return new Marketplace.ResultDataTake(Marketplace.ResultDataTake.Result.SUCCESS, summary.amountBase, summary.amountQuote);
		}

		return Marketplace.ResultDataTake.FAILURE;
	}


	/**
	 * Place order on market by direction
	 * @param direction
	 * @param amount
	 * @param rate
	 * @return
	 */
	public Order placeOrder(Direction direction, BigDecimal amount, BigDecimal rate, BinanceTimeInForce tif) {
		Order order = null;

		Map<String, String> params = new HashMap<>();

		String m = getMarket(direction.directionPair);

		if (m == null) {
			System.err.println("No market found for direction " + direction);
			return order;
		}
		// fill market name
		params.put("symbol", m.toUpperCase());
		if (tif == BinanceTimeInForce.IOC) {
			params.put("newOrderRespType", "FULL");
		}

		params.put("side", direction.ask ? OrderSide.SELL.value : OrderSide.BUY.value);
		params.put("type", OrderType.LIMIT.value);
		// GTC - good until cancel, IOC - Immediate or Cancel
		params.put("timeInForce", tif.value);
		params.put("quantity", amount.toPlainString());
		params.put("price", rate.toPlainString());
		JsonNode res = null;
		// System.out.println(params);
		if (USE_TEST_ORDERS) {
			res = client.queryJson(ClientBinance.Action.ORDER_NEW_TEST, params);
		} else {
			res = client.queryJson(ClientBinance.Action.ORDER_NEW, params);
		}
		if (isApiError(res, String.format("Binance placeOrder symbol: %s, amount: %s, rate: %s", m.toUpperCase(), amount.toPlainString(), rate.toPlainString()))) {
			return order;
		}

		if (res.get("orderId") != null) {

			order = createMarketOrder(direction, res.get("orderId").asText(), amount, rate, getMarketTimeInForce(tif.value));

			if (res.get("fills") != null) {

				int tradeNum = 1;
				for (JsonNode n : res.get("fills")) {
					if (n.get("qty") == null || n.get("price") == null) continue;

					BigDecimal qty   		= new BigDecimal(n.get("qty"       ).asText());
					BigDecimal price 		= new BigDecimal(n.get("price"     ).asText());
					BigDecimal fee   		= new BigDecimal(n.get("commission").asText());

					Currency   feeCurrency 	= marketplacesManager.getCurrency(n.get("commissionAsset").asText());

					String tradeID = order.id + "_" + tradeNum++;

					marketplace.addTradeToOrder(direction, order.id, tradeID, qty, price, fee, feeCurrency, res.get("transactTime").asLong());

				}

			}
		}
		return order;
	}

	private TimeInForce getMarketTimeInForce(String value) {
		TimeInForce result = TimeInForce.GTC;
		switch (value) {
			case "IOC":
				result = TimeInForce.IOC;
				break;
		}
		return result;
	}

	/**
	 * update activeOrders and activeOrdersById record for given symbol and orderId
	 * @param symbol
	 * @param orderId
	 */
	public BinanceOrder queryOrderApi(String symbol, Long orderId) {
		Map<String, String> params = new HashMap<>();

		params.put("symbol", symbol);
		params.put("orderId", String.valueOf(orderId));

		JsonNode root = client.queryJson(ClientBinance.Action.ORDER_INFO, params);

		if (isApiError(root, String.format("Binance queryOrderApi symbol: %s, orderId: %s", symbol, orderId))) {
			return null;
		}

		ObjectReader reader = mapper.readerFor(BinanceOrder.class);
		BinanceOrder order = null;

		if (root != null) {
			try {
				order = reader.readValue(root);

				syncToMarketOrder(order);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return order;
	}

	@Override
	protected Marketplace.ResultDataCancel cancel(Marketplace.RequestDataCancel requestDataCancel) {
		String symbol = getMarket(requestDataCancel.direction.directionPair);

		Marketplace.ResultDataCancel result = Marketplace.ResultDataCancel.FAILURE;
		if (cancelOrderApi(symbol, requestDataCancel.order.id)) {
			Order order = marketplace.getOrder(requestDataCancel.direction, requestDataCancel.order.id);
			order.setStatus(Order.Status.CANCELLED);
			result = Marketplace.ResultDataCancel.SUCCESS;
		}

		return result;
	}

	/**
	 * Cancel order on market use symbol and orderId
	 *
	 * @param symbol
	 * @param orderId
	 * @return
	 */
	public Boolean cancelOrderApi(String symbol, String orderId) {
		JsonNode res;
		Map<String, String> params = new HashMap<>();
		params.put("symbol", symbol);
		params.put("orderId", orderId);

		res = client.queryJson(ClientBinance.Action.ORDER_CANCEL, params);

		if (!isApiError(res, String.format("Binance cancelOrder %s:%s", symbol, orderId))) {
			return true;
		}
		return false;
	}

	public void loadAccountInfo() {
		log.trace("Load account info");
		boolean account_loaded = false;
		while (!account_loaded) {
			JsonNode root = client.queryJson(ClientBinance.Action.ACCOUNT);

			if (isApiError(root, "Binance loadAccountInfo")) {
				try {
					client.updateTimeDiff();
					Thread.sleep(500);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					//e.printStackTrace();
				}
				continue;
			}

			if (accountInformation == null) {
				accountInformation = new BinanceAccountInformation();
				accountInformation.balances = new HashMap<>();
			}

			accountInformation.makerCommission 	= new BigDecimal(root.get("makerCommission") .asText());
			accountInformation.takerCommission	= new BigDecimal(root.get("takerCommission") .asText());
			accountInformation.buyerCommission 	= new BigDecimal(root.get("buyerCommission") .asText());
			accountInformation.sellerCommission = new BigDecimal(root.get("sellerCommission").asText());

			accountInformation.canTrade 	= root.get("canTrade")	 .asBoolean();
			accountInformation.canWithdraw 	= root.get("canWithdraw").asBoolean();
			accountInformation.canDeposit 	= root.get("canDeposit") .asBoolean();

			processBalancesUpdate(root.get("balances"));
			account_loaded = true;
		}
		return;
	}
	private void processBalancesUpdate(JsonNode balances) {
		ObjectReader reader = mapper.readerFor(BinanceBalance.class);
		BinanceBalance b = null;
		Currency c = null;

		for (Iterator<JsonNode> it = balances.elements(); it.hasNext();) {
			JsonNode balance = it.next();
			// get balance currency
			try {
				b = reader.readValue(balance);
				if (b == null) continue;
				c = marketplacesManager.getCurrency(b.asset);
				CurrencyLocal currencyLocal = marketplace.getLocal(c);
				if (currencyLocal != null) {
					b.free = currencyLocal.scaleOp.set(b.free);
					b.locked = currencyLocal.scaleOp.set(b.locked);

					marketplace.purses.set(c, b.free);
				};
				accountInformation.balances.put(b.asset, b);


			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		updatePursesLastUpdated();
	}

	private void processBalancesUpdateSmall(JsonNode balances) {
		ObjectReader reader = mapper.readerFor(BinanceBalanceSmall.class);
		BinanceBalance 		b;
		BinanceBalanceSmall bs = null;
		Currency c = null;

		for (Iterator<JsonNode> it = balances.elements(); it.hasNext();) {
			JsonNode balance = it.next();
			// get balance currency
			try {
				bs = reader.readValue(balance);
				if (bs == null) continue;
				b = new BinanceBalance();
				bs.fillNormal(b);
				c = marketplacesManager.getCurrency(b.asset);
				CurrencyLocal currencyLocal = marketplace.getLocal(c);
				if (currencyLocal != null) {
					b.free = currencyLocal.scaleOp.set(b.free);
					b.locked = currencyLocal.scaleOp.set(b.locked);

					marketplace.purses.set(c, b.free);
				};
				accountInformation.balances.put(b.asset, b);


			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		updatePursesLastUpdated();
	}

	public void loadOpenOrders(DirectionPair pair) {
		BinanceMarket m = markets.get(pair);
		if (m == null) return;
		loadOpenOrdersApi(m.symbol);
	}

	public void loadOpenOrdersApi(String symbol) {
		JsonNode res;
		Map<String, String> params = new HashMap<>();
		params.put("symbol", symbol);

		res = client.queryJson(ClientBinance.Action.ORDERS_OPEN, params);

		if (!isApiError(res, "LoadOpenOrdersApi " + symbol)) {
			return;
		}
		BinanceOrder order = null;
		ObjectReader reader = mapper.readerFor(BinanceOrder.class);
		for (Iterator<JsonNode> it = res.elements(); it.hasNext();) {
			JsonNode node = it.next();
			try {
				order = reader.readValue(node);
				syncToMarketOrder(order);

			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		return;

	}

	/**
	 * Get market Symbol based on DirectionPair
	 * @param pair
	 * @return
	 */
	public String getMarket(DirectionPair pair) {
		return (markets.get(pair) != null) ? markets.get(pair).symbol : null;
	}

	private Boolean isApiError(JsonNode node, String where) {
		if (node == null) {
			System.err.println(where);
			System.err.println("Json is null");
			return true;
		}
		if (node.get("code") != null) {
			System.err.println(where);
			System.err.println(String.format("Api Error: %s: %s", node.get("code").asText(), node.get("msg").asText()));
			return true;
		}
		return false;
	}
	public Direction getDirectionBySymbolAndSide(String symbol, String side) {
		DirectionPair pair = getDirectionPairBySymbol(symbol);

		if (pair == null) return null;

		return side.equals(OrderSide.BUY.value) ? pair.bid : pair.ask;
	}

	@Override
	protected void updateMarketOrder(Direction direction, Order order, boolean beforeSummary) {
		// TODO Auto-generated method stub
		// no need to update, updates async over web-socket

		// update order only if not finished
		if (beforeSummary && !order.isFinished()) {
			log.trace("Query order with API");
			queryOrderApi(direction, order.id);
		}
	}
	private void queryOrderApi(Direction direction, String orderID) {
		// TODO Auto-generated method stub
		BinanceMarket bm = markets.get(direction.directionPair);

		BinanceOrder border = queryOrderApi(bm.symbol, Long.valueOf(orderID));

		// load trades if there any
		if ((new BigDecimal(border.executedQty)).signum() != 0) loadOrderTrades(bm, border);

		syncToMarketOrder(border);
	}

	/*
     * Sync internal order to Market Order
     */
	private Order syncToMarketOrder(BinanceOrder border) {
		Direction 	direction 	= getDirectionBySymbolAndSide(border.symbol, border.side);
		Order 		order 		= marketplace.getOrder(direction, border.orderId);
		// if order not exists - create it
		if (order == null) {

			BigDecimal amount			= new BigDecimal(border.origQty);
			BigDecimal rate				= new BigDecimal(border.price);

			order = createMarketOrder(direction, border.orderId, amount, rate, getMarketTimeInForce(border.timeInForce), border.time);
		}

		syncOrderStatus(border.status, order);

		for (BinanceTrade btrade : border.trades) {
			marketplace.addTradeToOrder(
					direction,
					border.orderId,
					btrade.id,
					btrade.qty,
					btrade.price,
					btrade.commission,
					btrade.commissionAsset,
					btrade.time
			);
		}
		return order;
	}

	private static void syncOrderStatus(String status, Order o) {
		// just modify status
		// select right status
		switch (status) {
			case "NEW":
				o.setStatus(Order.Status.CREATED);
				break;
			case "PARTIALLY_FILLED":
				o.setStatus(Order.Status.PARTIALLY_FILLED);
				break;
			case "FILLED":
				o.setStatus(Order.Status.FILLED);
				break;
			case "CANCELED":
				o.setStatus(Order.Status.CANCELLED);
				break;
			case "PENDING_CANCEL":
				o.setStatus(Order.Status.CANCELLING);
				break;
			case "REJECTED":
				o.setStatus(Order.Status.REJECTED);
				break;
			case "EXPIRED":
				o.setStatus(Order.Status.EXPIRED);
				break;
		}
	}

	@Override
	protected void updatePurses(boolean force) {
		if (force) {
			updatePursesApi();
		}

	}

	private void updatePursesApi() {
		// balances under account information
		loadAccountInfo();

	}


}
