package Model.Marketplace;

import Application.Events.EventDispatcher;
import Application.Events.EventListener;
import Application.Preferences.LocalSettings;
import Application.Preferences.PreferencesApp;
import Model.Marketplace.Base.Currency.Currency;
import Model.Marketplace.Base.Currency.Direction;
import Model.Marketplace.Base.Currency.DirectionPair;
import Model.Marketplace.Base.Currency.RatePolicyBaseToQuote;
import Model.Marketplace.Base.Marketplace;
import Model.Marketplace.Binance.ConnectorBinance;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.math.BigDecimal;
import java.util.*;

// Test

public class MarketplacesManager {

    protected static Logger log = LogManager.getLogger(MarketplacesManager.class.getName());

    private LocalSettings localSettings;
    private PreferencesApp preferencesApp;

    private EventDispatcher eventDispatcher;
    public Map<Marketplace.Type, Marketplace> marketplaces;

    public Marketplace viewedMarketplace;

    public List<Currency>      currencies;
    public List<Direction>     directions;
    public List<DirectionPair> directionPairs;

    private int commID;

    private int traceID;

    private static class TimeNotch {
        private long millis;
        private String reason;

        private TimeNotch(long millis, String reason) {
            this.millis = millis;
            this.reason = reason;
        }
    }

    private Map<Integer, List<TimeNotch>> timeLines;

    //////////////////////////////////////////////////
    // Constructor.

    public MarketplacesManager(LocalSettings localSettings, PreferencesApp preferencesApp) {

        this.localSettings = localSettings;
        this.preferencesApp = preferencesApp;

        eventDispatcher = new EventDispatcher();
        marketplaces = new HashMap<>();

        currencies     = new ArrayList<>();
        directions     = new ArrayList<>();
        directionPairs = new ArrayList<>();

        commID = 0;
        traceID = 0;

        timeLines = new HashMap<>();
    }

    public void done() {
        for (Marketplace marketplace : marketplaces.values()) {
            marketplace.done();
        }
    }

    public void test() {
//        Marketplace marketplaceBIT = new Marketplace(this, Marketplace.Type.BITTREX, preferencesApp);
//        marketplaceBIT.init(new RatePolicyBaseToQuote(), null);
//        marketplaces.put(Marketplace.Type.BITTREX, marketplaceBIT);
//
//        Marketplace marketplaceLIQ = new Marketplace(this, Marketplace.Type.LIQUI, preferencesApp);
//        marketplaceLIQ.init(new RatePolicyBaseToQuote(), null);
//        marketplaces.put(Marketplace.Type.LIQUI, marketplaceLIQ);

        Marketplace marketplaceBIN = new Marketplace(this, Marketplace.Type.BINANCE, preferencesApp);
        marketplaceBIN.init(new RatePolicyBaseToQuote(), null);
        marketplaces.put(Marketplace.Type.BINANCE, marketplaceBIN);

        Set<DirectionPair> marketsRequired = new HashSet<>();
        marketsRequired.add(getDirectionPair("BTC", "SNM"));

//        marketplaceBIT.marketsManager.requireMarkets(marketsRequired);
//        marketplaceLIQ.marketsManager.requireMarkets(marketsRequired);
        marketplaceBIN.marketsManager.requireMarkets(marketsRequired);

        Currency currencyBTC = getCurrency("BTC");
        Currency currencySNM = getCurrency("SNM");
        DirectionPair directionPairSNM_BTC = getDirectionPair(currencySNM, currencyBTC);
        Direction directionSNM_BTC = getDirection(directionPairSNM_BTC, true );
        Direction directionBTC_SNM = getDirection(directionPairSNM_BTC, false);

//        marketplaceBIT.addLocal(currencyBTC, 8);
//        marketplaceBIT.addLocal(currencySNM, 8);
//        marketplaceBIT.addLocal(directionSNM_BTC);
//        marketplaceBIT.addLocal(directionBTC_SNM);
//        marketplaceBIT.addLocal(directionPairSNM_BTC, new BigDecimal("0.001"), new BigDecimal("0.001"));
//        CurrencyLocal currencyLocalBTT_BTC = marketplaceBIT.getLocal(currencyBTC);
//        CurrencyLocal currencyLocalBTT_SNM = marketplaceBIT.getLocal(currencySNM);
//        marketplaceBIT.purses.add(currencyBTC, new BigDecimal("1.5"));
//        marketplaceBIT.purses.add(currencySNM, new BigDecimal("5000"));
//
//        marketplaceLIQ.addLocal(currencyBTC, 8);
//        marketplaceLIQ.addLocal(currencySNM, 8);
//        marketplaceLIQ.addLocal(directionSNM_BTC, DirectionLocal.Mode.BID);
//        marketplaceLIQ.addLocal(directionBTC_SNM, DirectionLocal.Mode.ASK);
//        marketplaceLIQ.addLocal(directionPairSNM_BTC, new BigDecimal("0.001"), new BigDecimal("0.001"));
//        CurrencyLocal currencyLocalLIQ_BTC = marketplaceLIQ.getLocal(currencyBTC);
//        CurrencyLocal currencyLocalLIQ_SNM = marketplaceLIQ.getLocal(currencySNM);
//        marketplaceLIQ.purses.add(currencyBTC, new BigDecimal("5"));
//        marketplaceLIQ.purses.add(currencySNM, new BigDecimal("5000"));
//
//        marketplaceBIN.addLocal(currencyBTC, 8);
//        marketplaceBIN.addLocal(currencySNM, 8);
//        marketplaceBIN.addLocal(directionSNM_BTC, DirectionLocal.Mode.BID);
//        marketplaceBIN.addLocal(directionBTC_SNM, DirectionLocal.Mode.ASK);
//        marketplaceBIN.addLocal(directionPairSNM_BTC, new BigDecimal("0.001"), new BigDecimal("0.001"));
//        CurrencyLocal currencyLocalBIN_BTC = marketplaceBIN.getLocal(currencyBTC);
//        CurrencyLocal currencyLocalBIN_SNM = marketplaceBIN.getLocal(currencySNM);
//        marketplaceBIN.purses.add(currencyBTC, new BigDecimal("1.5"));
//        marketplaceBIN.purses.add(currencySNM, new BigDecimal("5000"));
//
//        Market marketBTT_SNM_BTC = marketplaceBIT.marketsManager.markets.get(directionSNM_BTC);
//        marketBTT_SNM_BTC.offers.add(new Offer(directionSNM_BTC, currencyLocalBTT_SNM, currencyLocalBTT_BTC, new BigDecimal("3.0"), new BigDecimal("1006")));
//        marketBTT_SNM_BTC.offers.add(new Offer(directionSNM_BTC, currencyLocalBTT_SNM, currencyLocalBTT_BTC, new BigDecimal("2.0"), new BigDecimal("1005")));
//        marketBTT_SNM_BTC.offers.add(new Offer(directionSNM_BTC, currencyLocalBTT_SNM, currencyLocalBTT_BTC, new BigDecimal("1.0"), new BigDecimal("1001")));
//        marketBTT_SNM_BTC.offers.add(new Offer(directionSNM_BTC, currencyLocalBTT_SNM, currencyLocalBTT_BTC, new BigDecimal("1.0"), new BigDecimal("1000")));
//
//        Market marketBIN_BTC_SNM = marketplaceBIN.marketsManager.markets.get(directionBTC_SNM);
//        marketBIN_BTC_SNM.offers.add(new Offer(directionBTC_SNM, currencyLocalBIN_BTC, currencyLocalBIN_SNM, new BigDecimal("1.0"), new BigDecimal("1000")));
//        marketBIN_BTC_SNM.offers.add(new Offer(directionBTC_SNM, currencyLocalBIN_BTC, currencyLocalBIN_SNM, new BigDecimal("1.0"), new BigDecimal("1001")));
//        marketBIN_BTC_SNM.offers.add(new Offer(directionBTC_SNM, currencyLocalBIN_BTC, currencyLocalBIN_SNM, new BigDecimal("0.5"), new BigDecimal("1005")));
//        marketBIN_BTC_SNM.offers.add(new Offer(directionBTC_SNM, currencyLocalBIN_BTC, currencyLocalBIN_SNM, new BigDecimal("1.5"), new BigDecimal("1008")));

//        broadcast(new Connector.EventMarketChanged(Marketplace.Type.BITTREX, directionSNM_BTC));
    }

    //////////////////////////////////////////////////
    // Event dispatcher.

    public void addListener(EventListener eventListener) { eventDispatcher.addListener(eventListener); }

    public void remListener(EventListener eventListener) { eventDispatcher.remListener(eventListener); }

    public void broadcast(EventListener.Event event) { eventDispatcher.broadcast(event); }

    //////////////////////////////////////////////////
    // Connector event chain.

    public synchronized int getCommID() {
        return commID++;
    }

//    public int getTraceID() {
//        return traceID++;
//    }
//
//    public void setTimeNotch(int traceID, String reason) {
//        List<TimeNotch> timeNotches = timeLines.get(traceID);
//        if (timeNotches == null) {
//            timeNotches = new ArrayList<>();
//            timeLines.put(traceID, timeNotches);
//        }
//        timeNotches.add(new TimeNotch(System.currentTimeMillis(), reason));
//    }
//
//    public void printTimeLine(int traceID) {
//        List<TimeNotch> timeNotches = timeLines.remove(traceID);
//        if (timeNotches != null) {
//            for (TimeNotch timeNotch : timeNotches) {
//                log.trace(timeNotch.millis + " " + timeNotch.reason);
//            }
//        }
//    }
//
//    public void removeTimeLine(int traceID) {
//        timeLines.remove(traceID);
//    }

    //////////////////////////////////////////////////
    // Core functionality.

    public Marketplace createMarketplace(Marketplace.Type marketplaceType) {
        Marketplace marketplace = null;
        switch (marketplaceType) {
            case BINANCE:
                marketplace = new Marketplace(this, Marketplace.Type.BINANCE, preferencesApp);
                marketplace.init(new RatePolicyBaseToQuote(), new ConnectorBinance(this, marketplace, localSettings.getProviderAuth(marketplaceType.providerName)));
                break;
        }
        viewedMarketplace = marketplace;
        marketplaces.put(marketplaceType, marketplace);
        return marketplace;
    }

    public Currency getCurrency(String name) {
        synchronized (currencies) {
            for (Currency currencyInList : currencies) {
                if (currencyInList.matches(name)) {
                    return currencyInList;
                }
            }

            Currency currency = new Currency(name);
            currencies.add(currency);
            return currency;
        }
    }

    public DirectionPair getDirectionPair(String base, String quote) {
        return getDirectionPair(getCurrency(base), getCurrency(quote));
    }

    public DirectionPair getDirectionPair(Currency base, Currency quote) {
        synchronized (directionPairs) {
            for (DirectionPair directionPairInList : directionPairs) {
                if (directionPairInList.matches(base, quote)) {
                    return directionPairInList;
                }
            }

            DirectionPair directionPair = new DirectionPair(base, quote);
            Direction ask = new Direction(directionPair, true);
            Direction bid = new Direction(directionPair, false);
            directionPair.setDirections(ask, bid);
            directions.add(ask);
            directions.add(bid);
            return directionPair;
        }
    }

    public Direction getDirection(DirectionPair directionPair, boolean ask) {
        synchronized (directionPairs) {
            for (Direction directionInList : directions) {
                if (directionInList.matches(directionPair, ask)) {
                    return directionInList;
                }
            }

            // Error.
            return null;
        }
    }

    public Direction getOppositeDirection(Direction direction) {
        return getDirection(direction.directionPair, !direction.ask);
    }

    //////////////////////////////////////////////////
    // Additional properties.

    public BigDecimal getWorth(Currency currency) {
        BigDecimal worth = BigDecimal.ZERO;
        for (Marketplace marketplace : marketplaces.values()) {
            worth = worth.add(marketplace.purses.get0(currency));
        }
        return worth;
    }
}
