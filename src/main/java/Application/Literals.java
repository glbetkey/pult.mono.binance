package Application;

/**
 * Created by Dustangel on 7/6/2017.
 */
public class Literals {

    public static final String logErrorPreferencesInvalidValue = "Invalid %s value.";

    public static final String nameDirectionPair = "%s-%s";
    public static final String nameDirection = "%s %s";
    public static final String nameDirectionAsk  = "Ask";
    public static final String nameDirectionBid  = "Bid";
    public static final String nameDirectionAsk2 = "Sell";
    public static final String nameDirectionBid2 = "Buy";
    public static final String nameOffer = "%s@%s";

    public static final String nameTurnoverKey = "%s %s";

    public static final String infoTelegramArbitrageTitleObserve = "%s arbitrage OBSERVED.";
    public static final String infoTelegramArbitrageTitleSuccess = "Arbitrage SUCCESS %s \u0394%s.";
    public static final String infoTelegramArbitrageTitleFailure = "Arbitrage FAILURE:\n";
    public static final String infoTelegramArbitrageColumnTitleMarketplace   = "ex";
    public static final String infoTelegramArbitrageColumnTitleSuccessRatio  = "made";
    public static final String infoTelegramArbitrageColumnTitleCurrencyDelta = "\u0394%s";

    public static final String infoTelegramLowFunds         = "Low"         + " funds: %s %s %s.\n";
    public static final String infoTelegramSuperfluousFunds = "Superfluous" + " funds: %s %s %s.\n";

    public static final String sessionInfoTitle = "%s:\n";
    public static final String sessionInfoCurrency = "%s\n";
    public static final String sessionInfoCurrencyTurnoverNone = "No turnover.";
    public static final String sessionInfoCurrencyTurnover    =  "%s: -%s(%s%%) +%s(%s%%) d_purse %s(%s%%).\n";
    public static final String sessionInfoCurrencyTurnoverAll = "All: -%s(%s%%) +%s(%s%%) d_purse %s(%s%%).\n";

    public static final String logErrorViewFailedToLoad = "Failed to load %s view.";
    public static final String nameViewCurrencyList = "CurrencyList";
    public static final String nameViewCurrencyEdit = "CurrencyEdit";
    public static final String nameViewPreferences  = "Preferences";

    public static final String logErrorTelegramIncorrectSettings = "Incorrect telegram bot settings.";
    public static final String logInfoTelegramUnauthorizedClient = "Message from unauthorized client %s.";

    public static final String orbitBaseLabel = "> %s <";
}
