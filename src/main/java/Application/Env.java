package Application;

import Application.Preferences.LocalSettings;
import Application.Preferences.PreferencesApp;
import Model.Marketplace.Base.Currency.DirectionPair;
import Model.Marketplace.Base.Marketplace;
import Model.Strategy3.Strategy;
import Model.Marketplace.MarketplacesManager;
import Model.Stats.SessionsManager;
//import Telegram.BotLaunch;
import View.EnvAccess;
import View.MainWindowController;
import com.fasterxml.jackson.core.JsonFactory;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.JsonNodeFactory;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.HashMap;

//import Model.User.Strategy;

//import Model.Strategy.Strategy;

/**
 * Created by Dustangel on 7/18/2017.
 */
public class Env {

    private static Logger log = LogManager.getLogger(Env.class.getName());

    public JsonFactory jsonFactory;
    public ObjectMapper objectMapper;
    public JsonNodeFactory jsonNodeFactory;

    public LocalSettings localSettings;
    public PreferencesApp preferencesApp;

    public MarketplacesManager marketplacesManager;
    public SessionsManager sessionsManager;

    private MainWindowController mainWindowController;

    private HashMap<DirectionPair, Strategy> activeStrategies = new HashMap<>();

//    private BotLaunch telegramBotLaunch;



    void init() {
        EnvAccess.setEnv(this);

        jsonFactory = new JsonFactory();
        objectMapper = new ObjectMapper();
        jsonNodeFactory = new JsonNodeFactory(false);

        localSettings = new LocalSettings(objectMapper);
        localSettings.load();

        preferencesApp = new PreferencesApp(jsonFactory, objectMapper, jsonNodeFactory);
        preferencesApp.setFileName("preferences.json");
        preferencesApp.load();

        marketplacesManager = new MarketplacesManager(localSettings, preferencesApp);
//        marketplacesManager.createMarketplace(Marketplace.Type.BITTREX);
        marketplacesManager.createMarketplace(Marketplace.Type.BINANCE);

        sessionsManager = new SessionsManager(jsonFactory, objectMapper, jsonNodeFactory, marketplacesManager);
        marketplacesManager.addListener(sessionsManager);


//        telegramBotLaunch = new BotLaunch(localSettings, preferencesApp, marketplacesManager, sessionsManager);
//        preferencesApp.addListener(telegramBotLaunch);
//        if (preferencesApp.getTelegram()) {
//            telegramBotLaunch.start();
//        }
//
//        strategy = new Strategy(preferencesApp, marketplacesManager, sessionsManager);//, telegramBotLaunch);
//        strategy.start();

//        MarketplacesManager marketplacesManagerTest = new MarketplacesManager(localSettings, preferencesApp);
//        Strategy strategyTest = new Strategy(preferencesApp, marketplacesManagerTest);
//        strategyTest.start();
//        marketplacesManagerTest.test();
//        strategyTest.stop();
//        marketplacesManagerTest.done();
    }

    public void onMainWindowControllerCreated(MainWindowController mainWindowController) {
        this.mainWindowController = mainWindowController;
//        marketplacesManager.addListener(mainWindowController);
//        sessionsManager.setMainWindowController(mainWindowController);
//        user.mainWindowController = mainWindowController;
    }

    public void startStrategy(Marketplace.Type marketplaceType, DirectionPair directionPair) {
//        Strategy strategyAsk = new Strategy(marketplacesManager,
//                marketplacesManager.marketplaces.get(marketplaceType).marketsManager.markets.get(directionPair.ask),
//                marketplacesManager.marketplaces.get(marketplaceType).connector);
//        Strategy strategyBid = new Strategy(marketplacesManager,
//                marketplacesManager.marketplaces.get(marketplaceType).marketsManager.markets.get(directionPair.bid),
//                marketplacesManager.marketplaces.get(marketplaceType).connector);
        Strategy strategy = new Strategy(marketplacesManager, marketplaceType, directionPair, preferencesApp);
        activeStrategies.put(directionPair,strategy);
        strategy.start();
//        activeStrategies.put(directionPair.ask, strategyAsk);
//        activeStrategies.put(directionPair.bid, strategyBid);

    }

    void done() {
        for (Strategy strategy : activeStrategies.values()) {
            strategy.stop();
        }
        activeStrategies.clear();
        sessionsManager.done();
        marketplacesManager.remListener(sessionsManager);
        marketplacesManager.done();
//        telegramBotLaunch.stop();
    }

    public void stopStrategy(DirectionPair directionPair) {
        Strategy strategy = activeStrategies.get(directionPair);
        strategy.stop();
        activeStrategies.remove(directionPair);
//        Strategy strategyBid = activeStrategies.get(directionPair.bid);

//        strategyAsk.stop();
//        strategyBid.stop();
//        activeStrategies.remove(directionPair.ask);
//        activeStrategies.remove(directionPair.bid);
    }
    public boolean isActiveStrategyPair(DirectionPair dp){
        return activeStrategies.containsKey(dp);
    }
}
