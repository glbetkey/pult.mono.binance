package Application;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class Main extends Application {

    private Env env;

    @Override
    public void start(Stage primaryStage) throws Exception {
        env = new Env();
        env.init();

        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/View/PlaceholderView.fxml"));
        Parent root = fxmlLoader.load();

        primaryStage.setTitle("Pult3");
        primaryStage.setScene(new Scene(root));
        primaryStage.show();
    }

    @Override
    public void stop() throws Exception {
        env.done();
        super.stop();
    }

    public static void main(String[] args) {
        launch(args);
    }
}
