package Application.Preferences;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

public class LocalSettings {

    public static class InfoProviderAuth {

        public String publicKey;

        public String secretKey;

    }

    public static class InfoTelegramAuth {
        public String token;
    }

    public static class InfoTelegramUser {
        public String access;
        public String name;
        public String phone;
    }

    public static class InfoTelegramUserIndexed {
        public int id;
        public String access;
        public String name;
        public String phone;
    }

    private ObjectMapper objectMapper;

    private String telegramBotNameDefault;

    private HashMap<String,  InfoProviderAuth> providerAuths;
    private HashMap<String,  InfoTelegramAuth> telegramAuths;
    private HashMap<Integer, InfoTelegramUser> telegramUsers;
    private ArrayList<InfoTelegramUserIndexed> telegramUsersIndexed;

    public LocalSettings(ObjectMapper objectMapper) {
        this.objectMapper = objectMapper;
    }

    public void load()
    {
        providerAuths = new HashMap<String,  InfoProviderAuth>();
        telegramAuths = new HashMap<String,  InfoTelegramAuth>();
        telegramUsers = new HashMap<Integer, InfoTelegramUser>();
        telegramUsersIndexed = new ArrayList<InfoTelegramUserIndexed>();

        try {
        	File lp = new File("localsettings.json");

        	if (!lp.exists()) {
        		if (!lp.createNewFile()) {
        			throw new IOException("Cannot create local settings json file");
        		} else {
        			String initial_content = "{\"providerAuths\":[],\"telegramBotNameDefault\":\"\",\"telegramAuths\": [],\"telegramUsers\":[]}";
        			FileOutputStream out = new FileOutputStream(lp);
        			out.write(initial_content.getBytes());
        			out.close();
        		}
        	}

        	FileInputStream file = new FileInputStream(lp);	

            JsonNode nodeRoot = objectMapper.readTree(file);

            // Load provider auth info
            ArrayNode nodeProviderAuths = (ArrayNode) nodeRoot.get("providerAuths");
            for (int i = 0; i < nodeProviderAuths.size(); ++i) {
                JsonNode nodeIndex = nodeProviderAuths.get(i);
                InfoProviderAuth infoProviderAuth = new InfoProviderAuth();
                infoProviderAuth.publicKey = nodeIndex.get("publicKey").asText();
                infoProviderAuth.secretKey = nodeIndex.get("secretKey").asText();
                providerAuths.put(nodeIndex.get("name").asText(), infoProviderAuth);
            }

            telegramBotNameDefault = nodeRoot.get("telegramBotNameDefault").asText();

            // Load telegram bot auth info
            ArrayNode nodeTelegramAuths = (ArrayNode) nodeRoot.get("telegramAuths");
            for (int i = 0; i < nodeTelegramAuths.size(); ++i) {
                JsonNode nodeIndex = nodeTelegramAuths.get(i);
                InfoTelegramAuth infoTelegramAuth = new InfoTelegramAuth();
                infoTelegramAuth.token = nodeIndex.get("token").asText();
                telegramAuths.put(nodeIndex.get("name").asText(), infoTelegramAuth);
            }

            // Load telegram bot users info
            ArrayNode nodeTelegramUsers = (ArrayNode) nodeRoot.get("telegramUsers");
            telegramUsersIndexed.ensureCapacity(nodeTelegramUsers.size());
         
            for (int i = 0; i < nodeTelegramUsers.size(); ++i) {
                JsonNode nodeIndex = nodeTelegramUsers.get(i);
                InfoTelegramUser infoTelegramUser = new InfoTelegramUser();
                infoTelegramUser.access = nodeIndex.get ("access").asText();
                infoTelegramUser.name   = nodeIndex.path("name"  ).asText();
                infoTelegramUser.phone  = nodeIndex.path("phone" ).asText();
                telegramUsers.put(nodeIndex.get("id").asInt(), infoTelegramUser);

                InfoTelegramUserIndexed infoTelegramUserIndexed = new InfoTelegramUserIndexed();
                infoTelegramUserIndexed.id     = nodeIndex.get ("id").asInt();
                infoTelegramUserIndexed.access = nodeIndex.get ("access").asText();
                infoTelegramUserIndexed.name   = nodeIndex.path("name"  ).asText();
                infoTelegramUserIndexed.phone  = nodeIndex.path("phone" ).asText();
                telegramUsersIndexed.add(infoTelegramUserIndexed);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public InfoProviderAuth getProviderAuth(String providerName) { return providerAuths.get(providerName); }

    public String getTelegramBotNameDefault() { return telegramBotNameDefault; }

    public InfoTelegramAuth getTelegramAuth(String name) { return telegramAuths.get(name); }

    public InfoTelegramUser getTelegramUser(Integer id) { return telegramUsers.get(id); }

    public int getTelegramUserCount() { return telegramUsersIndexed.size(); }

    public InfoTelegramUserIndexed getTelegramUserIndexed(int index) { return telegramUsersIndexed.get(index); }
}
