package Application.Preferences;

import Application.Literals;
import Model.Marketplace.Base.Currency.Currency;
import Model.Marketplace.Base.Currency.Direction;
//import Telegram.Menu;
import Model.Marketplace.Base.Currency.DirectionPair;
import com.fasterxml.jackson.core.JsonFactory;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.JsonNodeFactory;
import com.fasterxml.jackson.databind.node.ObjectNode;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.math.BigDecimal;

public class PreferencesApp extends Preferences {

    protected static Logger log = LogManager.getLogger(PreferencesApp.class.getName());

    private static final String nodeApp = "app";
    private static final String nodeAppLoggingLevelBrief   = "logginglevelbrief";
    private static final String nodeAppLoggingLevelVerbose = "logginglevelverbose";
    private static final String nodeAppTransferFee = "transferfee";
    private static final String nodeAppArbitrageMode = "arbitragemode";
    private static final String nodeAppTelegram = "telegram";
    private static final String nodeAppLogging = "logging";

    private static final Level         defaultAppLoggingLevelBrief   = Level.WARN;
    private static final Level         defaultAppLoggingLevelVerbose = Level.TRACE;
    private static final BigDecimal    defaultAppTransferFee         = BigDecimal.ZERO;
    private static final ArbitrageMode defaultAppArbitrageMode       = ArbitrageMode.OBSERVE;
    private static final boolean       defaultAppTelegram            = false;
    private static final boolean       defaultAppLogging             = false;

    private static final String nodeCurrency = "currency";
    private static final String nodeCurrencyOrbitMin = "orbitMin";
    private static final String nodeCurrencyOrbitMax = "orbitMax";
    private static final String nodeCurrencySelected = "selected";
    private static final String nodeCurrencyMargin = "margin";
    private static final String nodeCurrencyLowFunds = "lowfunds";
    private static final String nodeCurrencySuperfluousFunds = "superfluousfunds";
    private static final String nodeCurrencyArbitrageLimit = "arbitragelimit";

    private static final boolean defaultCurrencySelected = false;
    private static final BigDecimal defaultCurrencyOrbitMin = BigDecimal.ZERO;
    private static final BigDecimal defaultCurrencyOrbitMax = BigDecimal.ZERO;
    private static final BigDecimal defaultCurrencyMargin = BigDecimal.ZERO;
    private static final BigDecimal defaultCurrencyLowFunds = BigDecimal.ZERO;
    private static final BigDecimal defaultCurrencySuperfluousFunds = BigDecimal.ZERO;
    private static final BigDecimal defaultCurrencyArbitrageLimit = BigDecimal.ZERO;

    private static final String nodeDirection = "direction";
    private static final String nodeDirectionPlast = "plast";
    private static final String nodeDirectionSize = "size";
    private static final String nodeDirectionAround = "around";
    private static final String nodeDirectionMargin = "margin";
    private static final String nodeDirectionRateSourcePlast = "rateSourcePLast";

    private static final double defaultDirectionPlast = 222.0;
    private static final BigDecimal defaultDirectionSize = BigDecimal.TEN;
    private static final boolean defaultDirectionAround = false;
    private static final BigDecimal defaultDirectionMargin = BigDecimal.ONE;
    private static final double defaultDirectionRateSourcePlast = 500.0;


//    private static final String nodeMarketplace = "marketplace";
//    private static final String nodeMarketplaceNonce = "nonce";
//
//    private static final long defaultMarketplaceNonce = 0;
//
//    private static final String nodeTelegram = "telegram";
//    private static final String nodeTelegramUsers = "users";
//    private static final String nodeTelegramUserChatID = "chatid";
//    private static final String nodeTelegramUserUpdates = "updates";
//    private static final String nodeTelegramUserMenu = "menu";


//    private static final int       defaultTelegramUserChatID  = 0;
//    private static final boolean   defaultTelegramUserUpdates = false;
//    private static final Menu.Data defaultTelegramUserMenu = null;

    public enum Name {
        LOGGINGLEVELBRIEF("Brief log level"),
        LOGGINGLEVELVERBOSE("Verbose log level"),
        TELEGRAM("Telegram");

        public String name;

        private Name(String name) {
            this.name = name;
        }
    }

    public enum Param {
        MARKETPLACETYPE,
        CURRENCY,
        CURRENCYPAIR,
        DIRECTION;
    }

    public enum ArbitrageMode {
        OBSERVE,
        LIMITED;
//        STANDARD;
    }

    //////////////////////////////////////////////////
    // Constructor.

    public PreferencesApp(JsonFactory jsonFactory, ObjectMapper objectMapper, JsonNodeFactory jsonNodeFactory) {
        super(jsonFactory, objectMapper, jsonNodeFactory);
    }

    //////////////////////////////////////////////////
    // App

    private JsonNode getNodeApp() {
        return path(getNodeMain(), nodeApp);
    }

    public void setLoggingLevelBrief(Level level) { setLoggingLevelBrief(level.name()); }

    public void setLoggingLevelBrief(String levelName) {
        if (setPreferenceIfDiffers((ObjectNode) getNodeApp(), nodeAppLoggingLevelBrief, levelName, String.class)) {
            broadcastEventPreferenceChanged(Name.LOGGINGLEVELBRIEF, null, levelName);
        }
    }

    public Level getLoggingBrief() {
        try {
            return Level.valueOf(getPreference(getNodeApp(), nodeAppLoggingLevelBrief, String.class));
        } catch (IllegalArgumentException e) {
            log.error(String.format(Literals.logErrorPreferencesInvalidValue, nodeAppLoggingLevelBrief));
            return defaultAppLoggingLevelBrief;
        }
    }

    public void setLoggingLevelVerbose(Level level) { setLoggingLevelVerbose(level.name()); }

    public void setLoggingLevelVerbose(String levelName) {
        if (setPreferenceIfDiffers((ObjectNode) getNodeApp(), nodeAppLoggingLevelVerbose, levelName, String.class)) {
            broadcastEventPreferenceChanged(Name.LOGGINGLEVELVERBOSE, null, levelName);
        }
    }

    public Level getLoggingVerbose() {
        try {
            return Level.valueOf(getPreference(getNodeApp(), nodeAppLoggingLevelVerbose, String.class));
        } catch (IllegalArgumentException e) {
            log.error(String.format(Literals.logErrorPreferencesInvalidValue, nodeAppLoggingLevelVerbose));
            return defaultAppLoggingLevelVerbose;
        }
    }

    public void setTransferFee(BigDecimal transferFee) {
        setPreferenceIfDiffers((ObjectNode) getNodeApp(), nodeAppTransferFee, transferFee, defaultAppTransferFee, BigDecimal.class);
    }

    public BigDecimal getTransferFee() {
        return getPreference(getNodeApp(), nodeAppTransferFee, defaultAppTransferFee, BigDecimal.class);
    }

    public void setArbitrageMode(ArbitrageMode arbitrageMode) {
        setPreferenceIfDiffers((ObjectNode) getNodeApp(), nodeAppArbitrageMode, arbitrageMode.name(), defaultAppArbitrageMode.name(), String.class);
    }

    public ArbitrageMode getArbitrageMode() {
        try {
            return ArbitrageMode.valueOf(getPreference(getNodeApp(), nodeAppArbitrageMode, String.class));
        } catch (IllegalArgumentException e) {
            log.error(String.format(Literals.logErrorPreferencesInvalidValue, nodeAppArbitrageMode));
            return defaultAppArbitrageMode;
        }
    }

    public void setTelegram(boolean telegram) {
        if (setPreferenceIfDiffers((ObjectNode) getNodeApp(), nodeAppTelegram, telegram, defaultAppTelegram, Boolean.class)) {
            broadcastEventPreferenceChanged(Name.TELEGRAM, null, telegram);
        }
    }

    public boolean getTelegram() {
        return getPreference(getNodeApp(), nodeAppTelegram, defaultAppTelegram, Boolean.class);
    }

    public void setLogging(boolean logging) {
        setPreferenceIfDiffers((ObjectNode) getNodeApp(), nodeAppLogging, logging, defaultAppLogging, Boolean.class);
    }

    public boolean getLogging() {
        return getPreference(getNodeApp(), nodeAppLogging, defaultAppLogging, Boolean.class);
    }

    //////////////////////////////////////////////////
    // Marketplace

    private JsonNode getNodeDirection(Direction direction){
        return path(getNodeMain(),nodeDirection+direction.getName());
    }

    public Double getPlast(Direction direction){
        return getPreference(getNodeDirection(direction), nodeDirectionPlast, defaultDirectionPlast, Double.class);
    }

    public void setPlast(Direction direction, double plast){
        setPreferenceIfDiffers((ObjectNode) getNodeDirection(direction), nodeDirectionPlast, plast,defaultDirectionPlast, Double.class);
    }

    public BigDecimal getSize(Direction direction){
        return getPreference(getNodeDirection(direction), nodeDirectionSize, defaultDirectionSize, BigDecimal.class);
    }

    public void setSize(Direction direction, BigDecimal size){
        setPreferenceIfDiffers((ObjectNode) getNodeDirection(direction), nodeDirectionSize,size, defaultDirectionSize, BigDecimal.class);
    }

    public void setAround(Direction direction, boolean selected) {
        setPreferenceIfDiffers((ObjectNode) getNodeDirection(direction), nodeDirectionAround, selected, defaultDirectionAround, Boolean.class);
    }

    public boolean getAround(Direction direction) {
        return getPreference(getNodeDirection(direction), nodeDirectionAround, defaultDirectionAround, Boolean.class);
    }

    public BigDecimal getMargin(Direction direction){
        return getPreference(getNodeDirection(direction), nodeDirectionMargin, defaultDirectionMargin, BigDecimal.class);
    }

    public void setMargin(Direction direction, BigDecimal margin){
        setPreferenceIfDiffers((ObjectNode) getNodeDirection(direction), nodeDirectionMargin, margin, defaultDirectionMargin, BigDecimal.class);
    }

    public double getRateSourcePlast(DirectionPair directionPair){
        return getPreference(getNodeDirection(directionPair.ask), nodeDirectionRateSourcePlast, defaultDirectionRateSourcePlast, Double.class);
    }

    public void setRateSourcePlast(DirectionPair directionPair, double rateSourcePlast){
        setPreferenceIfDiffers((ObjectNode) getNodeDirection(directionPair.ask), nodeDirectionRateSourcePlast, rateSourcePlast, defaultDirectionRateSourcePlast, Double.class);
    }

    private JsonNode getNodeCurrency(Currency currency) {
        return path(getNodeMain(), nodeCurrency + currency.name);
    }

    public BigDecimal getOrbitMin(Currency currency){
        return getPreference(getNodeCurrency(currency), nodeCurrencyOrbitMin, defaultCurrencyOrbitMin, BigDecimal.class);
    }

    public void setOrbitMin(Currency currency, BigDecimal orbitMin){
        setPreferenceIfDiffers((ObjectNode) getNodeCurrency(currency), nodeCurrencyOrbitMin, orbitMin, defaultCurrencyOrbitMin, BigDecimal.class);
    }

    public BigDecimal getOrbitMax(Currency currency){
        return getPreference(getNodeCurrency(currency), nodeCurrencyOrbitMax, defaultCurrencyOrbitMax, BigDecimal.class);
    }

    public void setOrbitMax(Currency currency, BigDecimal orbitMax){
        setPreferenceIfDiffers((ObjectNode) getNodeCurrency(currency), nodeCurrencyOrbitMax, orbitMax, defaultCurrencyOrbitMax, BigDecimal.class);
    }

    public void setSelected(Currency currency, boolean selected) {
        setPreferenceIfDiffers((ObjectNode) getNodeCurrency(currency), nodeCurrencySelected, selected, defaultCurrencySelected, Boolean.class);
    }

    public boolean getSelected(Currency currency) {
        return getPreference(getNodeCurrency(currency), nodeCurrencySelected, defaultCurrencySelected, Boolean.class);
    }

    public void setMargin(Currency currency, BigDecimal margin) {
        setPreferenceIfDiffers((ObjectNode) getNodeCurrency(currency), nodeCurrencyMargin, margin, defaultCurrencyMargin, BigDecimal.class);
    }

    public BigDecimal getMargin(Currency currency) {
        return getPreference(getNodeCurrency(currency), nodeCurrencyMargin, defaultCurrencyMargin, BigDecimal.class);
    }

    public void setLowFunds(Currency currency, BigDecimal lowFunds) {
        setPreferenceIfDiffers((ObjectNode) getNodeCurrency(currency), nodeCurrencyLowFunds, lowFunds, defaultCurrencyLowFunds, BigDecimal.class);
    }

    public BigDecimal getLowFunds(Currency currency) {
        return getPreference(getNodeCurrency(currency), nodeCurrencyLowFunds, defaultCurrencyLowFunds, BigDecimal.class);
    }

    public void setSuperfluousFunds(Currency currency, BigDecimal superfluousFunds) {
        setPreferenceIfDiffers((ObjectNode) getNodeCurrency(currency), nodeCurrencySuperfluousFunds, superfluousFunds, defaultCurrencySuperfluousFunds, BigDecimal.class);
    }

    public BigDecimal getSuperfluousFunds(Currency currency) {
        return getPreference(getNodeCurrency(currency), nodeCurrencySuperfluousFunds, defaultCurrencySuperfluousFunds, BigDecimal.class);
    }

    public void setArbitrageLimit(Currency currency, BigDecimal arbitrageLimit) {
        setPreferenceIfDiffers((ObjectNode) getNodeCurrency(currency), nodeCurrencyArbitrageLimit, arbitrageLimit, defaultCurrencyArbitrageLimit, BigDecimal.class);
    }

    public BigDecimal getArbitrageLimit(Currency currency) {
        return getPreference(getNodeCurrency(currency), nodeCurrencyArbitrageLimit, defaultCurrencyArbitrageLimit, BigDecimal.class);
    }

    //////////////////////////////////////////////////
    // Marketplace

//    private JsonNode getNodeMarketplace(Marketplace.Type marketplaceType) {
//        return path(getNodeMain(), nodeMarketplace + marketplaceType.name);
//    }

//    public void setNonce(Marketplace.Type marketplaceType, long nonce) {
//        setPreferenceIfDiffers((ObjectNode) getNodeMarketplace(marketplaceType), nodeMarketplaceNonce, nonce, defaultMarketplaceNonce, Long.class);
//    }
//
//    public long getNonce(Marketplace.Type marketplaceType) {
//        return getPreference(getNodeMarketplace(marketplaceType), nodeMarketplaceNonce, defaultMarketplaceNonce, Long.class);
//    }

    //////////////////////////////////////////////////
    // Telegram

//    private JsonNode getNodeTelegram() {
//        return path(getNodeMain(), nodeTelegram);
//    }
//
//    private JsonNode getNodeTelegramUsers() {
//        return path(getNodeTelegram(), nodeTelegramUsers);
//    }
//
//    public List<Integer> getTelegramUsers() {
//        JsonNode nodeTelegramUsers = getNodeTelegramUsers();
//
//        ArrayList<Integer> userIDs = new ArrayList<Integer>(nodeTelegramUsers.size());
//
//        Iterator<Map.Entry<String, JsonNode>> nodeIterator = nodeTelegramUsers.fields();
//        while (nodeIterator.hasNext()) {
//            String userIDText = nodeIterator.next().getKey();
//            try {
//                userIDs.add(Integer.parseInt(userIDText));
//            } catch (NumberFormatException e) {
//                // Do nothing. Skip node.
//            }
//        }
//
//        return userIDs;
//    }
//
//    private ObjectNode getNodeTelegramUser(int userID) { return (ObjectNode) path(getNodeTelegramUsers(), Integer.toString(userID)); }
//
//    public void setTelegramUserChatID(int userID, long chatID) {
//        setPreferenceIfDiffers(getNodeTelegramUser(userID), nodeTelegramUserChatID, chatID, Long.class);
//    }
//
//    public long getTelegramUserChatID(int userID) {
//        return getPreference(getNodeTelegramUser(userID), nodeTelegramUserChatID, Long.class);
//    }
//
//    public void setTelegramUserUpdates(int userID, boolean updates) {
//        setPreferenceIfDiffers(getNodeTelegramUser(userID), nodeTelegramUserUpdates, updates, Boolean.class);
//    }
//
//    public boolean getTelegramUserUpdates(int userID) {
//        return getPreference(getNodeTelegramUser(userID), nodeTelegramUserUpdates, Boolean.class);
//    }

//    public void setTelegramUserMenu(int userID, Menu.Data menu) {
//        setPreferenceIfDiffers(getNodeTelegramUser(userID), nodeTelegramUserMenu, menu.name(), String.class);
//    }
//
//    public Menu.Data getTelegramUserMenu(int userID) {
//        try {
//            return Menu.Data.valueOf(getPreference(getNodeTelegramUser(userID), nodeTelegramUserMenu, String.class));
//        } catch (IllegalArgumentException e) {
//            log.error(String.format(Literals.logErrorPreferencesInvalidValue, nodeTelegramUserMenu));
//            return defaultTelegramUserMenu;
//        }
//    }
}
