package Application.Preferences;

import Application.Events.EventDispatcher;
import Application.Events.EventListener;
import com.fasterxml.jackson.core.JsonFactory;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.JsonNodeFactory;
import com.fasterxml.jackson.databind.node.ObjectNode;

import java.io.*;
import java.math.BigDecimal;
import java.util.Map;

public class Preferences extends EventDispatcher {

    public static class EventPreferenceChanged extends EventListener.Event {
        public Object name;
        public Map<Object, Object> params;
        public Object value;

        private EventPreferenceChanged(Object name, Map<Object, Object> params, Object value) {
            this.name = name;
            this.params = params;
            this.value = value;
        }
    }

    public JsonFactory jsonFactory;
    public ObjectMapper objectMapper;
    public JsonNodeFactory jsonNodeFactory;

    private String fileName;
    protected JsonNode nodeMain;

    private boolean saveAuto;

    //////////////////////////////////////////////////
    // Constructor.

    public Preferences(JsonFactory jsonFactory, ObjectMapper objectMapper, JsonNodeFactory jsonNodeFactory) {
        this.jsonFactory = jsonFactory;
        this.objectMapper = objectMapper;
        this.jsonNodeFactory = jsonNodeFactory;
        setSaveAuto(true);
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    //////////////////////////////////////////////////
    // Data storing.

    public void load() {
        if (nodeMain == null) {
            File f = new File(fileName);

            try {
                FileInputStream fileInputStream = new FileInputStream(f);

                try {
                    nodeMain = objectMapper.readTree(fileInputStream);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
        }
    }

    public void save() {
        if (nodeMain != null) {
            File f = new File(fileName);

            try {
                FileOutputStream fileOutputStream = new FileOutputStream(f);

                try {
                    JsonGenerator jsonGenerator = jsonFactory.createGenerator(fileOutputStream);
                    jsonGenerator.setCodec(objectMapper);
                    jsonGenerator.writeTree(nodeMain);
                    //objectMapper.writeTree(jsonGenerator, nodeMain);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
        }
    }

    public boolean setSaveAuto(boolean saveAuto) {
        boolean saveAutoOld = this.saveAuto;
        this.saveAuto = saveAuto;
        return saveAutoOld;
    }

    protected void saveAuto() {
        if (saveAuto) {
            save();
        }
    }

    //////////////////////////////////////////////////
    // Data access.

    protected JsonNode getNodeMain() {
        if (nodeMain == null) {
            nodeMain = objectMapper.createObjectNode();
        }

        return nodeMain;
    }

    protected static JsonNode path(JsonNode node, String name) {
        JsonNode path = node.get(name);
        if (path == null) {
            path = ((ObjectNode) node).putObject(name);
        }

        return path;
    }

    protected static JsonNode pathArray(JsonNode node, String name) {
        JsonNode path = node.get(name);
        if (path == null) {
            path = ((ObjectNode) node).putArray(name);
        }

        return path;
    }

    //////////////////////////////////////////////////
    // Generics.

    protected <T> boolean setPreferenceIfDiffers(ObjectNode nodeParent, String nodeChildName, T value, Class<T> Tc) {
        if (!getPreference(nodeParent, nodeChildName, Tc).equals(value)) {
            setPreference(nodeParent, nodeChildName, value);
            return true;
        } else {
            return false;
        }
    }

    protected <T> boolean setPreferenceIfDiffers(ObjectNode nodeParent, String nodeChildName, T value, T valueDefault, Class<T> Tc) {
        if (!getPreference(nodeParent, nodeChildName, valueDefault, Tc).equals(value)) {
            setPreference(nodeParent, nodeChildName, value);
            return true;
        } else {
            return false;
        }
    }

    protected <T> void setPreference(ObjectNode nodeParent, String nodeChildName, T value) {
        if (value instanceof Boolean) {
            nodeParent.put(nodeChildName, (Boolean) value);
        }
        if (value instanceof Integer) {
            nodeParent.put(nodeChildName, (Integer) value);
        }
        if (value instanceof Long) {
            nodeParent.put(nodeChildName, (Long) value);
        }
        if (value instanceof Double) {
            nodeParent.put(nodeChildName, (Double) value);
        }
        if (value instanceof String) {
            nodeParent.put(nodeChildName, (String) value);
        }
        if (value instanceof BigDecimal) {
            nodeParent.put(nodeChildName, ((BigDecimal) value).toString());
        }
        saveAuto();
    }

    protected <T> T getPreference(JsonNode nodeParent, String nodeChildName, T valueDefault, Class<T> c) {
        if (c == Boolean.class) {
            return c.cast(path(nodeParent, nodeChildName).asBoolean((Boolean) valueDefault));
        }
        if (c == Integer.class) {
            return c.cast(path(nodeParent, nodeChildName).asInt((Integer) valueDefault));
        }
        if (c == Long.class) {
            return c.cast(path(nodeParent, nodeChildName).asLong((Long) valueDefault));
        }
        if (c == Double.class) {
            return c.cast(path(nodeParent, nodeChildName).asDouble((Double) valueDefault));
        }
        if (c == String.class) {
            return c.cast(path(nodeParent, nodeChildName).asText((String) valueDefault));
        }
        if (c == BigDecimal.class) {
            try {
                return c.cast(new BigDecimal(path(nodeParent, nodeChildName).asText()));
            } catch (NumberFormatException e) {
                return valueDefault;
            }
        }
        return null;
    }

    protected <T> T getPreference(JsonNode nodeParent, String nodeChildName, Class<T> c) {
        if (c == Boolean.class) {
            return c.cast(path(nodeParent, nodeChildName).asBoolean());
        }
        if (c == Integer.class) {
            return c.cast(path(nodeParent, nodeChildName).asInt());
        }
        if (c == Long.class) {
            return c.cast(path(nodeParent, nodeChildName).asLong());
        }
        if (c == Double.class) {
            return c.cast(path(nodeParent, nodeChildName).asDouble());
        }
        if (c == String.class) {
            return c.cast(path(nodeParent, nodeChildName).asText());
        }
        if (c == BigDecimal.class) {
            try {
                return c.cast(new BigDecimal(path(nodeParent, nodeChildName).asText()));
            } catch (NumberFormatException e) {
                // Do nothing. Return null.
            }
        }
        return null;
    }

    //////////////////////////////////////////////////
    // Event dispatcher

    void broadcastEventPreferenceChanged(Object name, Map<Object, Object> params, Object value) {
        broadcast(new EventPreferenceChanged(name, params, value));
    }
}
