package Application;

import Application.Events.EventDispatcher;
import Application.Events.EventListener;

import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public class Timer extends EventDispatcher {

    public static class EventTimer extends EventListener.Event {}

    private static final int timeoutDefault = 1000;

    private int timeout;
    private boolean isRunning;
    private ScheduledExecutorService execService;
    private Future<?> future;

    public Timer() {
        this.timeout = timeoutDefault;
        execService = Executors.newSingleThreadScheduledExecutor();
    }

    public void done() {
        stop();
        execService.shutdown();
    }

    public void setTimeout(int timeout) {
        this.timeout = timeout;
    }

    /**
     * Starts the timer. If the timer was already running, this call is ignored.
     */
    public void start() {
        if (isRunning) {
            return;
        }

        isRunning = true;
        future = execService.scheduleWithFixedDelay(new Runnable() {
            @Override
            public void run() {
                broadcast(new EventTimer());
            }
        }, 0, timeout, TimeUnit.MILLISECONDS);
    }

    /**
     * Paused the timer. If the timer is not running, this call is ignored.
     */
    public void stop() {
        if (!isRunning) {
            return;
        }

        if (future != null) {
            future.cancel(false);
        }
        isRunning = false;
    }
}
