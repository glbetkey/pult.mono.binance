package Application.Events;

import java.util.ArrayList;
import java.util.List;

public abstract class EventListenerAsync implements EventListener {

    private Thread thread;
    private List<Event> events;

    public EventListenerAsync() {
        events = new ArrayList<>();
    }

    public void start() {
        thread = new Thread(() -> { run(); });
        thread.start();
    }

    public void stop() {
        thread.interrupt();
    }

    private void run() {
        // Run indefinitely until stopped from outside.
        while (true) {
            synchronized (events) {
                // Remove processed event.
                if (events.size() > 0) {
                    events.remove(0);
                }

                // Wait until there's an event in the queue.
                try {
                    while (events.size() == 0) {
                        events.wait();
                    }
                } catch (InterruptedException e) {
                    break;
                }
            }

            listenAsync(events.get(0));
        }
    }

    protected abstract void listenAsync(Event event);

    @Override
    public void listen(Event event) {
        synchronized (events) {
            events.add(event);
            if (events.size() == 1) {
                events.notify();
            }
        }
    }
}
