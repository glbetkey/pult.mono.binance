package Application.Events;

import java.util.HashSet;
import java.util.Set;

public class EventDispatcher {
    private Set<EventListener> eventListeners;

    public EventDispatcher() {
        eventListeners = new HashSet<EventListener>();
    }

    public synchronized void addListener(EventListener eventListener) {
        eventListeners.add(eventListener);
    }

    public synchronized void remListener(EventListener eventListener) {
        eventListeners.remove(eventListener);
    }

    public synchronized void broadcast(EventListener.Event event) {
        for (EventListener eventListener : eventListeners) {
            eventListener.listen(event);
        }
    }
}
