package Application.Events;

public interface EventListener {
    class Event {
    }

    void listen(Event event);
}
