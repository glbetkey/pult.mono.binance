package View;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.scene.control.Spinner;
import javafx.scene.control.SpinnerValueFactory;
import javafx.util.StringConverter;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.ParseException;

/**
 * Created by FlyingPirate on 13/05/2017.
 */
public abstract class SpinnerController {

    void setSpinner(Spinner<Double> spinner, double min, double max, double initialValue, double amountToStepBy) {
        spinner.setValueFactory(new SpinnerValueFactory.DoubleSpinnerValueFactory(min, max, min, amountToStepBy));
        setDoubleSpinnerStringConverter(spinner);
        spinner.getValueFactory().setValue(initialValue);
        setSpinnerFocusProperty(spinner);
        spinner.setOnScroll(event -> {
            if (event.getDeltaY() < 0) {
                spinner.decrement();
            } else if (event.getDeltaY() > 0) {
                spinner.increment();
            }
        });
        spinner.setFocusTraversable(false);
        spinner.setEditable(true);
    }

    /**
     * для того, чтобы писать числа с точкой, а не с запятой
     *
     * @param spinner
     */
    private void setDoubleSpinnerStringConverter(Spinner<Double> spinner) {
        DecimalFormatSymbols dfs = new DecimalFormatSymbols();
        dfs.setDecimalSeparator('.');
        SpinnerValueFactory<Double> valueFactory = spinner.getValueFactory();
        valueFactory.setConverter(new StringConverter<Double>() {
            private final DecimalFormat df = new DecimalFormat("#.####", dfs);

            @Override
            public String toString(Double value) {
                // If the specified value is null, return a zero-length String
                if (value == null) {
                    return "";
                }

                return df.format(value);
            }

            @Override
            public Double fromString(String value) {
                try {
                    // If the specified value is null or zero-length, return null
                    if (value == null) {
                        return null;
                    }
                    value = value.trim();
                    if (value.length() < 1) {
                        return null;
                    }
                    // Perform the requested parsing
                    return df.parse(value).doubleValue();
                } catch (ParseException ex) {
                    throw new RuntimeException(ex);
                }
            }
        });
    }

    private void setSpinnerFocusProperty(Spinner spinner) {
        spinner.focusedProperty().addListener(new ChangeListener<Boolean>() {
            @Override
            public void changed(ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) {
                onSpinnerChange(spinner);
            }
        });
    }

    abstract void onSpinnerChange(Spinner spinner);

    <T> void commitEditorText(Spinner<T> spinner) {
        if (!spinner.isEditable()) return;
        String text = spinner.getEditor().getText();
        SpinnerValueFactory<T> valueFactory = spinner.getValueFactory();
        if (valueFactory != null) {
            StringConverter<T> converter = valueFactory.getConverter();
            if (converter != null) {
                T value = converter.fromString(text);
                valueFactory.setValue(value);
            }
        }
    }
}
