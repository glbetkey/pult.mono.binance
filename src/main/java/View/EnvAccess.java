package View;

import Application.Env;

public class EnvAccess {
    // This field is here only for the controllers to get a reference to initialize themselves. You shouldn't
    // access this field from any other class.
    static Env env;

    static public void setEnv(Env env) {
        EnvAccess.env = env;
    }
}