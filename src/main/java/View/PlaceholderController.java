package View;

import Application.Env;
import Application.Events.EventListener;
import Application.Events.EventListenerAsync;
import Application.Literals;
import Application.Preferences.PreferencesApp;
import Model.Marketplace.Base.Connector;
import Model.Marketplace.Base.Currency.DirectionPair;
import Model.Marketplace.Base.Market.Order;
import Model.Marketplace.Base.Marketplace;
import Model.Marketplace.MarketplacesManager;
import Model.Stats.SessionsManager;
import javafx.application.Platform;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Set;

/**
 * Created by FlyingPirate on 18.11.2017.
 */
public class PlaceholderController extends SpinnerController implements EventListener  {

    private static Logger log = LogManager.getLogger(MainWindowController.class.getName());
    public Spinner<Double> marginSpinnerAsk;
    public Spinner<Double> rateSoursePlastSpinner;
    public Spinner<Double> marginSpinnerBid;
    public Label labelMarginAsk;
    public Label labelRateSourcePlast;
    public Label labelMarginBid;

    private Env env;

    private ChangesEventListener changesEventListener;

    private PreferencesApp      preferencesApp;
    private SessionsManager     sessionsManager;
    private MarketplacesManager marketplacesManager;

    private Marketplace.Type visibleMarketplaceType;
    private DirectionPair visibleDirectionPair;

    ObservableList<Order> visibleOpenOrders;



//    private class OrderMonitor implements Runnable{
//        Order order;
////        Direction direction;
//
//        @Override
//        public void run() {
//            synchronized (order) {
//                while (!order.isFinished()) {
//                    try {
//                        order.wait();
//                    } catch (InterruptedException e) {
//                    }
//                }
//                visibleOpenOrders.remove(order);
//            }
//        }
//
//        public OrderMonitor(Order order) {
//            this.order = order;
////            this.direction = direction;
//        }
//    }
    @Override
    public void listen(Event event) {
//        log.fatal(event.toString());
        if (event instanceof  Connector.EventMarketplaceInitialized){
            fillComboBoxes();
        }
        if (event instanceof Connector.EventMarketChanged){

        }
//        if (event instanceof  Connector.EventResult){
//            onConnectorResult((Connector.EventResult) event);
//        }
//        if (event instanceof Connector.EventBalanceUpdated){
//            Connector.EventBalanceUpdated eventTyped = (Connector.EventBalanceUpdated) event;
//            log.fatal(eventTyped.marketplaceType);
//            if (eventTyped.orderUpdate && (eventTyped.direction == visibleDirectionPair.ask || eventTyped.direction == visibleDirectionPair.bid)) {
//                ArrayList<Order> orders = new ArrayList<>();
//                orders.addAll(marketplacesManager.marketplaces.get(visibleMarketplaceType).marketsManager.markets.get(visibleDirectionPair.ask).orders);
//                orders.addAll(marketplacesManager.marketplaces.get(visibleMarketplaceType).marketsManager.markets.get(visibleDirectionPair.bid).orders);
//                openOrderTable.setItems(FXCollections.observableArrayList(orders));
//            }
            //todo if orderUpdate - update tableView
//        }
//        if (event instanceof  Connector.EventOrderUpdate){
//            Connector.EventOrderUpdate eventTyped = (Connector.EventOrderUpdate) event;
//            log.fatal(eventTyped.order.toString());
//        }
    }

//    private void onConnectorResult(Connector.EventResult eventResult) {
//        Marketplace.ResultData resultData = eventResult.resultData;
//        if (resultData instanceof Marketplace.ResultDataCancel){
////            onResultCancel((Marketplace.ResultDataCancel) resultData); todo ?
//        }
//        if (resultData instanceof Marketplace.ResultDataMake){
//            onResultMake((Marketplace.ResultDataMake) resultData);
//        }
//    }

//    private void onResultMake(Marketplace.ResultDataMake resultData) {
//        fillOpenOrdersList();
//    }


    public class ChangesEventListener extends EventListenerAsync {
        @Override
        protected void listenAsync(Event event) {
            PlaceholderController.this.listen(event);
        }
    }
    @FXML
    public void initialize(){
        env = EnvAccess.env;

        preferencesApp = env.preferencesApp;
        sessionsManager = env.sessionsManager;
        marketplacesManager = env.marketplacesManager;

        changesEventListener = new ChangesEventListener();
        marketplacesManager.addListener(changesEventListener);
//        sessionsManager.addListener(changesEventListener);
//        preferencesApp.addListener(changesEventListener);

        changesEventListener.start();

        startButton.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                DirectionPair chosen = directionPairComboBox.getValue();
                if (chosen!=null){
                    env.startStrategy(marketplaceComboBox.getValue(),chosen);
                }
                stopButton.setDisable(false);
                startButton.setDisable(true);
            }
        });

        stopButton.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                DirectionPair chosen = directionPairComboBox.getValue();
                if (chosen!=null){
                    env.stopStrategy(chosen);
                }
                stopButton.setDisable(true);
                startButton.setDisable(false);
            }
        });

        saveButton.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                onSaveClick();
                saveButton.setDisable(true);
            }
        });
        visibleOpenOrders = FXCollections.observableArrayList();
        openOrderTable.setItems(visibleOpenOrders);
        initializeComboBoxes();
        initializeSpinners();
        initializeCheckBoxes();
        colId.setCellValueFactory(new PropertyValueFactory<Order, String>("id"));
        colAmount.setCellValueFactory(new PropertyValueFactory<Order, BigDecimal>("amount"));
        colRate.setCellValueFactory(new PropertyValueFactory<Order, BigDecimal>("rate"));
        colFilled.setCellValueFactory(new PropertyValueFactory<Order, BigDecimal>("filled"));
        setEmptyInfo();

//        fillComboBoxes();
    }

    private void onSaveClick() {
        preferencesApp.setOrbitMin(visibleDirectionPair.base,new BigDecimal(orbitMinSpinner.getValue()));
        preferencesApp.setOrbitMax(visibleDirectionPair.base,new BigDecimal(orbitMaxSpinner.getValue()));
        preferencesApp.setPlast   (visibleDirectionPair.ask,plastSpinnerAsk.getValue());
        preferencesApp.setPlast   (visibleDirectionPair.bid,plastSpinnerBid.getValue());
        preferencesApp.setSize    (visibleDirectionPair.ask,new BigDecimal(sizeSpinnerAsk.getValue()));
        preferencesApp.setSize    (visibleDirectionPair.bid,new BigDecimal(sizeSpinnerBid.getValue()));
        preferencesApp.setAround  (visibleDirectionPair.ask,aroundCheckBoxAsk.isSelected());
        preferencesApp.setAround  (visibleDirectionPair.bid,aroundCheckBoxBid.isSelected());
        preferencesApp.setMargin(visibleDirectionPair.ask,new BigDecimal(marginSpinnerAsk.getValue()));
        preferencesApp.setMargin(visibleDirectionPair.bid,new BigDecimal(marginSpinnerBid.getValue()));
        preferencesApp.setRateSourcePlast(visibleDirectionPair, rateSoursePlastSpinner.getValue());
        preferencesApp.setRateSourcePlast(visibleDirectionPair, rateSoursePlastSpinner.getValue());

        saveButton.setDisable(true);
    }


    private void initializeComboBoxes(){
        marketplaceComboBox.valueProperty().addListener(new ChangeListener<Marketplace.Type>() {
            @Override
            public void changed(ObservableValue<? extends Marketplace.Type> observable, Marketplace.Type oldValue, Marketplace.Type newValue) {
                visibleMarketplaceType = newValue;
                if (newValue==null){
                    setEmptyInfo();
                    return;}
                Marketplace chosen = marketplacesManager.marketplaces.get(newValue);
                Set<DirectionPair> directionSet = chosen.marketsManager.marketsRequired;
                ArrayList<DirectionPair> directionPairList = new ArrayList<>();
                directionPairList.addAll(directionSet);
                if (directionPairList.size()>0){
                    directionPairComboBox.setItems(FXCollections.observableArrayList(directionPairList));
                    directionPairComboBox.setValue(directionPairList.get(0));
                }
            }
        });
        directionPairComboBox.valueProperty().addListener(new ChangeListener<DirectionPair>() {
            @Override
            public void changed(ObservableValue<? extends DirectionPair> observable, DirectionPair oldValue, DirectionPair newValue) {
                visibleDirectionPair = newValue;
                if (newValue==null){
                    setEmptyInfo();
                    return;}
                fillWithInfo();
            }
        });
    }
    private void initializeSpinners(){
        setSpinner(orbitMinSpinner,0,Double.MAX_VALUE,0,25);
        setSpinner(orbitMaxSpinner,0,Double.MAX_VALUE, 99999999,25);
        setSpinner(plastSpinnerAsk,0,Double.MAX_VALUE,0,50);
        setSpinner(plastSpinnerBid,0,Double.MAX_VALUE,0,50);
        setSpinner(sizeSpinnerAsk,0,Double.MAX_VALUE ,0,10);
        setSpinner(sizeSpinnerBid,0,Double.MAX_VALUE ,0,10);
        setSpinner(marginSpinnerAsk,1,Double.MAX_VALUE,1,0.01);
        setSpinner(rateSoursePlastSpinner,0,Double.MAX_VALUE ,0,100);
        setSpinner(marginSpinnerBid,0,1 ,1,0.01);
    }
    private void initializeCheckBoxes(){
        EventHandler<ActionEvent> eventHandler = new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                saveButton.setDisable(false);
            }
        };
        aroundCheckBoxAsk.setOnAction(eventHandler);
        aroundCheckBoxBid.setOnAction(eventHandler);
    }

    private void setEmptyInfo() {
        startButton.setDisable(true);
        stopButton .setDisable(true);
        assetsTableView.setItems(FXCollections.observableArrayList());//todo
        tradesTable    .setItems(FXCollections.observableArrayList());//todo
        visibleOpenOrders.clear();
        orbitLabel.setText(String.format(Literals.orbitBaseLabel, "BASE"));
        saveButton.setDisable(true);
    }

    private void fillWithInfo(){
        boolean activePair = env.isActiveStrategyPair(visibleDirectionPair);
        startButton.setDisable(activePair);
        stopButton .setDisable(!activePair);
        saveButton .setDisable(true);

        loadPreferences();
        orbitLabel.setText(String.format(Literals.orbitBaseLabel, visibleDirectionPair.base.getName()));

//        fillOpenOrdersList();;
        //todo trades?
        //todo offers?
    }

//    private void fillOpenOrdersList(){
//        for (Order order : marketplacesManager.marketplaces.get(visibleMarketplaceType).marketsManager.markets.get(visibleDirectionPair.ask).orders.values()) {
//            if(order.isActive()){
//                if (!visibleOpenOrders.contains(order)) {
//                    visibleOpenOrders.add(order);
//                    new Thread(new OrderMonitor(order));
//                }
//            }
//        }
//        for (Order order : marketplacesManager.marketplaces.get(visibleMarketplaceType).marketsManager.markets.get(visibleDirectionPair.bid).orders.values()) {
//            if(order.isActive()){
//                if (!visibleOpenOrders.contains(order)) {
//                    visibleOpenOrders.add(order);
//                    new Thread(new OrderMonitor(order));
//                }
//            }
//        }
//
//    }

    private void loadPreferences() {
        orbitMinSpinner  .getValueFactory().setValue(preferencesApp.getOrbitMin(visibleDirectionPair.base).doubleValue());
        orbitMaxSpinner  .getValueFactory().setValue(preferencesApp.getOrbitMax(visibleDirectionPair.base).doubleValue());
        plastSpinnerAsk  .getValueFactory().setValue(preferencesApp.getPlast(visibleDirectionPair.ask));
        plastSpinnerBid  .getValueFactory().setValue(preferencesApp.getPlast(visibleDirectionPair.bid));
        sizeSpinnerAsk   .getValueFactory().setValue(preferencesApp.getSize(visibleDirectionPair.ask).doubleValue());
        sizeSpinnerBid   .getValueFactory().setValue(preferencesApp.getSize(visibleDirectionPair.bid).doubleValue());
        aroundCheckBoxAsk.setSelected(preferencesApp.getAround(visibleDirectionPair.ask));
        aroundCheckBoxBid.setSelected(preferencesApp.getAround(visibleDirectionPair.bid));
        marginSpinnerAsk .getValueFactory().setValue(preferencesApp.getMargin(visibleDirectionPair.ask).doubleValue());
        rateSoursePlastSpinner.getValueFactory().setValue(preferencesApp.getRateSourcePlast(visibleDirectionPair));
        marginSpinnerBid .getValueFactory().setValue(preferencesApp.getMargin(visibleDirectionPair.bid).doubleValue());
    }


    /**
     * маректплейсМанагер должен быть инициализирован
     *
     */
    private void fillComboBoxes(){
        Platform.runLater(new Runnable() {
            @Override
            public void run() {
                if (marketplaceComboBox.getItems() != null) {
                    marketplaceComboBox.getItems().clear();
                }
                if (directionPairComboBox.getItems()!=null) {
                    directionPairComboBox.getItems().clear();
                }

                Set<Marketplace.Type> activeMarketPlaces = marketplacesManager.marketplaces.keySet();
                ArrayList<Marketplace.Type> markList= new ArrayList<>();
                markList.addAll(activeMarketPlaces);
                if(markList.size()>0){
                    marketplaceComboBox.setItems(FXCollections.observableArrayList(markList));
                    marketplaceComboBox.setValue(markList.get(0));
                }
            }
        });
    }


    @Override
    void onSpinnerChange(Spinner spinner) {
        commitEditorText(spinner);
        saveButton.setDisable(false);
    }


    @FXML
    private ComboBox<Marketplace.Type> marketplaceComboBox;

    @FXML
    private ComboBox<DirectionPair> directionPairComboBox;

    @FXML
    private Button startButton;
    @FXML
    private Button stopButton;
    @FXML
    private Label orbitLabel;
    @FXML
    private Spinner<Double> orbitMinSpinner;
    @FXML
    private Spinner<Double> orbitMaxSpinner;
    @FXML
    private Spinner<Double> plastSpinnerAsk;
    @FXML
    private Spinner<Double> sizeSpinnerAsk;
    @FXML
    private CheckBox aroundCheckBoxAsk;
    @FXML
    private Spinner<Double> plastSpinnerBid;
    @FXML
    private Spinner<Double> sizeSpinnerBid;
    @FXML
    private CheckBox aroundCheckBoxBid;
    @FXML
    private Button saveButton;
    @FXML
    private TableView assetsTableView;
    @FXML
    private TableColumn colPurseName;
    @FXML
    private TableColumn colPurseOpen;
    @FXML
    private TableColumn colPurseLocked;
    @FXML
    private TableColumn colPurseTotal;
    @FXML
    private TableView<Order> openOrderTable;
    @FXML
    private TableColumn<Order,String> colId;
    @FXML
    private TableColumn<Order,BigDecimal> colAmount;
    @FXML
    private TableColumn<Order,BigDecimal> colRate;
    @FXML
    private TableColumn<Order,BigDecimal> colFilled;
    @FXML
    private TableView tradesTable;
    @FXML
    private TableColumn colTrades1;
    @FXML
    private TableColumn colTrades2;

}
