package View;

//import Model.Market.Market;
//import Model.Market.MarketDirection;
//import Model.User.OfferMarket;
//import Model.User.User;
import Model.Marketplace.Base.Market.Market;
import Model.Marketplace.Base.Market.Offer;
import javafx.application.Platform;
import javafx.beans.property.ReadOnlyObjectWrapper;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.transformation.SortedList;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.util.Callback;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created by FlyingPirate on 18.01.2017.
 */
public class DirectionViewController extends SpinnerController {

    private static Logger log = LogManager.getLogger(DirectionViewController.class.getName());

//    private User user;

    public Button saveButton;

    private boolean isAsk;
    private Market market;
    private Market oppMarket;

    private double rateCapAsk;
    private double rateExpert;
    private double rateCapBid;

    private class MyTableCellFactory<S, T> implements Callback<TableColumn<Offer, T>, TableCell<Offer, T>> {

        @Override
        public TableCell<Offer, T> call(TableColumn<Offer, T> param) {
            return new MyTableCell<Offer, T>();
        }
    }

    private class MyTableCell<S, T> extends TableCell<Offer, T> {
        @Override
        protected void updateItem(T item, boolean empty) {
            super.updateItem(item, empty);

            setText(empty ? "" : item.toString());
            TableRow<Offer> tableRow = getTableRow();
            Offer offer = tableRow.getItem();

            if (!isEmpty() && offer != null) {
                if (getTableColumn().getId().equals("offersIdCol")) {
                    StringBuilder rowStyle = new StringBuilder();

//                    if (offer.isPersonal()) {
//                        rowStyle.append(isAsk ? "-fx-background-color:#EF9A9A;" :
//                                                "-fx-background-color:#C5E1A5;");
//                    }

                    tableRow.setStyle(rowStyle.toString());
                }

                StringBuilder cellStyle = new StringBuilder();
                double rate = offer.getRate().doubleValue();

                if (isAsk && rate < rateCapBid ||
                   !isAsk && rate > rateCapAsk) {
                    cellStyle.append("-fx-font-weight:bold;");
                }

                if (rate > rateExpert && rate < rateCapAsk) {
                    cellStyle.append("-fx-text-fill:#0288D1;");
                }

                if (rate < rateExpert && rate > rateCapBid) {
                    cellStyle.append("-fx-text-fill:#311B92;");
                }

                setStyle(cellStyle.toString());
            }
        }
    }

    @FXML
    public void initialize() {
//        user = EnvAccess.env.user;

        offersIdCol.setCellValueFactory(new PropertyValueFactory<>("id"));
        offersAmountCol.setCellValueFactory(new PropertyValueFactory<>("amount"));
        offersRateCol.setCellValueFactory(new PropertyValueFactory<>("rate"));
        offersAllamountinCol.setCellValueFactory(new PropertyValueFactory<>("allAmountIn"));

//        offersIdCol.setCellFactory(new MyTableCellFactory<>());
//        offersAmountCol.setCellFactory(new MyTableCellFactory<>());
//        offersRateCol.setCellFactory(new MyTableCellFactory<>());
//        offersAllamountinCol.setCellFactory(new MyTableCellFactory<>());

        userOffersNumCol.setCellValueFactory(p -> new ReadOnlyObjectWrapper(userOffersTable.getItems().indexOf(p.getValue()) + ""));
        userOffersIdCol.setCellValueFactory(new PropertyValueFactory<>("id"));
        userOffersAmountInCol.setCellValueFactory(new PropertyValueFactory<>("amount"));
        userOffersOutinrateCol.setCellValueFactory(new PropertyValueFactory<>("rate"));
        userOffersDateCol.setCellValueFactory(new PropertyValueFactory<>("queryDate"));

        setSpinner(slicePlastSpinner, 0, 999999999, 0, 1);
        setSpinner(sliceCapSpinner, 0, 999999999, 0, 0.01);
        setSpinner(sliceSizeSpinner, 0, 999999999, 0, 1);
        setSpinner(bulkPlastSpinner, 0, 999999999, 0, 1);
        setSpinner(bulkCapSpinner, 0, 999999999, 0, 0.01);
        setSpinner(bulkSizeSpinner, 0, 999999999, 0, 1);

        saveButton.setDisable(true);
    }

    void setViewAskDirection(boolean isAsk) {
        this.isAsk = isAsk;
        String amount = isAsk ? "amountIn" : "amountOut";
        double min    = isAsk ? 1 : 0;
        double max    = isAsk ? 99999 : 1;


        userOffersAmountInCol.setCellValueFactory(new PropertyValueFactory<>(amount));
        offersAmountCol.setCellValueFactory(new PropertyValueFactory<>(amount));
        userOffersAmountInCol.setText(amount);
        offersAmountCol.setText(amount);

        setSpinner(sliceMultSpinner, min, max, 1, 0.001);
        setSpinner(bulkMultSpinner,  min, max, 1, 0.001);
    }

    void setMarkets(Market market, Market oppMarket) {

        this.market = market;
        this.oppMarket = oppMarket;
        Platform.runLater(() -> {
            directionLabel.setText(market.direction.toString());
            loadSpinners();
            loadOffers();
        });


//        userOffersTable.setItems(user.getOffersListInTrade(market.getDirection()));

//        checkBoxBuy.setSelected(user.preferencesUser.getAllowGrabDumb(market.getDirection()));
    }

    void loadSpinners() {
//        MarketDirection marketDirection = market.getDirection();
//
//        slicePlastSpinner.getValueFactory().setValue(user.preferencesUser.getPlast(marketDirection, OfferMarket.Type.SLICE));
//        sliceCapSpinner  .getValueFactory().setValue(user.preferencesUser.getCap  (marketDirection, OfferMarket.Type.SLICE));
//        sliceMultSpinner .getValueFactory().setValue(user.preferencesUser.getMult (marketDirection, OfferMarket.Type.SLICE));
//        sliceSizeSpinner .getValueFactory().setValue(user.preferencesUser.getSize (marketDirection, OfferMarket.Type.SLICE));
//
//        bulkPlastSpinner.getValueFactory().setValue(user.preferencesUser.getPlast(marketDirection, OfferMarket.Type.BULK));
//        bulkCapSpinner  .getValueFactory().setValue(user.preferencesUser.getCap  (marketDirection, OfferMarket.Type.BULK));
//        bulkMultSpinner .getValueFactory().setValue(user.preferencesUser.getMult (marketDirection, OfferMarket.Type.BULK));
//        bulkSizeSpinner .getValueFactory().setValue(user.preferencesUser.getSize (marketDirection, OfferMarket.Type.BULK));
    }

    void loadOffers() {
        ObservableList<Offer> items = FXCollections.observableArrayList(market.offers);
//        if (market.direction.ask) {
//            Collections.reverse(items);//todo???
//        }
        offersTable.setItems(items);
        if (market.direction.ask) {
            offersTable.scrollTo(items.size() - 1);
        }
    }

    public void setRates(double rateCapAsk, double rateExpert, double rateCapBid) {
        this.rateCapAsk = rateCapAsk;
        this.rateExpert = rateExpert;
        this.rateCapBid = rateCapBid;
    }

    void refreshTables() {
        Platform.runLater(() -> {
//           log.fatal(market.offers.size());
//            loadOffers();
//            System.out.println();
//            log.fatal(market.offers.size());
//            log.fatal(offersTable.getItems().size());
//            log.fatal(System.identityHashCode(items));
//            log.fatal(System.identityHashCode(market.offers));
//            log.fatal(market.offers.size());
//            if(items!= null) {
//                offersTable.setItems(FXCollections.observableArrayList(market.offers));
//                items = null;
//            }
//            log.fatal(items.size());
//            log.fatal(items2.size());
//            offersTable.refresh();
//            userOffersTable.refresh();
        });
    }

    //////////////////////////////////////////////////
    // On clicks.

    @FXML
    public void onClickMy() {
//        user.loadPersonalList();
    }

    @FXML
    public void onClickAfloat() {
//        user.strategy.modify(market.getDirection());
    }

    @FXML
    public void onClickAppend() {
//        user.strategy.merge(market.getDirection());
    }

    @FXML
    public void onClickCut() {
//        user.strategy.split(market.getDirection());
    }

    @FXML
    public void onClickBuy() {
//        user.strategy.buy(market.getDirection(), oppMarket.getDirection());
    }

    @FXML
    public void onClickSave() {
//        MarketDirection marketDirection = market.getDirection();
//
//        user.preferencesUser.setPlast(marketDirection, OfferMarket.Type.SLICE, slicePlastSpinner.getValue());
//        user.preferencesUser.setCap  (marketDirection, OfferMarket.Type.SLICE, sliceCapSpinner  .getValue());
//        user.preferencesUser.setMult (marketDirection, OfferMarket.Type.SLICE, sliceMultSpinner .getValue());
//        user.preferencesUser.setSize (marketDirection, OfferMarket.Type.SLICE, sliceSizeSpinner .getValue());
//
//        user.preferencesUser.setPlast(marketDirection, OfferMarket.Type.BULK, bulkPlastSpinner.getValue());
//        user.preferencesUser.setCap  (marketDirection, OfferMarket.Type.BULK, bulkCapSpinner  .getValue());
//        user.preferencesUser.setMult (marketDirection, OfferMarket.Type.BULK, bulkMultSpinner .getValue());
//        user.preferencesUser.setSize (marketDirection, OfferMarket.Type.BULK, bulkSizeSpinner .getValue());
//
//        saveButton.setDisable(true);
    }

    @FXML
    public void onClickCheckBoxBuy() {
//        user.preferencesUser.setAllowGrabDumb(market.getDirection(), checkBoxBuy.isSelected());
    }

    @FXML
    public void onClickHistory() {
//        user.logHistory(market.getDirection());
    }

    @Override
    void onSpinnerChange(Spinner spinner) {
        commitEditorText(spinner);
        saveButton.setDisable(false);
    }

    @FXML
    private Spinner<Double> slicePlastSpinner;
    @FXML
    private Spinner<Double> sliceCapSpinner;
    @FXML
    private Spinner<Double> sliceMultSpinner;
    @FXML
    private Spinner<Double> sliceSizeSpinner;

    @FXML
    private Spinner<Double> bulkPlastSpinner;
    @FXML
    private Spinner<Double> bulkCapSpinner;
    @FXML
    private Spinner<Double> bulkMultSpinner;
    @FXML
    private Spinner<Double> bulkSizeSpinner;

    @FXML
    private Label directionLabel;

    @FXML
    private TableView<Offer> userOffersTable;
    @FXML
    private TableView<Offer> offersTable;

    @FXML
    private TableColumn<Offer, String> offersIdCol;
    @FXML
    private TableColumn<Offer, BigDecimal> offersAmountCol;
    @FXML
    private TableColumn<Offer, BigDecimal> offersRateCol;
    @FXML
    private TableColumn<Offer, Double> offersAllamountinCol;

    @FXML
    private TableColumn<Offer, String> userOffersNumCol;
    @FXML
    private TableColumn<Offer, String> userOffersIdCol;
    @FXML
    private TableColumn<Offer, Double> userOffersAmountInCol;
    @FXML
    private TableColumn<Offer, Double> userOffersOutinrateCol;
    @FXML
    private TableColumn<Offer, String> userOffersDateCol;

    @FXML
    public CheckBox checkBoxBuy;
}
