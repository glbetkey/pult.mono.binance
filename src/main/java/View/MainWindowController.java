package View;

import Application.Env;
import Application.Events.EventListener;
import Application.Events.EventListenerAsync;
import Application.Preferences.PreferencesApp;
//import Application.Preferences.PreferencesData;
//import Model.External.Telegram.BotController;
//import Model.Market.*;
//import Model.Purse.PursesManager;
//import Model.Rate.RateSourcePreset;
//import Model.Stats.CapitalizationFormat;
//import Model.Stats.SessionDirectionFormat;
import Model.Marketplace.Base.Connector;
import Model.Marketplace.Base.Currency.Direction;
import Model.Marketplace.Base.Currency.DirectionPair;
import Model.Marketplace.Base.Market.Market;
import Model.Marketplace.Base.Marketplace;
import Model.Marketplace.MarketplacesManager;
import Model.Stats.SessionsManager;
//import Model.User.OfferMarket;
//import Model.User.SafeRateInfo;
//import Model.User.User;
//import Model.User.UserTick;
import javafx.application.Platform;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.layout.FlowPane;
import javafx.scene.text.Text;
import javafx.stage.Modality;
import javafx.stage.Stage;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.*;
import java.util.Arrays;
import java.util.Comparator;
import java.util.Iterator;
import java.util.Set;

/**
 * Created by FlyingPirate on 22.12.2016.
 */
public class MainWindowController extends SpinnerController implements EventListener {

    private static Logger log = LogManager.getLogger(MainWindowController.class.getName());

    public Env env;
    private PreferencesApp preferencesApp;
//    private PursesManager pursesManager;
//    private User user;
    private SessionsManager sessionsManager;
    private MarketplacesManager marketplacesManager;
//    private BotController telegramBotController;

    static final private int radioRateButtonCount = 3;

    public ListView listViewLogBrief;
    public ListView listViewLogVerbose;

    public Label trueAverLabel;
    public Label askCapRateLabel;
    public Label bidCapRateLabel;
    public Spinner<Double> askNetCostSpinner;
    public Spinner<Double> askMarginSpinner;
    public Spinner<Double> bidNetCostSpinner;
    public Spinner<Double> bidMarginSpinner;

    public static final int DEFAULT_CENTRAL_PLAST = 1000;
    public Label trueAverLabelAsk;
    public Label trueAverLabelBid;
    public Label askSliceRateLabel;
    public Label bidSliceRateLabel;
    public Label askCapDiffLabel;
    public Label bidCapDiffLabel;
    public Label askSliceDiffLabel;
    public Label bidSliceDiffLabel;
    public RadioButton radioRate1;
    public RadioButton radioRate2;
    public RadioButton radioRate3;
    private RadioButton radioButtonsRates[];
    private String radioButtonTextsRates[];
//    private RateSourcePreset radioButtonRateSourcePresets[];
    public Label askSliceDiffLabel1;
    public Label askCapDiffLabel1;

    public Label deltaLabel1;
    public Label bidCapDiffLabel1;
    public Label bidSliceDiffLabel1;

//    public ButtonStartController buttonStartController;

    private Level[] loggingLevelsSorted;
    private Level loggingLevelBrief;
    private Level loggingLevelVerbose;

    //////////////////////////////////////////////////
    // Check menu items for logging levels.

    private MenuItemCheckLoggingLevel[] menuItemsCheckLoggingLevelBrief;
    private MenuItemCheckLoggingLevel[] menuItemsCheckLoggingLevelVerbose;

    private ChangesEventListener changesEventListener;


    public class ChangesEventListener extends  EventListenerAsync{
        @Override
        protected void listenAsync(Event event) {
            MainWindowController.this.listen(event);
        }
    }

    public enum MenuItemCheckLoggingLevelMode {
        BRIEF,
        VERBOSE;
    }

    private class MenuItemCheckLoggingLevel extends CheckMenuItem {

        MenuItemCheckLoggingLevelMode mode;

        MenuItemCheckLoggingLevel(String name, MenuItemCheckLoggingLevelMode mode) {
            super(name);
            this.mode = mode;
            setOnAction((event) -> select());
        }

        void select() {
            if (mode == MenuItemCheckLoggingLevelMode.BRIEF) {
                preferencesApp.setLoggingLevelBrief(getText());
            }
            if (mode == MenuItemCheckLoggingLevelMode.VERBOSE) {
                preferencesApp.setLoggingLevelVerbose(getText());
            }
        }
    }

    //////////////////////////////////////////////////
    // Initialization section.

    // One initialization function.
    @FXML
    public void initialize() {
        env = EnvAccess.env;
        env.onMainWindowControllerCreated(this);

        preferencesApp        = env.preferencesApp;
//        pursesManager         = env.pursesManager;
//        user                  = env.user;
        sessionsManager       = env.sessionsManager;
        marketplacesManager   = env.marketplacesManager;
//        telegramBotController = env.telegramBotController;


        changesEventListener = new ChangesEventListener();
        changesEventListener.start();
        marketplacesManager.addListener(changesEventListener);
        preferencesApp.addListener(this);

        loggingLevelsSorted = Level.values();
        Arrays.sort(loggingLevelsSorted, Comparator.<Level>comparingInt(level -> level.intLevel()));

        menuItemsCheckLoggingLevelBrief   = new MenuItemCheckLoggingLevel[loggingLevelsSorted.length];
        menuItemsCheckLoggingLevelVerbose = new MenuItemCheckLoggingLevel[loggingLevelsSorted.length];

        loggingLevelBrief   = preferencesApp.getLoggingBrief();
        loggingLevelVerbose = preferencesApp.getLoggingVerbose();

        int indexMenuItem = 0;
        for (Level level : loggingLevelsSorted) {
            MenuItemCheckLoggingLevel menuItemLevel;

            menuItemLevel = new MenuItemCheckLoggingLevel(level.name(), MenuItemCheckLoggingLevelMode.BRIEF);
            menuLoggingBrief.getItems().add(menuItemLevel);
            menuItemsCheckLoggingLevelBrief[indexMenuItem] = menuItemLevel;
            if (level.name().equals(loggingLevelBrief.name())) {
                menuItemLevel.setSelected(true);
            }

            menuItemLevel = new MenuItemCheckLoggingLevel(level.name(), MenuItemCheckLoggingLevelMode.VERBOSE);
            menuLoggingVerbose.getItems().add(menuItemLevel);
            menuItemsCheckLoggingLevelVerbose[indexMenuItem] = menuItemLevel;
            if (level.name().equals(loggingLevelVerbose.name())) {
                menuItemLevel.setSelected(true);
            }

            ++indexMenuItem;
        }

//        UserTick userTick = user.userTick;
//        buttonStartController = new ButtonStartController(userTick, telegramBotController, buttonStart, spinnerTimer);
//        userTick.onMainWindowControllerCreated(buttonStartController);

        // Setup market pair selection combo box.
//        marketPairComboBox.setItems(FXCollections.observableArrayList(MarketPair.values()));

        // Setup timer period edit box.
        spinnerTimer.setValueFactory(new SpinnerValueFactory.IntegerSpinnerValueFactory(30, Integer.MAX_VALUE, 60, 15));

        setSpinner(askNetCostSpinner, 0.8, 1.1, 1, 0.001);
        setSpinner(askMarginSpinner, 0.8, 1.1, 1.001, 0.001);
        setSpinner(bidNetCostSpinner, 0.8, 1.1, 1, 0.001);
        setSpinner(bidMarginSpinner, 0.8, 1.1, 1.001, 0.001);

        // Setup exchange rate radio button.
        radioButtonsRates = new RadioButton[] { radioRate1, radioRate2, radioRate3};
        radioButtonTextsRates = new String[radioRateButtonCount];

        for (int i = 0; i < radioRateButtonCount; ++i) {
            int j = i;
            radioButtonsRates[i].selectedProperty().addListener(new ChangeListener<Boolean>() {
                @Override
                public void changed(ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean isNowSelected) {
                    if (isNowSelected) {
//                        user.setCurrentRateSourcePreset(radioButtonRateSourcePresets[j]);
                    }
                }
            });
        }
//
//        columnCapitalizationCurrency .setCellValueFactory(new PropertyValueFactory<>("currency" ));
//        columnCapitalizationTotal    .setCellValueFactory(new PropertyValueFactory<>("total"    ));
//        columnCapitalizationDelim1   .setCellValueFactory(new PropertyValueFactory<>("delim1"   ));
//        columnCapitalizationPurse    .setCellValueFactory(new PropertyValueFactory<>("purse"    ));
//        columnCapitalizationDelim2   .setCellValueFactory(new PropertyValueFactory<>("delim2"   ));
//        columnCapitalizationOffers   .setCellValueFactory(new PropertyValueFactory<>("offers"   ));
//        columnCapitalizationDelim3   .setCellValueFactory(new PropertyValueFactory<>("delim3"   ));
//        columnCapitalizationCredit   .setCellValueFactory(new PropertyValueFactory<>("credit"   ));
//        columnCapitalizationDeltaText.setCellValueFactory(new PropertyValueFactory<>("deltaText"));
//        columnCapitalizationDelta    .setCellValueFactory(new PropertyValueFactory<>("delta"    ));
//        columnCapitalizationDelim4   .setCellValueFactory(new PropertyValueFactory<>("delim4"   ));
//        columnCapitalizationDeltaP   .setCellValueFactory(new PropertyValueFactory<>("deltaP"   ));
//        columnCapitalizationDeltaN   .setCellValueFactory(new PropertyValueFactory<>("deltaN"   ));
//
//        columnCapitalizationCurrency.setStyle("-fx-alignment:CENTER-RIGHT;");
//        columnCapitalizationTotal   .setStyle("-fx-alignment:CENTER-RIGHT;");
//        columnCapitalizationPurse   .setStyle("-fx-alignment:CENTER-RIGHT;");
//        columnCapitalizationOffers  .setStyle("-fx-alignment:CENTER-RIGHT;");
//        columnCapitalizationCredit  .setStyle("-fx-alignment:CENTER-RIGHT;");
//        columnCapitalizationDelta   .setStyle("-fx-alignment:CENTER-RIGHT;");
//        columnCapitalizationDeltaP  .setStyle("-fx-alignment:CENTER-RIGHT;");
//        columnCapitalizationDeltaN  .setStyle("-fx-alignment:CENTER-RIGHT;");
//
//        columnTurnoverBase     .setCellValueFactory(new PropertyValueFactory<>("amountBase" ));
//        columnTurnoverDirection.setCellValueFactory(new PropertyValueFactory<>("direction"  ));
//        columnTurnoverQuote    .setCellValueFactory(new PropertyValueFactory<>("amountQuote"));
//        columnTurnoverRate     .setCellValueFactory(new PropertyValueFactory<>("rate"       ));
//
//        columnTurnoverBase     .setStyle( "-fx-alignment: CENTER-RIGHT;");
//        columnTurnoverDirection.setStyle( "-fx-alignment: CENTER;      ");
//        columnTurnoverQuote    .setStyle( "-fx-alignment: CENTER-RIGHT;");
//        columnTurnoverRate     .setStyle( "-fx-alignment: CENTER-LEFT; ");

        initializeLog();

        askDirViewController.setViewAskDirection(true);
        bidDirViewController.setViewAskDirection(false);

        // Update everything based on default market pair.
//        marketPairComboBox.setValue(user.getCurrentMarketPair());
//        user.setCurrentMarketPair(user.getCurrentMarketPair());
    }

    //////////////////////////////////////////////////
    // On events.

    @Override
    public void listen(Event event) {
//        if (event instanceof PreferencesData.EventPreferenceChanged) {
//            Object name = ((PreferencesData.EventPreferenceChanged) event).name;
//            if (name instanceof PreferencesApp.Name) {
//                if (name == PreferencesApp.Name.LOGGINGLEVELBRIEF) {
//                    loggingLevelBrief = preferencesApp.getLoggingBrief();
//                    for (MenuItemCheckLoggingLevel menuItemCheckLoggingLevel : menuItemsCheckLoggingLevelBrief) {
//                        menuItemCheckLoggingLevel.setSelected(menuItemCheckLoggingLevel.getText().equals(loggingLevelBrief.name()));
//                    }
//                }
//                if (name == PreferencesApp.Name.LOGGINGLEVELVERBOSE) {
//                    loggingLevelVerbose = preferencesApp.getLoggingVerbose();
//                    for (MenuItemCheckLoggingLevel menuItemCheckLoggingLevel : menuItemsCheckLoggingLevelVerbose) {
//                        menuItemCheckLoggingLevel.setSelected(menuItemCheckLoggingLevel.getText().equals(loggingLevelVerbose.name()));
//                    }
//                }
//            }
//        }
        if (event instanceof Connector.EventMarketplaceInitialized) {
            Connector.EventMarketplaceInitialized eventTyped = (Connector.EventMarketplaceInitialized) event;
                Set<Direction> directions = marketplacesManager.marketplaces.get(eventTyped.marketplaceType).marketsManager.markets.keySet();
                if(directions.size()==2){
                    Iterator<Direction> it= directions.iterator();
                    Direction dir = it.next();
                    Direction askDir = null;
                    Direction bidDir = null;
                    if(dir.ask)
                        askDir = dir;
                    else
                        bidDir = dir;
                    if (askDir==null)
                        askDir = it.next();
                    else
                        bidDir = it.next();
                    onCurrentMarketPairChanged(null,null, askDir,bidDir);
                }
            log.fatal("INIT");
        }
        if (event instanceof Connector.EventMarketChanged) {
//            askDirViewController.refreshTables();
//            bidDirViewController.refreshTables();
//            log.fatal("CHANGE");
        }
    }

    //////////////////////////////////////////////////
    // On clicks.

    @FXML
    public void onClickStart() {
//        buttonStartController.onClick();
    }

    @FXML
    public void onClickReset() {
//        env.sessionsManager.reset();
//        updateTurnover();
    }

    @FXML
    public void onClickPurse() {
//        pursesManager.readPursesInfo();
    }

    public void onClickMarketPair() {
//        user.setCurrentMarketPair(marketPairComboBox.getValue());
    }

    @FXML
    public void onClickRefresh() {
//        user.refresh();
        // Bot start is linked to either refreshing or auto refreshing (on bitcoin market).
//        telegramBotController.start();
    }

    @FXML
    public void onClickDecide() {
//        user.strategy.decide();
    }

    @FXML
    public void onClickExecute() {
//        user.strategy.execute();
    }

    @FXML
    public void onClickSpread5() {
//        user.spreadAll();
    }

    @FXML
    public void onClickCollapse5() {
//        user.collapseAll();
    }

    @Override
    void onSpinnerChange(Spinner spinner) {
        commitEditorText(spinner);
        double value = (Double) spinner.getValue();
        if (spinner == askNetCostSpinner) {
//            user.preferencesUser.setNetCost(user.getMarketDirectionAsk().getOutCurrency(), value);
        } else if (spinner == bidNetCostSpinner) {
//            user.preferencesUser.setNetCost(user.getMarketDirectionBid().getOutCurrency(), value);
        } else if (spinner == askMarginSpinner) {
//            user.preferencesUser.setMargin(user.getCurrentMarketPair().getCurrencyPair(), value);
            bidMarginSpinner.getValueFactory().setValue(value);
        } else if (spinner == bidMarginSpinner) {
//            user.preferencesUser.setMargin(user.getCurrentMarketPair().getCurrencyPair(), value);
            askMarginSpinner.getValueFactory().setValue(value);
        }
    }

    //////////////////////////////////////////////////
    // Content update section.

    public void onCurrentMarketPairChanged(Direction oldDirectionAsk,Direction oldDirectionBid, Direction newDirectionAsk, Direction newDirectionBid) {


//        CurrencyPair newCurrencyPair = newDirectionPair.getCurrencyPair();

        //////////////////////////////////////////////////
        // Markets update section.
        Market ask = env.marketplacesManager.marketplaces.get(Marketplace.Type.BINANCE).marketsManager.markets.get(newDirectionAsk);
        Market bid = env.marketplacesManager.marketplaces.get(Marketplace.Type.BINANCE).marketsManager.markets.get(newDirectionBid);
        askDirViewController.setMarkets(ask, bid);
        bidDirViewController.setMarkets(bid, ask);

        //////////////////////////////////////////////////
        // Update radio buttons' properties and bindings.

        for (int i = 0; i < radioRateButtonCount; ++i) {
            radioButtonsRates[i].setSelected(false);
        }

//        RateSourcePreset[] rateSourcePresetsCapitalization = newDirectionPair.getRateSourcePresetsCapitalization();
//        int radioRateButtonActiveCount = Math.min(rateSourcePresetsCapitalization.length, radioRateButtonCount);
//
//        RateSourcePreset rateSourcePresetPref = user.preferencesUser.getRateSourcePreset(newCurrencyPair);
//        boolean radioSelected = false;

//        for (int i = 0; i < radioRateButtonCount; ++i) {
//            radioButtonsRates[i].setVisible(i < radioRateButtonActiveCount);
//            if (i < radioRateButtonActiveCount) {
//                RateSourcePreset rateSourcePresetCapitalization = rateSourcePresetsCapitalization[i];
//                radioButtonTextsRates[i] = rateSourcePresetCapitalization.getNameShort() + " %.4g";
//                radioButtonRateSourcePresets = rateSourcePresetsCapitalization;
//                if (rateSourcePresetPref == rateSourcePresetsCapitalization[i]) {
//                    radioButtonsRates[i].setSelected(true);
//                    radioSelected = true;
//                }
//            }
//        }

//        if (!radioSelected && radioRateButtonActiveCount > 0) {
//            radioButtonsRates[0].setSelected(true);
//        }

        //////////////////////////////////////////////////
        // Reset spinner values from preferences.

//        askMarginSpinner .getValueFactory().setValue(user.preferencesUser.getMargin(newCurrencyPair));
//        bidMarginSpinner .getValueFactory().setValue(user.preferencesUser.getMargin(newCurrencyPair));
//        askNetCostSpinner.getValueFactory().setValue(user.preferencesUser.getNetCost(newDirectionPair.getMdAsk().getOutCurrency()));
//        bidNetCostSpinner.getValueFactory().setValue(user.preferencesUser.getNetCost(newDirectionPair.getMdBid().getOutCurrency()));

        //////////////////////////////////////////////////
        // Update turnover panel.

        updateTurnover();

        //////////////////////////////////////////////////
        // Update central information panel and capitalization panel.

        updateLabels();
        updateCapitalization();
    }

//    public void onCurrentRateSourcePresetChanged(RateSourcePreset oldRateSourcePreset, RateSourcePreset newRateSourcePreset) {
//        updateLabels();
//        updateCapitalization();
//    }

    public void onUserRefresh() {
        updateTurnover();

        Platform.runLater(() -> {
            updateLabels();
            updateCapitalization();
            askDirViewController.loadOffers();
            bidDirViewController.loadOffers();
            askDirViewController.refreshTables();
            bidDirViewController.refreshTables();
        });
    }

    // Update data that feeds into turnover table.
    private void updateTurnover() {
//        SessionDirectionFormat [] sessionDirectionFormatsLifetime = SessionDirectionFormat.constructAll(sessionsManager.getSessionLifetime(), user.getCurrentMarketPair().getCurrencyPair());
//        if (sessionDirectionFormatsLifetime != null) {
//            tableTurnover.setItems(FXCollections.observableArrayList(sessionDirectionFormatsLifetime));
//        }
    }

    // Update all labels in the central information panel.
    private void updateLabels() {
//        SafeRateInfo safeRateInfo = user.getSafeRate();
//        if (safeRateInfo.isValid()) {
//            double rateMid = safeRateInfo.rateExpertCorrected;
//            double rateAskCap = user.calcAskCapRate(rateMid);
//            double rateBidCap = user.calcBidCapRate(rateMid);
//
//            // Update rates displayed next to radio buttons.
//            MarketPair marketPair = user.getCurrentMarketPair();
//            RateSourcePreset[] rateSourcePresetsCapitalization = marketPair.getRateSourcePresetsCapitalization();
//            int radioRateButtonActiveCount = Math.min(rateSourcePresetsCapitalization.length, radioRateButtonCount);
//            for (int i = 0; i < radioRateButtonActiveCount; ++i) {
//                radioButtonsRates[i].setText(String.format(radioButtonTextsRates[i], env.rateSourceLibrary.getRate0(marketPair.getCurrencyPair(), marketPair.getRateSourcePresetsCapitalization()[i], false)));
//            }
//
//            // Update displayed expert correction value.
//            labelExpertCorrectionAuto.setText(String.format("%.3f", safeRateInfo.expertCorrection));
//
//            // Update all other displayed values around central spinners.
//            double delta = user.getMarketAsk().getAverageRate(DEFAULT_CENTRAL_PLAST)
//                         - user.getMarketBid().getAverageRate(DEFAULT_CENTRAL_PLAST);
//            deltaLabel .setText(String.format("%+.4f", delta));
//            deltaLabel1.setText(String.format("%+.4f%%", delta / rateMid * 100));
//
//            trueAverLabel   .setText(String.format("%.4f", rateMid));
//            trueAverLabelAsk.setText(String.format("%.4f", rateMid));
//            trueAverLabelBid.setText(String.format("%.4f", rateMid));
//
//            askCapRateLabel .setText(String.format("%.3f", rateAskCap));
//            bidCapRateLabel .setText(String.format("%.3f", rateBidCap));
//            askCapDiffLabel .setText(String.format("+%.3f", rateAskCap - rateMid));
//            bidCapDiffLabel .setText(String.format( "%.3f", rateBidCap - rateMid));
//            askCapDiffLabel1.setText(String.format("+%.3f%%", 100 * (rateAskCap - rateMid) / rateMid));
//            bidCapDiffLabel1.setText(String.format( "%.3f%%", 100 * (rateBidCap - rateMid) / rateMid));
//
//            OfferMarket askSlice = user.findFirstUserOfferByType(user.getMarketDirectionAsk(), OfferMarket.Type.SLICE);
//            OfferMarket bidSlice = user.findFirstUserOfferByType(user.getMarketDirectionBid(), OfferMarket.Type.SLICE);
//            askSliceRateLabel .setText(askSlice == null ? "NONE" : String.format("%.4f", askSlice.getRate()));
//            bidSliceRateLabel .setText(bidSlice == null ? "NONE" : String.format("%.4f", bidSlice.getRate()));
//            askSliceDiffLabel .setText(askSlice == null ? "NONE" : String.format((askSlice.getRateD() - rateAskCap > 0 ? "+" : "") + "%.3f",          askSlice.getRateD() - rateAskCap));
//            bidSliceDiffLabel .setText(bidSlice == null ? "NONE" : String.format((bidSlice.getRateD() - rateBidCap > 0 ? "+" : "") + "%.3f",          bidSlice.getRateD() - rateBidCap));
//            askSliceDiffLabel1.setText(askSlice == null ? "NONE" : String.format((askSlice.getRateD() - rateAskCap > 0 ? "+" : "") + "%.3f%%", 100 * (askSlice.getRateD() - rateAskCap) / rateMid));
//            bidSliceDiffLabel1.setText(bidSlice == null ? "NONE" : String.format((bidSlice.getRateD() - rateBidCap > 0 ? "+" : "") + "%.3f%%", 100 * (bidSlice.getRateD() - rateBidCap) / rateMid));
//        }
    }

    private void updateCapitalization() {
//        CapitalizationFormat[] capitalizationFormats = CapitalizationFormat.constructAll(user.getCapitalization(), sessionsManager.getSessionH());
//        if (capitalizationFormats != null) {
//            tableCapitalization.setItems(FXCollections.observableArrayList(capitalizationFormats));
//        }
    }

    //////////////////////////////////////////////////
    // Controls interaction

    public void loadDirectionSpinnerValues() {
        askDirViewController.loadSpinners();
        bidDirViewController.loadSpinners();
    }

    //////////////////////////////////////////////////
    // Preferences menu section.

    public void showModalRelations() {
//        try {
//            RelationsControllerLaunch.start("../View/RelationsView.fxml", user.expertCorrectionAuto);
//        } catch (IOException e) {
//            log.error(String.format(Literals.logErrorControllerFailedToLoad, Literals.logControllerRelationsView));
//            e.printStackTrace();
//        }
    }

    public void showModalOrbits() {
//        try {
//            OrbitsControllerLaunch.start("../View/OrbitsView.fxml", user.preferencesUser);
//        } catch (IOException e) {
//            log.error(String.format(Literals.logErrorControllerFailedToLoad, Literals.logControllerOrbitsView));
//            e.printStackTrace();
//        }
    }

    public void showModalPreferences(ActionEvent actionEvent) throws IOException {
        Stage dialog = new Stage();
        Parent root = FXMLLoader.load(getClass().getResource("/View/TestSomethingView.fxml"));
        dialog.setScene(new Scene(root));
        dialog.initModality(Modality.APPLICATION_MODAL);
        dialog.showAndWait();
    }

    //////////////////////////////////////////////////
    // FXML section.

    @FXML
    private Label labelExpertCorrectionAuto;

    @FXML
    private ComboBox<DirectionPair> marketPairComboBox;
    @FXML
    private Button buttonStart;
    @FXML
    private Spinner<Integer> spinnerTimer;


    @FXML
    public DirectionViewController askDirViewController;
    @FXML
    public DirectionViewController bidDirViewController;

//    @FXML
//    private TableView<CapitalizationFormat> tableCapitalization;
//    @FXML
//    private TableColumn<CapitalizationFormat, String> columnCapitalizationCurrency;
//    @FXML
//    private TableColumn<CapitalizationFormat, String> columnCapitalizationTotal;
//    @FXML
//    private TableColumn<CapitalizationFormat, String> columnCapitalizationDelim1;
//    @FXML
//    private TableColumn<CapitalizationFormat, String> columnCapitalizationPurse;
//    @FXML
//    private TableColumn<CapitalizationFormat, String> columnCapitalizationDelim2;
//    @FXML
//    private TableColumn<CapitalizationFormat, String> columnCapitalizationOffers;
//    @FXML
//    private TableColumn<CapitalizationFormat, String> columnCapitalizationDelim3;
//    @FXML
//    private TableColumn<CapitalizationFormat, String> columnCapitalizationCredit;
//    @FXML
//    private TableColumn<CapitalizationFormat, String> columnCapitalizationDeltaText;
//    @FXML
//    private TableColumn<CapitalizationFormat, String> columnCapitalizationDelta;
//    @FXML
//    private TableColumn<CapitalizationFormat, String> columnCapitalizationDelim4;
//    @FXML
//    private TableColumn<CapitalizationFormat, String> columnCapitalizationDeltaP;
//    @FXML
//    private TableColumn<CapitalizationFormat, String> columnCapitalizationDeltaN;

//    @FXML
//    private TableView<SessionDirectionFormat> tableTurnover;
//    @FXML
//    private TableColumn<SessionDirectionFormat, String> columnTurnoverBase;
//    @FXML
//    private TableColumn<SessionDirectionFormat, String> columnTurnoverDirection;
//    @FXML
//    private TableColumn<SessionDirectionFormat, String> columnTurnoverQuote;
//    @FXML
//    private TableColumn<SessionDirectionFormat, String> columnTurnoverRate;

    @FXML
    private Label deltaLabel;

    @FXML
    private Menu menuLoggingBrief;
    @FXML
    private Menu menuLoggingVerbose;

    //////////////////////////////////////////////////
    // Logger section.

    public class LogStringCell extends ListCell<String> {

        @Override
        protected void updateItem(String string, boolean empty) {
            super.updateItem(string, empty);
            if (string != null && !isEmpty()) {
                setGraphic(createAssembledFlowPane(string));
            } else {
                setGraphic(null);
                setText(null);
            }
        }

        /* Erzeuge ein FlowPane mit gefüllten Textbausteien */
        private FlowPane createAssembledFlowPane(String... messageTokens) {
            FlowPane flow = new FlowPane();
            for (String token : messageTokens) {
                Text text = new Text(token);

                if (text.toString().contains(" TRACE ")) {
                    text.setStyle("-fx-fill: #0000FF");
                }
                if (text.toString().contains(" ALL ")) {
                    text.setStyle("-fx-fill: #FF00FF");
                }
                if (text.toString().contains(" ERROR ")) {
                    text.setStyle("-fx-fill: #FF8080");
                }
                if (text.toString().contains(" INFO ")) {
                    text.setStyle("-fx-fill: #1B5E20");
                }
                if (text.toString().contains(" FATAL ")) {
                    text.setStyle("-fx-fill: #FF0000");
                }
                if (text.toString().contains(" DEBUG ")) {
                    text.setStyle("-fx-fill: #000000");
                }
                if (text.toString().contains(" OFF ")) {
                    text.setStyle("-fx-fill: #8040FF");
                }
                if (text.toString().contains(" WARN ")) {
                    text.setStyle("-fx-fill: #FF8000");
                }

                flow.getChildren().add(text);
            }
            return flow;
        }
    }

    void initializeLog() {
//        listViewLogBrief  .setCellFactory((listView) -> { return new LogStringCell(); });
//        listViewLogVerbose.setCellFactory((listView) -> { return new LogStringCell(); });
//
//        LoggerContext loggerContext = (LoggerContext) LogManager.getContext(false);
//        Configuration loggerConfiguration = loggerContext.getConfiguration();
//        LoggerConfig loggerConfig = loggerConfiguration.getLoggerConfig(LogManager.ROOT_LOGGER_NAME);
//        loggerConfig.setLevel(Level.ALL);
//        loggerContext.updateLoggers(); // übernehme aktuellen LogLevel
//
//        try {
//            PipedOutputStream pOut = new PipedOutputStream();
//            BufferedReader reader = new BufferedReader(new InputStreamReader(new PipedInputStream(pOut)));
//            System.setOut(new PrintStream(pOut));
//
//            Task<Void> task = new Task<Void>() {
//                @Override
//                protected Void call() throws Exception {
//                    while (!isCancelled()) {
//                        try {
//                            String line = reader.readLine();
//                            if (line != null) {
//                                for (Level level : loggingLevelsSorted) {
//                                    if (line.contains(level.toString())) {
//                                        Platform.runLater(() -> {
//                                            if (loggingLevelBrief  .intLevel() >= level.intLevel()) {
//                                                listViewLogBrief.getItems().add(line);
//                                            }
//                                            if (loggingLevelVerbose.intLevel() >= level.intLevel()) {
//                                                listViewLogVerbose.getItems().add(line);
//                                            }
//                                        });
//                                        if (level.intLevel() <= Level.ERROR.intLevel()) {
////                                            telegramBotController.sendMessage(line);
//                                        }
//                                        break;
//                                    }
//                                }
//                            }
//                        } catch (IOException e) {
//                            e.printStackTrace();
//                        }
//                    }
//
//                    return null;
//                }
//            };
//
//            Thread thread = new Thread(task);
//            thread.setDaemon(true);
//            thread.start();
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
    }
}