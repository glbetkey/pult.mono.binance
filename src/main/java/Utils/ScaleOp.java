package Utils;

import java.math.BigDecimal;
import java.math.RoundingMode;

public class ScaleOp {

    public final int scale;

    private static final int scaleDefault = 8;

    public static final RoundingMode roundingModeDefault = RoundingMode.HALF_EVEN;

    public ScaleOp() {
        this.scale = scaleDefault;
    }

    public ScaleOp(int scale) {
        this.scale = scale;
    }

    public ScaleOp min(ScaleOp other) {
        return (scale <= other.scale) ? this : other;
    }

    public ScaleOp max(ScaleOp other) {
        return (scale >= other.scale) ? this : other;
    }

    public ScaleOp min(int scale) {
        return (this.scale <= scale) ? this : new ScaleOp(scale);
    }

    public ScaleOp max(int scale) {
        return (this.scale >= scale) ? this : new ScaleOp(scale);
    }

    public BigDecimal set(BigDecimal amount) {
        return amount.setScale(scale, roundingModeDefault);
    }

    public BigDecimal set(BigDecimal amount, RoundingMode roundingMode) {
        return amount.setScale(scale, roundingMode);
    }

    public BigDecimal mul(BigDecimal multiplicand1, BigDecimal multiplicand2, RoundingMode roundingMode) {
        return multiplicand1.multiply(multiplicand2).setScale(scale, roundingMode);
    }

    public BigDecimal mul(BigDecimal multiplicand1, BigDecimal multiplicand2) {
        return multiplicand1.multiply(multiplicand2).setScale(scale, roundingModeDefault);
    }

    public BigDecimal div(BigDecimal dividend, BigDecimal divisor) {
        return dividend.divide(divisor, scale, roundingModeDefault);
    }

    public BigDecimal div(BigDecimal dividend, BigDecimal divisor, RoundingMode roundingMode) {
        return dividend.divide(divisor, scale, roundingMode);
    }
}
