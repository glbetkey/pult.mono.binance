package Utils;

import java.math.BigDecimal;
import java.math.RoundingMode;

public class Format {
    public static String toSignedString(BigDecimal arg) {
        return (arg.signum() >= 0) ? ('+' + arg.toPlainString()) : arg.toPlainString();
    }

    public static String toPercentage(BigDecimal dividend, BigDecimal divisor, int scale, RoundingMode roundingMode) {
        return (divisor.signum() == 0) ? "-" : dividend.movePointLeft(2).divide(divisor, scale, roundingMode).toPlainString();
    }

    public static String toPercentage(BigDecimal dividend, BigDecimal divisor) {
        return (divisor.signum() == 0) ? "-" : dividend.movePointRight(2).divide(divisor, 0, RoundingMode.FLOOR).toPlainString();
    }
}
