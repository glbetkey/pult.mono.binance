package Utils;

import java.math.BigDecimal;

public class TableMono {

    private static final String formatPercentage = "%s%%";

    private static class Cell {

        public CellStyle style;
        public Object data;
    }

    private static class CellRenderParams {

        public String text;
        public int width;

        CellRenderParams(String text, int width) {
            this.text  = text;
            this.width = width;
        }
    }

    private static class CellRenderParamsBigDecimal extends CellRenderParams {

        public int intWidth;
        public int dotWidth;

        public CellRenderParamsBigDecimal(String text, int width, int intWidth, int dotWidth) {
            super(text, width);
            this.intWidth = intWidth;
            this.dotWidth = dotWidth;
        }
    }

    private static class ColRenderParams {

        public int width;
        public int bigDecimalWidth;
        public int bigDecimalIntWidth;
        public int bigDecimalDotWidth;

        public ColRenderParams(int width, int bigDecimalWidth, int bigDecimalIntWidth, int bigDecimalDotWidth) {
            this.width              = width;
            this.bigDecimalWidth    = bigDecimalWidth;
            this.bigDecimalIntWidth = bigDecimalIntWidth;
            this.bigDecimalDotWidth = bigDecimalDotWidth;
        }
    }

    public static class CellStyle {

        public enum Type {
            STRING,
            BIGDECIMAL,
            PERCENTAGE
        }

        public static class TypeParamsBigDecimal {
            public boolean alwaysSigned;

            public TypeParamsBigDecimal(boolean alwaysSigned) {
                this.alwaysSigned = alwaysSigned;
            }
        }

        public enum Align {
            LEFT,
            RIGHT
        }

        public Type   type;
        public Object typeParams;
        public Align  align;

        public CellStyle(Type type, Object typeParams, Align align) {
            this.type       = type;
            this.typeParams = typeParams;
            this.align      = align;
        }
    }

    private int rowCount;
    private int colCount;
    private Cell[][] cells;

    public TableMono(int rowCount, int colCount) {
        this.rowCount = rowCount;
        this.colCount = colCount;
        cells = new Cell[rowCount][colCount];
        for (int rowIndex = 0; rowIndex < rowCount; rowIndex++) {
            for (int colIndex = 0; colIndex < colCount; colIndex++) {
                cells[rowIndex][colIndex] = new Cell();
            }
        }
    }

    public void setStyle(int rowIndex, int colIndex, CellStyle style) {
        if (rowIndex < rowCount && colIndex < colCount) {
            cells[rowIndex][colIndex].style = style;
        }
    }

    public void setData(int rowIndex, int colIndex, Object data) {
        if (rowIndex < rowCount && colIndex < colCount) {
            cells[rowIndex][colIndex].data = data;
        }
    }

    public void setCell(int rowIndex, int colIndex, CellStyle style, Object data) {
        if (rowIndex < rowCount && colIndex < colCount) {
            cells[rowIndex][colIndex].style = style;
            cells[rowIndex][colIndex].data  = data;
        }
    }

    public String toString() {
        int rowIndex;
        int colIndex;

        ColRenderParams[] colsRenderParams = new ColRenderParams[colCount];
        CellRenderParams[][] cellsRenderParams = new CellRenderParams[rowCount][colCount];

        for (colIndex = 0; colIndex < colCount; ++colIndex) {
            int colWidth = 0;

            int colBigDecimalIntWidth = 0;
            int colBigDecimalDotWidth = 0;

            for (rowIndex = 0; rowIndex < rowCount; ++rowIndex) {
                Cell cell = cells[rowIndex][colIndex];
                int cellWidth = 0;
                try {
                    switch (cell.style.type) {
                        case STRING: {
                            String text = (String) cell.data;
                            cellWidth = text.length();
                            cellsRenderParams[rowIndex][colIndex] = new CellRenderParams(text, cellWidth);
                        }
                        break;
                        case BIGDECIMAL: {
                            BigDecimal data = (BigDecimal) cell.data;
                            CellStyle.TypeParamsBigDecimal typeParams = (CellStyle.TypeParamsBigDecimal) cell.style.typeParams;
                            int cellBigDecimalIntWidth = data.precision() - data.scale();
                            int cellBigDecimalDotWidth =                    data.scale();
                            if (typeParams.alwaysSigned || data.signum() < 0) {
                                ++cellBigDecimalIntWidth;
                            }
                            colBigDecimalIntWidth = Math.max(colBigDecimalIntWidth, cellBigDecimalIntWidth);
                            colBigDecimalDotWidth = Math.max(colBigDecimalDotWidth, cellBigDecimalDotWidth);
                            cellsRenderParams[rowIndex][colIndex] = new CellRenderParamsBigDecimal(typeParams.alwaysSigned ? Format.toSignedString(data) : data.toPlainString(), cellBigDecimalIntWidth + 1 + cellBigDecimalDotWidth, cellBigDecimalIntWidth, cellBigDecimalDotWidth);
                        }
                        break;
                        case PERCENTAGE: {
                            int data = (Integer) cell.data;
                            if (data < 0 || data > 100) {
                                throw new IllegalArgumentException();
                            }
                            cellWidth = 4;
                            cellsRenderParams[rowIndex][colIndex] = new CellRenderParams(String.format(formatPercentage, data), cellWidth);
                        }
                        break;
                    }
                } catch (NullPointerException | ClassCastException | IllegalArgumentException e) {
                    // Do nothing. Ignore the cell with incorrect information.
                }
                colWidth = Math.max(colWidth, cellWidth);
            }

            int colBigDecimalWidth = (colBigDecimalIntWidth == 0 && colBigDecimalDotWidth == 0) ? 0 : (colBigDecimalIntWidth + 1 + colBigDecimalDotWidth);
            colsRenderParams[colIndex] = new ColRenderParams(Math.max(colWidth, colBigDecimalWidth), colBigDecimalWidth, colBigDecimalIntWidth, colBigDecimalDotWidth);
        }

        StringBuilder result = new StringBuilder();

        for (rowIndex = 0; rowIndex < rowCount; ++rowIndex) {
            for (colIndex = 0; colIndex < colCount; ++colIndex) {
                Cell             cell             = cells            [rowIndex][colIndex];
                CellRenderParams cellRenderParams = cellsRenderParams[rowIndex][colIndex];
                ColRenderParams  colRenderParams  = colsRenderParams           [colIndex];

                int whitespacePrec = 0;
                int whitespaceSucc = 0;

                try {
                    switch (cell.style.type) {
                        case STRING: {
                            if (cell.style.align == CellStyle.Align.LEFT) {
                                whitespaceSucc = colRenderParams.width - cellRenderParams.width;
                            }
                            if (cell.style.align == CellStyle.Align.RIGHT) {
                                whitespacePrec = colRenderParams.width - cellRenderParams.width;
                            }
                        }
                        break;
                        case BIGDECIMAL: {
                            if (cell.style.align == CellStyle.Align.LEFT) {
                                whitespaceSucc = colRenderParams.width - colRenderParams.bigDecimalWidth;
                            }
                            if (cell.style.align == CellStyle.Align.RIGHT) {
                                whitespacePrec = colRenderParams.width - colRenderParams.bigDecimalWidth;
                            }
                            whitespacePrec += colRenderParams.bigDecimalIntWidth - ((CellRenderParamsBigDecimal) cellRenderParams).intWidth;
                            whitespaceSucc += colRenderParams.bigDecimalDotWidth - ((CellRenderParamsBigDecimal) cellRenderParams).dotWidth;
                        }
                        break;
                        case PERCENTAGE: {
                            if (cell.style.align == CellStyle.Align.LEFT) {
                                whitespaceSucc = colRenderParams.width - cellRenderParams.width;
                            }
                            if (cell.style.align == CellStyle.Align.RIGHT) {
                                whitespacePrec = colRenderParams.width - cellRenderParams.width;
                            }
                            whitespacePrec += cellRenderParams.width - cellRenderParams.text.length();
                        }
                        break;
                    }

                    appendWhitespace(result, whitespacePrec);
                    result.append(cellRenderParams.text);
                    appendWhitespace(result, whitespaceSucc);
                } catch (NullPointerException | ClassCastException e) {
                    appendWhitespace(result, colRenderParams.width);
                }

                result.append((colIndex == colCount - 1) ? '\n' : ' ');
            }
        }

        return result.toString();
    }

    private void appendWhitespace(StringBuilder stringBuilder, int count) {
        for (int index = 0; index < count; ++index) {
            stringBuilder.append(' ');
        }
    }
}
